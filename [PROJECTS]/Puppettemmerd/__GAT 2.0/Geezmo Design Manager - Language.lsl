// Geezmo Crazy Pets - Settings Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec",
                 "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2",
                 "d84f75f9-058c-40b0-aa35-770b71218179"];

integer Start;

string ModuleName;

integer Channel;
integer Listen;


// --------------------------------------------------------

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 209;

// --------------------------------------------------------

key LanguageReq;
list LanguagesFullList;
string Language = "English";

// --------------------------------------------------------

Menu(key _Id)
{
    integer i;
    list Languages = List2Buttons( GetButtons(LanguagesFullList, Start) );
    integer Length = llGetListLength(Languages);
        
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    string text;
    
    if ( Length > 1 ) text = "Current language: " + Language + "\n\nPlease choose a language below:";
    else {
        text = "No one language found.";
        Languages = ["Back <<", "Ok"];
    }
    
    Dialog( _Id, text, Languages, Channel );
}

list GetButtons(list _List, integer _Start)
{
    integer i = _Start;
    list Return;
    integer Length = llGetListLength(_List);

    if ( Length )
    for (;i<=(_Start+9);++i)
    {
        if ( i < Length )
            Return += llList2String(_List, i);
            
        if ( ( (_Start+8 ) == i && _Start + 9 < Length ) || i == Length )
            Return += "Back <<";

        else if ( (_Start+9) == i && (_Start+10) < Length )
            Return += ">> Next";

        if ( i >= Length ) i = (_Start+11);
    }

    return Return;
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += String2Button( llList2String( List, i ) );
        
    return Ret;
}

string String2Button( string String )
{
    return (llStringTrim( llGetSubString( String, 0, 23 ), STRING_TRIM ));
}

string Button2String( string ButtonText, list Stack )
{
    integer i;
    integer Length = llGetListLength( Stack );
    for( ;i<Length;++i)
        if ( llStringTrim( llGetSubString( llList2String( Stack, i ), 0, 23 ), STRING_TRIM ) == ButtonText )
            return llList2String( Stack, i );
    
    return "";
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Crazy Pets :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

Init()
{
    Start = 0;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
}

default
{
    on_rez( integer _p )
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        Listen = llListen(Channel, "", "", "");
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            LanguageReq = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/pets/ajax/get-languages", [], "" );
            return;
        }
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            Start += 10;
            Menu(_i);
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) llMessageLinked( LINK_THIS, 210, "", _i );
            else {
                Start -= 10;
                Menu(_i);
            }
        } else if ( _m != "Ok" ) {
            Language = Button2String( _m, LanguagesFullList );
            llMessageLinked( LINK_THIS, 30000, "Language", Language );
        }
    }

    http_response(key id, integer status, list meta, string body)
    {
        if ( LanguageReq == id )
        {
            LanguagesFullList = llCSV2List( body );
            Start = 0;
            Menu(llGetOwner());
        }
    }
}state Disabled{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "]");}}