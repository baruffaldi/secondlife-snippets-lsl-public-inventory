
list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec", "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2", "d84f75f9-058c-40b0-aa35-770b71218179"];

integer GZM_TALKREQUEST = 700;

// ---------// ---------// ---------// ---------// ---------

 string TargetName;
    key TargetKey;

list Sentences;

// --------------------------------------------------------

// -------------------------------------------------------------------
// 
//        if ( _n == GZM_SETSETTINGS )
//        {
//              SettingsSetItem( _m, [(string)_k] );
//        }
//
// ** SettingsDelItem(string dkey)
// ** SettingsSetItem(string dkey, list data)
// ** SettingsGetItem(string dkey)
// ** SettingsGetElems(integer elem)
// ** SettingsGetKeys() **  ** 
// -------------------------------------------------------------------
//
integer GZM_SETSETTINGS = 30000;
list Settings = [1, "TalkEnabled", "1", "Mode", "1", "Verbose", "1", "ObjectName", "", "Language", "English"];
// -------------------------------------------------------------------
//
list SettingsDelItem(string dkey) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return Settings; else return [elements] + llDeleteSubList(llList2List(Settings,1,-1),loc*(elements+1),(loc*(elements+1))+elements); } SettingsSetItem(string dkey, list data) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) Settings = Settings + [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1); else Settings = [elements] + llListReplaceList(llList2List(Settings,1,-1), [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1),loc*(elements+1),(loc*(elements+1)+elements)); } list SettingsGetItem(string dkey) { if (dkey == "") return []; integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return []; else return llList2List(llList2List(Settings,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements); } list SettingsGetElems(integer elem) { integer elements = llList2Integer(Settings,0); return llList2ListStrided(llList2List(Settings,elem+2,-1),0,-1,elements+1); } list SettingsGetKeys() { return SettingsGetElems(-1); }
// -------------------------------------------------------------------
//

Talk( string Phrase )
{
    string Return;
    string Name = llGetObjectName();
    llSetObjectName("");

    if ( TargetKey == NULL_KEY )
        TargetKey = llGetOwner();

    if ( llList2String( SettingsGetItem( "ObjectName" ), 0 ) != "" )
         Return = "/me " + llList2String( SettingsGetItem( "ObjectName" ), 0 ) + " Geezmo";
    else Return = "/me " + Name;
    
    Return += ": " + llDumpList2String( llParseString2List( llDumpList2String( llParseString2List( Phrase, ["[TARGET]"], [""] ), TargetName ), ["[OWNER]"], [""] ), llKey2Name(llGetOwner()) );
    
    if ( llList2Integer( SettingsGetItem( "Verbose" ), 0 ) && TargetName != "" ) 
        Return += " ( for: " + TargetName + " )";

         if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 3 )
        llInstantMessage( TargetKey, Return );
    else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 0 )
        llWhisper( 0, Return );
    else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 1 )
        llSay( 0, Return );
    else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 2 )
        llShout( 0, Return );
        
    llSetObjectName(Name);
}

default
{
    on_rez( integer _p )
    {
        state PopulateSentences;
    }
    
    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Protected;
        if ( ! llGetListLength( Sentences ) || llGetListLength( Sentences ) == 1 )
            state PopulateSentences;
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_TALKREQUEST )
        {
            TargetKey = _k;
            TargetName = llKey2Name( _k );
            
            if ( llList2Integer( SettingsGetItem( "TalkEnabled" ), 0 ) )
                Talk( llList2String( Sentences, ((integer)llFrand(llGetListLength(Sentences))) ) );
            else if ( TargetKey == "" || TargetKey == llGetOwner() )
                llOwnerSay( "hhhhmmmmhhhh... ronpffffffsss.... pleeease... don't wake me up... ronpfffsss...." );
            return;
        }
        
        if ( _n == GZM_SETSETTINGS )
        {
            SettingsSetItem( _m, [(string)_k] );
            
            if ( _m == "Language" )
                state PopulateSentences;
                
            if ( _m == "TalkEnabled" )
            {
                if ( llList2Integer( SettingsGetItem( "TalkEnabled" ), 0 ) )
                    llOwnerSay("I hate silence... I will answer to all taunts right now!");
                else llOwnerSay("I won't talk anymore on local-chat.");
            }
                
            if ( _m == "ObjectName" )
            {
                if ( llList2String( SettingsGetItem( _m ), 0 ) != "" )
                    llMessageLinked( -1, 830, "*** Miamai ***\n" + llList2String( SettingsGetItem( _m ), 0 ) + " Geezmo", "" );
                else llMessageLinked( -1, 830, "", "" );
            }
             
            if ( _m == "Verbose" )
            {   
                if ( llList2Integer( SettingsGetItem( _m ), 0 ) ) llOwnerSay( "The verbose option is now enabled." );
                else llOwnerSay( "The verbose option is now disabled." );
            }
            
            if ( _m == "Mode" )
            {   
                     if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 0 )
                    llOwnerSay( "The new talk mode is \"Whisper\"." );
                else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 1 )
                    llOwnerSay( "The new talk mode is \"Say\"." );
                else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 2 )
                    llOwnerSay( "The new talk mode is \"Shout\"." );
                else if ( llList2Integer( SettingsGetItem( "Mode" ), 0 ) == 3 )
                    llOwnerSay( "The new talk mode is \"InstantMessage\"." );
            }
        }
    }
}

state PopulateSentences
{
    state_entry()
    {
        llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/pets/ajax/get-sentences?l="+llList2String( SettingsGetItem( "Language" ), 0 ), [], "" );
    }

    http_response(key id, integer status, list meta, string body)
    {
        Sentences = llParseString2List( body, ["\n"], [] );
        if ( llGetListLength( Sentences ) >= 2 )
            llOwnerSay( (string)llGetListLength( Sentences ) + " phrases for the language: " + llList2String( SettingsGetItem( "Language" ), 0 ) );//+ ". You can submit new phrases througth the geezmo laboratory website, check it out!" );
        state default;
    }
}state Protected{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "]");}}