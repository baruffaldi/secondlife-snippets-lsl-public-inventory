// Geezmo Crazy Pets - Mood Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec", "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2", "d84f75f9-058c-40b0-aa35-770b71218179"];

integer Channel;
integer Listen;
integer SetNameListen;
integer SetNameChannel = 3000;

string ModuleName;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 209;
integer GZM_REQUEST = 310;

integer Start;

// -------------------------------------------------------------------

// -------------------------------------------------------------------
// 
//        if ( _n == GZM_SETSETTINGS )
//        {
//              SettingsSetItem( _m, [(string)_k] );
//        }
//
// ** SettingsDelItem(string dkey)
// ** SettingsSetItem(string dkey, list data)
// ** SettingsGetItem(string dkey)
// ** SettingsGetElems(integer elem)
// ** SettingsGetKeys() **  ** 
// -------------------------------------------------------------------
//
integer GZM_SETSETTINGS = 30000;
list Settings = [1, "SoundsEnabled", "1", "FlipTitle", "0"];
// -------------------------------------------------------------------
//
list SettingsDelItem(string dkey) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return Settings; else return [elements] + llDeleteSubList(llList2List(Settings,1,-1),loc*(elements+1),(loc*(elements+1))+elements); } SettingsSetItem(string dkey, list data) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) Settings = Settings + [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1); else Settings = [elements] + llListReplaceList(llList2List(Settings,1,-1), [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1),loc*(elements+1),(loc*(elements+1)+elements)); } list SettingsGetItem(string dkey) { if (dkey == "") return []; integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return []; else return llList2List(llList2List(Settings,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements); } list SettingsGetElems(integer elem) { integer elements = llList2Integer(Settings,0); return llList2ListStrided(llList2List(Settings,elem+2,-1),0,-1,elements+1); } list SettingsGetKeys() { return SettingsGetElems(-1); }
// -------------------------------------------------------------------
//
Menu( key _Id )
{
    Dialog( _Id, "Please choose an option below:", ["Sleep", "Quiet", "Happy", "Angry", "<< Back"], Channel );
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Crazy Pets :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

Init()
{
    Start = 0;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
}

default
{
    on_rez( integer _p )
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        Listen = llListen(Channel, "", "", "");
        llSetText( "", ZERO_VECTOR, 0 );
        llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "ObjectName", "" );
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "");
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
        
        if ( _n == GZM_SETSETTINGS )
        {
              SettingsSetItem( _m, [(string)_k] );
        }
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == "Sleep" ) 
        {
            llMessageLinked( -1, GZM_SETSETTINGS, "Eyes", "0" );
            llMessageLinked( LINK_THIS, 3733, "DimGrey", "Eyes" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "SoundsEnabled", "0" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "TalkEnabled", "0" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Running", "0" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "AlwaysOn", "0" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Verbose", "0" );
            llOwnerSay( "I'm sleeping right now... see you later..." );
            Menu(_i);
        } else if ( _m == "Quiet" ) {
            llMessageLinked( -1, GZM_SETSETTINGS, "Eyes", "1" );
            llMessageLinked( LINK_THIS, 3733, "White", "Eyes" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "SoundsEnabled", "0" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "TalkEnabled", "1" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Running", "0" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "AlwaysOn", "0" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Verbose", "0" );
            llOwnerSay( "Yaaaawn... what a tired cat." );
            Menu(_i);
        } else if ( _m == "Happy" ) {
            llMessageLinked( -1, GZM_SETSETTINGS, "Eyes", "1" );
            llMessageLinked( LINK_THIS, 3733, "White", "Eyes" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "SoundsEnabled", "1" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "TalkEnabled", "1" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Running", "1" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "AlwaysOn", "0" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Verbose", "0" );
            llOwnerSay( "Heeeeeeeeeeeeyaaaaaaaa!!! Why giving me all this rhum? I'm little... HOOOOOOOOO" );
            Menu(_i);
        } else if ( _m == "Angry" ) {
            llMessageLinked( -1, GZM_SETSETTINGS, "Eyes", "1" );
            llMessageLinked( LINK_THIS, 3733, "White", "Eyes" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "SoundsEnabled", "1" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "TalkEnabled", "1" );
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Running", "1" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "AlwaysOn", "1" );
            llSleep(0.2);
            llMessageLinked( LINK_THIS, GZM_SETSETTINGS, "Verbose", "1" );
            llOwnerSay( "GRRRRRRRRRRRRRRRR... GIVE ME SOMEONE TO EAT PLEASE." );
            Menu(_i);
        } else if ( _m == "<< Back" )
            llMessageLinked( LINK_THIS, 210, "Core", _i );
    }
}state Disabled{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "]");}}