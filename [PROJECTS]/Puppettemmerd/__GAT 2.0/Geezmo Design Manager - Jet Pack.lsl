// Geezmo Design Manager - Settings Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec",
                 "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2",
                 "d84f75f9-058c-40b0-aa35-770b71218179"];

integer Channel;
integer Listen;

string ModuleName;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 209;
integer GZM_TALKREQUEST = 700;
integer GZM_SETCOLORRETURN = 871;

integer Start;

key EngineId = "594ce7bd-561b-62e1-5513-09a5a33d6af5";

// -------------------------------------------------------------------
// 
//      if ( _n == GZM_SETSETTINGS )
//      {
//            SettingsSetItem( _m, [(string)_k] )
//      }
//
// ** SettingsDelItem(string dkey)
// ** SettingsSetItem(string dkey, list data)
// ** SettingsGetItem(string dkey)
// ** SettingsGetElems(integer elem)
// ** SettingsGetKeys() **  ** 
// -------------------------------------------------------------------
//
integer GZM_SETSETTINGS = 30000;
list Settings = [1, "Running", "1",
                    "Flying", "0",
                    "AlwaysOn", "0",
                    "SoundsEnabled", "1"];
// -------------------------------------------------------------------
//                  
list SettingsDelItem(string dkey) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return Settings; else return [elements] + llDeleteSubList(llList2List(Settings,1,-1),loc*(elements+1),(loc*(elements+1))+elements); } SettingsSetItem(string dkey, list data) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) Settings = Settings + [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1); else Settings = [elements] + llListReplaceList(llList2List(Settings,1,-1), [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1),loc*(elements+1),(loc*(elements+1)+elements)); } list SettingsGetItem(string dkey) { if (dkey == "") return []; integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return []; else return llList2List(llList2List(Settings,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements); } list SettingsGetElems(integer elem) { integer elements = llList2Integer(Settings,0); return llList2ListStrided(llList2List(Settings,elem+2,-1),0,-1,elements+1); } list SettingsGetKeys() { return SettingsGetElems(-1); }
// -------------------------------------------------------------------
//

Menu( key _Id )
{
    list Buttons;
    
    if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) ) Buttons += "TurnOff";
    else Buttons += "TurnOn";
    
    if ( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ) ) Buttons += "AlwaysOff";
    else Buttons += "AlwaysOn";
    
    Buttons += ["Colors", "<< Back"];

    Dialog( _Id, "Please choose an option below:", Buttons, Channel );
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Crazy Pets :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

Init()
{
    Start = 0;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
    SettingsSetItem( "AlwaysOn", [0] );
    Fly( FALSE, FALSE );
}

Fly( integer Upper, integer Passive )
{
    if ( Upper ) {
        if ( ! Passive && ! llList2Integer( SettingsGetItem( "Flying" ), 0 ) )
        {
            llSetPrimitiveParams([PRIM_POSITION, (vector)(llGetLocalPos()+<0,0,0.1>)]);
        
            if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) )
                llLoopSound( EngineId, 3 );
        }
        llMessageLinked(-1, 2, "JET", NULL_KEY );
        SettingsSetItem( "Flying", [1] );
    } else {
        if ( ! Passive && llList2Integer( SettingsGetItem( "Flying" ), 0 ) )
        {
            llSetPrimitiveParams([PRIM_POSITION, (vector)(llGetLocalPos()-<0,0,0.1>)]);
            llStopSound();
        }
        SettingsSetItem( "Flying", [0] );
        llMessageLinked(-1, llList2Integer( SettingsGetItem( "Flying" ), 0 ), "JET", NULL_KEY );
    }
}
    
default
{
    on_rez( integer _p )
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
        Listen = llListen(Channel, "", "", "");
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
        
        if ( _n == GZM_TALKREQUEST )
        {
            if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) && ! llList2Integer( SettingsGetItem( "Flying" ), 0 ) )
            {
                Fly( llList2Integer( SettingsGetItem( "Running" ), 0 ), FALSE );
                llSetTimerEvent(6);
            }
            return;
        }
        
        if ( _n == GZM_SETCOLORRETURN )
        {
            if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) )
                if ( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ) )
                {
                    Fly( FALSE, FALSE );
                    Fly( llList2Integer( SettingsGetItem( "Running" ), 0 ), FALSE );
                }
            Menu( llGetOwner() );
            return;
        }
        
        if ( _n == GZM_SETSETTINGS )
        {
            SettingsSetItem( _m, [(string)_k] );
            
            if ( ! (integer) _m && llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) ) 
                llStopSound();
            else if ( (integer) _m && llList2Integer( SettingsGetItem( "Flying" ), 0 ) && !llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) ) 
                llLoopSound( EngineId, 3 );
                
            if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) )
                if ( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ) )
                {
                    Fly( FALSE, FALSE );
                    Fly( llList2Integer( SettingsGetItem( "Running" ), 0 ), FALSE );
                }
                
            if ( _m == "Running" )
            {
                if ( llList2Integer( SettingsGetItem( _m ), 0 ) )
                    llOwnerSay("The jet-pack has been enabled.");
                else llOwnerSay("The jet-pack has been disabled.");
            }

            if ( _m == "AlwaysOn" )
            {
                if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) )
                {    
                    if ( llList2Integer( SettingsGetItem( _m ), 0 ) )
                    {
                        llOwnerSay("Jet-pack always active. Defensive mode active.");
                    } else {
                        if ( llList2Integer( SettingsGetItem( "Flying" ), 0 ) )
                            Fly( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ), FALSE );
                        llOwnerSay("Jet-pack ready. Defensive mode inactive.");
                    }
                }
            }
            return;
        }
    }
    
    timer()
    {
        Fly( FALSE, FALSE );
        llSetTimerEvent(0);
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == "Colors" )
            llMessageLinked( LINK_THIS, 870, "JetPack", _i );
        else if ( _m == "<< Back" )
            llMessageLinked( LINK_THIS, 210, "Core", _i );
        else {
            if ( _m == "AlwaysOn" )
            {
                SettingsSetItem( _m, [1] );
                if ( llList2Integer( SettingsGetItem( "Running" ), 0 ) )
                    Fly( llList2Integer( SettingsGetItem( _m ), 0 ), FALSE );
            } else if ( _m == "AlwaysOff" ) {
                SettingsSetItem( "AlwaysOn", [0] );
                Fly( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ), FALSE );
            } else if ( _m == "TurnOn" ) {
                SettingsSetItem( "Running", [1] );
                if ( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ) )
                    Fly( llList2Integer( SettingsGetItem( "AlwaysOn" ), 0 ), FALSE );
            } else if ( _m == "TurnOff" ) {
                SettingsSetItem( "Running", [0] );
                Fly( llList2Integer( SettingsGetItem( "Running" ), 0 ), FALSE );
            } else {
                
            }
            Menu(_i);
        }
    }
}

state Disabled{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "]");}}