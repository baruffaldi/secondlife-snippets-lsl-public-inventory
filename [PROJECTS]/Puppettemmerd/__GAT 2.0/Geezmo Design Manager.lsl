// Geezmo Design Manager Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
    
list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec", "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2", "d84f75f9-058c-40b0-aa35-770b71218179"];

integer OwnerOnly = TRUE;
integer Start;
integer Channel;
integer Listen;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 210;

// --------------------------------------------------------

list Modules = [1];
list KnownModules = [1,
"Presets", 200,
"Textures", 201,
"Colors", 202,
"Alpha", 203,
"Sculp", 204,
"Sizer", 205,
"Animations", 206
];

// --------------------------------------------------------

Menu(key _Id)
{
    list Modules = GetDictButtons(Modules, Start, 1);
    integer Length = llGetListLength(Modules);

    string text;
    if ( Length > 0 ) {
        if ( _Id == NULL_KEY ) _Id = llGetOwner();
        text = "Please choose a module below:";
    } else {
        text = "No one Design Manager Module found.";
        Modules = [];
    }
    
    Dialog( _Id, text, List2Buttons(Modules) + ["Talk"], Channel );

    if ( ! Length ) 
        llInstantMessage( _Id, "You have to add at least one Design Manager Module on this prim's content." );
}

UpdateInventoryData( integer _Type )
{
    integer _i;
    integer _Length = llGetInventoryNumber(_Type);
    string ScriptName = llGetScriptName();
    Modules = [1];

    string Name;
    for(;_i<_Length;++_i)
    {
        Name = llGetInventoryName( _Type, _i );
        if ( llGetSubString( Name, 0, llStringLength( ScriptName )-1 ) == ScriptName && Name != ScriptName )
            Modules += [String2Button( llList2String( llParseString2List( Name, ["-"], [] ), 1 ) ), llList2String( llParseString2List( Name, ["-"], [] ), 1 )];
    }
}

list GetDictButtons(list _List, integer _Start, integer _StrideIndex)
{
    integer i = (_Start+1);
    list Return;
    integer _Length = llGetListLength(_List);
    integer _StrideLength = llList2Integer( _List, 0 );
    
    ++_StrideLength;
    ++_Start;


    if ( _Length ) 
    for (;i<=(_Start+(9*_StrideLength));)
    {
        if ( i < _Length )
            Return += llList2String(_List, i + (_StrideIndex-1));
            
        if ( ( (_Start+(8*_StrideLength)) == i || i == _Length ) && _Start != 1 )
            Return += "Back <<";

        else if ( (_Start+(9*_StrideLength)) == i && (_Start+(9*_StrideLength)) < _Length )
            Return += ">> Next";

        if ( i >= _Length ) i = (_Start+(11*_StrideLength));
        else i += _StrideLength;
    }
    
    return Return;
}

string String2Button( string String )
{
    return (llStringTrim( llGetSubString( String, 0, 23 ), STRING_TRIM ));
}

string DictButton2String( string ButtonText, list Stack )
{
    return String2Button( llList2String( Stack, llListFindList( Stack, [ButtonText] ) + 1 ) );
}

list DictGetItem(list dict, string dkey)
{
    if (dkey == "") return [];
    integer elements = llList2Integer(dict,0);
    integer loc = llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    if (loc<0)
        return [];
    else
        return llList2List(llList2List(dict,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements);
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += String2Button( llList2String( List, i ) );
        
    return Ret;
}

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Design Manager ::\n"+_Text, _Buttons, _Channel );
}

Init()
{
    UpdateInventoryData( INVENTORY_SCRIPT );
    
    key _Id; 
    
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    if ( OwnerOnly ) _Id = llGetOwner(); 
    
    Listen = llListen( Channel, "", _Id, "" );
    
    llMessageLinked( LINK_THIS, GZM_SETOWNER, (string)Channel, _Id );
}

default
{
    on_rez( integer _p )
    {
        Init();
    }
    
    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Init();
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_INVENTORY )
            UpdateInventoryData( INVENTORY_SCRIPT );
    }
    
    touch_start(integer total_number)
    {
        if ( ! OwnerOnly || llDetectedKey(0) == llGetOwner() )
            Menu(llDetectedKey(0));
        else {
            llMessageLinked( LINK_THIS, 700, llDetectedName(0), llDetectedKey(0) );
            llMessageLinked( LINK_THIS, 800, (string)llFrand(3), llDetectedKey(0) );
        }
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            Start += 20;
            Menu(_i);
        } else if ( _m == "Back <<" ) {
            if ( Start ) {
                Start -= 20;
                Menu(_i);
            }
        } else if ( _m == "Talk" ) {
            llMessageLinked( LINK_THIS, 700, "", NULL_KEY );
            llMessageLinked( LINK_THIS, 800, (string)llFrand(3), NULL_KEY );
            Menu(_i);
        } else llMessageLinked( LINK_THIS, llList2Integer( DictGetItem( KnownModules, DictButton2String(_m, Modules) ), 0 ), _m, _i );
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_MENUREQUEST )
        {
            Start = 0;
            Menu(_k);
        }
    }    
}state Disabled{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "]");}}