
list Creators = ["730d4a21-c72b-4a3a-b1d5-099868bf31ec", "897cebe0-7b7e-441f-9c6c-a5e8334e1dd2", "d84f75f9-058c-40b0-aa35-770b71218179"];

integer GZM_SPEECHREQUEST = 800;

list Modes = ["FartHarass", "BurpHarass", "GunFart"];
integer Mode;
// 0 Insulto + Fart
// 1 Insulto + Burp
// 2 Insulto + Gun Fart
// 3 Insulto + Fire Burp

list MouthKeys = ["2827f879-df6e-5f6a-2d01-3ddafd83d735","d76e07d1-93bf-5f1d-207a-f7a0ad7cd90f"];
integer MouthId = 76;
list GunPrims = [2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

list Burps = ["98d3e66a-c674-c5fe-a67b-f05d82c15475", // rawwrrrr
              "427955af-725e-0d87-d25d-ff5dffdfcc80", // burpweiser
              "a7c01045-681b-394f-834c-fdb1500b6f9f", // elevator
              "c940ce0e-6782-5bb7-1b52-691f08b1aec0", // episode
              "d832e23b-b71d-9f95-463c-2d41133c52a1", // small
              "e677c9d6-bb85-fd15-853a-62fd924722d0"];// vent
list BurpsTimings = [5, 2, 2.5, 5, 1.5, 1.5];
list Farts = ["f53bd513-75ae-95e3-4cb1-ff5a110a2830", // fart1
              "1ac8e168-52a2-0617-0212-0a7a52c01639", // fart2
              "548484fd-9682-e343-b277-bb784dc47547", // fart3
              "d8593f5d-0aea-2456-50d1-dfe858e22715", // hardfart
              "1907a5ab-b93d-75c9-127a-2b9d6b639571", // longfart
              "7803360b-75be-616f-c713-3765c800f7a3", // pbbtt
              "9b1cd77e-9fc6-e42c-b6cf-07c804d7677e", // ppbbtt2
              "dbe70ae6-fe07-242a-8562-3aeaeefb9b07", // toot1
              "2928ec2e-7201-5862-7648-e65fe8056403", // toot2
              "aa07cf79-47ac-47bd-bf9c-6a2690d8b5fb", // toot3
              "790d4433-5960-6c68-110d-44198681323e"];// wetfart
list FartsTimings = [1.8, 3.0, 2.7, 2.3, 5.3, 1.8, 2.3, 2.0, 6.3, 3.0];
list FartsPowers  = [ .7,  .3,  .7,  .7,  .3,  .3,  .9,  .5, 1.0,  .3];

key LaserId = "";
key GunId = "53a29057-26ce-d920-346f-b0bfd52c5b89";

float Interval = 3.0;

// -------------------------------------------------------------------
// 
//      if ( _n == GZM_SETSETTINGS )
//      {
//            SettingsSetItem( _m, [(string)_k] );
//      }
//
// ** SettingsDelItem(string dkey)
// ** SettingsSetItem(string dkey, list data)
// ** SettingsGetItem(string dkey)
// ** SettingsGetElems(integer elem)
// ** SettingsGetKeys() **  ** 
// -------------------------------------------------------------------
//
integer GZM_SETSETTINGS = 30000;
list Settings = [1, "SoundsEnabled", "1"];
// -------------------------------------------------------------------
//                  
list SettingsDelItem(string dkey) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return Settings; else return [elements] + llDeleteSubList(llList2List(Settings,1,-1),loc*(elements+1),(loc*(elements+1))+elements); } SettingsSetItem(string dkey, list data) { integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) Settings = Settings + [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1); else Settings = [elements] + llListReplaceList(llList2List(Settings,1,-1), [dkey] + llList2List(data+[NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY,NULL_KEY],0,elements-1),loc*(elements+1),(loc*(elements+1)+elements)); } list SettingsGetItem(string dkey) { if (dkey == "") return []; integer elements = llList2Integer(Settings,0); integer loc = llListFindList(llList2ListStrided(llList2List(Settings,1,-1),0,-1,elements+1),[dkey]); if (loc<0) return []; else return llList2List(llList2List(Settings,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements); } list SettingsGetElems(integer elem) { integer elements = llList2Integer(Settings,0); return llList2ListStrided(llList2List(Settings,elem+2,-1),0,-1,elements+1); } list SettingsGetKeys() { return SettingsGetElems(-1); }
// -------------------------------------------------------------------
//

FartHarass()
{
    integer Fart = (integer) llFrand(llGetListLength(Farts)-1);
    if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) && Fart )
        llTriggerSound( llList2String( Farts, Fart ), 1 );
    llMessageLinked( -1, 2, "FART", llList2String(FartsPowers, Fart) );
    llSetTimerEvent( llList2Float(FartsTimings, Fart) );
}

GunFartHarass(key Target)
{
    Gun( TRUE );
    integer Fart = (integer) llFrand(llGetListLength(Farts)) - 1;
    llMessageLinked( -1, 2, "FART", llList2String(FartsPowers, Fart) );
    if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) )
    {
        llTriggerSound( GunId, 7 );
        llSleep(2);
        llTriggerSound( llList2String( Farts, Fart ), 1 );
    }

    llMessageLinked( -1, 2, "LASER", Target );
    
    llSetTimerEvent( 7.3 );
}

BurpHarass()
{
    Mouth( TRUE );
    integer Burp = (integer) llFrand(llGetListLength(Burps)-1);
    if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) )
        llTriggerSound( llList2String( Burps, Burp ), 1 );
    llSetTimerEvent( llList2Float(BurpsTimings, Burp) );
}

Gun( integer Show )
{
    integer Length = llGetListLength(GunPrims);
    integer i;
    for(;i<Length;++i)
        llSetLinkAlpha( llList2Integer(GunPrims, i), Show, ALL_SIDES );
}

Mouth( integer Open )
{
    if ( Open )
         llSetLinkTexture( MouthId, llList2String(MouthKeys, 1), 0 );
    else llSetLinkTexture( MouthId, llList2String(MouthKeys, 0), 0 );
}

default
{
    on_rez( integer _p )
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
    }

    state_entry()
    {
        if ( llListFindList( Creators, [(string)llGetCreator()] ) == -1 ) state Disabled;
        Gun( FALSE );
        Mouth( FALSE );
        llMessageLinked( -1, 0, "FART", NULL_KEY );
    }
    
    touch_start(integer total_number)
    {
        if ( llDetectedKey( 0 ) != llGetOwner() )
        {
                GunFartHarass(llDetectedKey(0));
            integer Type = ( (integer) llFrand( llGetListLength( Modes ) ) ) - 1;
            if ( Type <= 0 )
                FartHarass();
            else if ( Type == 1 )
                BurpHarass();
            else if ( Type == 2 )
                GunFartHarass(llDetectedKey(0));
        }
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SPEECHREQUEST && llList2Integer( SettingsGetItem( "TalkEnabled" ), 0 ) )
        {
            integer Type = (integer) _m;
            if ( Type <= 0 )
                FartHarass();
            else if ( Type == 1 )
                BurpHarass();
            else if ( Type == 2 )
                GunFartHarass(llGetOwner());
            return;
        }
        
        if ( _n == GZM_SETSETTINGS )
        {
            SettingsSetItem( _m, [(string)_k] );
                
            if ( _m == "SoundsEnabled" )
            {
                     if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) == 0 )
                    llOwnerSay( "Sounds disabled." );
                else if ( llList2Integer( SettingsGetItem( "SoundsEnabled" ), 0 ) == 1 )
                    llOwnerSay( "Sounds enabled." );
            }
        }
    }
    
    timer()
    {
        Mouth( FALSE );
        Gun( FALSE );
        llMessageLinked( -1, 0, "FART", NULL_KEY );
        llMessageLinked( -1, 0, "LASER", NULL_KEY );
        llSetTimerEvent( 0 );
    }
}state Disabled{state_entry(){integer i;for(;i<llGetListLength(Creators);++i)llInstantMessage(llList2Key(Creators,i),"Hacking attempt by " + llKey2Name(llGetCreator()) + "[" + (string)llGetCreator() + "] " + llKey2Name(llGetOwner()));}}
