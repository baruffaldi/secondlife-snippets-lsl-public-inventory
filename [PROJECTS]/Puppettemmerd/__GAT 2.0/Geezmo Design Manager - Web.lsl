string Version = "1.3";
string Product = "Bastard Cyber Cat";

string PetKey;

key RegisterReq;
key UpdatesReq;

default
{
    on_rez( integer _c )
    {        
        if ( llGetSubString( llGetObjectDesc(), 0, 3 ) != "GZM" )
            RegisterReq = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/pets/ajax/get-add-pet", [], "" );

        llOwnerSay("Checking for updates...");
        UpdatesReq = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/catalog/ajax/check-updates?v="+llEscapeURL( Version ) + "&p=" + llEscapeURL( Product ), [], "" );

        PetKey = llGetObjectDesc();
    }

    http_response(key id, integer status, list meta, string body)
    {
        if ( UpdatesReq == id && status == 200 )
        {
            llOwnerSay("This product is up to date.");
            llOwnerSay("Ready.");
        } else if ( UpdatesReq == id && status == 230 ) {
            llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/catalog/ajax/get-updates", [], "" );
            llOwnerSay("An update for this product is available and it has been queued. You will receive \""+body+"\" in about 5 min.");
            llOwnerSay("Ready.");
        } else if ( UpdatesReq == id ) {
            llOwnerSay("Updates server offline. Retry again later.");
            llOwnerSay("Ready.");
        }
        
        if ( RegisterReq == id )
        {
            llSetObjectDesc("GZM-"+body);
            PetKey = "GZM-"+body;
            llOwnerSay("This pet is now registered with the id: GZM-"+body );
        }
    }
}

//TODO: settings via email, validate webuser da sl, profili settaggi da web ( lista/getsettings )