default
{
    state_entry()
    {
        // setta tutte le variabili
    }

    touch_start(integer total_number)
    {
        // parte
    }
    
    on_rez( integer _p )
    {
        llResetScript();
    }
    
    changed( integer _c )
    {
        llResetScript();
    }
}

state Running
{
    state_entry()
    {
        // get da lista del prim da elaborare
        // se esiste setta la variabile e rimanda a Working
        // state Working;
        // altrimenti torna al default
        // state default;
    }
}

state Working
{
    state_entry()
    {
        // Rez
        // get dei parametri di TUTTO
        // llSleep(1);
        // Invio informazioni
        // Controllo se ha finito quel prim di lavorare
        // state Working;
    }
}