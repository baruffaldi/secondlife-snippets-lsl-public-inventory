string URL   = "http://w-hat.com/name2key"; // name2key url
key    reqid;                               // http request id

// ------------------------------------------------

string Name;

default
{
    on_rez( integer _n )
    {
        llResetScript();
    }
    
    state_entry()
    {
        llListen(90, "", llGetOwner(), "" );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        Name = _m;
        reqid = llHTTPRequest( URL + "?terse=1&name=" +
                               llEscapeURL(Name), [], "" );
    }
    
    dataserver(key req, string data){
        if(data == "1" || data == "0"){
            if ( data == "1" ) llOwnerSay( Name + " is currently: ONLINE." );
            else llOwnerSay( Name + " is currently: OFFLINE." );
        }else{
            llOwnerSay( "Getting online status for " + Name);
        }
    }

    http_response(key id, integer status, list meta, string body) {
        if ( id != reqid )
            return;
        if ( status == 499 )
            llOwnerSay("HTTP Request timed out");
        else if ( status != 200 )
            llOwnerSay("the internet exploded!!");
        else if ( (key)body == NULL_KEY )
            llOwnerSay("No key found for " + Name);
        else
            llRequestAgentData((key)body,DATA_ONLINE);
    }
}