string feedTitle;
string feedLink;

list itemTitles;
list itemLinks;
list itemEnclosures;

parseElement(string line)
{
    /// sss element - new feed
    if(line == "sss")
    {
        feedTitle = "";
        feedLink = "";
        itemTitles = [];
        itemLinks = [];
        itemEnclosures = [];
        return;
    }
    
    // determine type of element
    string element = llGetSubString(line, 0, llSubStringIndex(line, " ") - 1);
    element = llToLower(element);
    
    // parse known elements
    if(element == "item")
        parseItemElement(llGetSubString(line, llStringLength(element) + 1, -1));
    else if(element == "feed")
        parseFeedElement(llGetSubString(line, llStringLength(element) + 1, -1));
}
parseFeedElement(string line)
{
    // parse attribute name/value pairs
    list parts = llParseString2List(line, ["=\"", "\" "], []);
    integer n = llGetListLength(parts);
    integer i;
    
    // loop through name/value pairs
    for(i = 0; i < n; i += 2)
    {
        // read name/value pair
        string name = llList2String(parts, i);
        name = llToLower(name);
        string value = llList2String(parts, i + 1);
        
        // assign known values
        if(name == "title") feedTitle = value;
        if(name == "link") feedLink = value;
    }
}
parseItemElement(string line)
{
    // parse attribute name/value pairs
    list parts = llParseString2List(line, ["=\"", "\" ", "\""], []);
    integer n = llGetListLength(parts);
    integer i;
    
    // set default values
    string title = "";
    string link = "";
    string enclosure = "";
    
    // loop through name/value pairs
    for(i = 0; i < n; i += 2)
    {
        // read name/value pair
        string name = llList2String(parts, i);
        name = llToLower(name);
        string value = llList2String(parts, i + 1);
        
        // assign known values
        if(name == "title") title = value;
        if(name == "link") link = value;
        if(name == "enclosure") enclosure = value;
    }
    
    // add new item
    itemTitles += [title];
    itemLinks += [link];
    itemEnclosures += [enclosure];
}

key HttpRequestId;

string GoogleBaseURL = "http://www.google.com/search?source_id=secondlife&ie=UTF-8&q=";
string GoogleFeedURL = "http://feedmysearch.com/?where=web&q=";
string SearchBaseURL = "http://secondlife.coolminds.org/?feed=";
string Search;

default
{
    state_entry()
    {
        SearchBaseURL += llEscapeURL( GoogleFeedURL );
        llListen(2, "", llGetOwner(), "" );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        llSay(0, "Searching for: " + _m );
        //llSay(0, SearchBaseURL + llEscapeURL( _m ));
        HttpRequestId = llHTTPRequest( SearchBaseURL + llEscapeURL( _m ), [], "");
        Search = _m;
    }
    
    http_response( key _r, integer _s, list _m, string _b )
    {
        if (_r == HttpRequestId)
        {
            string Name = llGetObjectName();
            llSetObjectName("");
            
            integer Length = llStringLength(_b);
            
            llWhisper(0, "/me Response length: " + (string) Length );
            llWhisper(0, "/me Response:" );

            // parse out xml elements
            list lines = llParseString2List(_b, ["/>","<", ">", "\n"], []);
            
            // loop through elements
            integer n = llGetListLength(lines);
            integer i;
            for(i = 0; i < n; i++)
                parseElement(llList2String(lines, i));
            
            for(i=0;i<llGetListLength(itemTitles);++i)
                llWhisper(0, "/me \nTitle: " + llList2String(itemTitles, i) + "\nURL: " + llList2String(itemLinks, i) );
                
            llWhisper(0, "/me Done searching for " + Search + " with " + (string)llGetListLength(itemTitles) + " results.");
            llSetObjectName(Name);
        }
    }
}