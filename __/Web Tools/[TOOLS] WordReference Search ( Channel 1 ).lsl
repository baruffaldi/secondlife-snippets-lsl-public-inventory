key HttpRequestId;

string TranslateURL = "http://secondlife.coolminds.org/translate.php";
list Search;

list SplitString2List( string String, integer MaxLength )
{
    list Return;
    integer Length = llStringLength( String );
    integer i;
    
    for(;i<Length;i+=MaxLength)
        Return += llGetSubString( String, i, i+MaxLength );
        
    return Return;
}

default
{
    on_rez( integer _n )
    {
        llResetScript();
    }
    
    state_entry()
    {
        llListen(1, "", llGetOwner(), "" );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        list Params = llParseString2List( _m, [" "], [] );
        
        string From = llList2String( Params, 0 );
        string To = llList2String( Params, 1 );
        string Word = llList2String( Params, 2 );
        
        HttpRequestId = llHTTPRequest( TranslateURL + "?word=" + llEscapeURL( Word ) + "&from=" + llEscapeURL( From ) + "&to=" + llEscapeURL( To ), [], "");
        Search = [Word, From, To];
    }
    
    http_response( key _r, integer _s, list _m, string _b )
    {
        if (_r == HttpRequestId)
        {
            string Name = llGetObjectName();
            llSetObjectName("");
            
            llWhisper(0, "/me Translating: " + llList2String( Search, 0 ) );
            llWhisper(0, "/me From: " + llList2String( Search, 1 ) );
            llWhisper(0, "/me To: " + llList2String( Search, 1 ) );
            llWhisper(0, "/me Response:" );

            list Response = SplitString2List( _b, 254 );
            integer i;
            for(;i<llGetListLength(Response);++i)
                llSay(0, "/me " + llList2String( Response, i ) );
                
            llSetObjectName(Name);
        }
    }
}