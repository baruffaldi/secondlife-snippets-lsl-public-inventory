//Constants
integer PICTURE_ROTATION_TIMER = 30;   //In whole seconds

integer DISPLAY_ON_SIDE = 4; //Change this to change where the image will be displayed

key VIDEO_DEFAULT = "71b8ff26-087d-5f44-285b-d38df2e11a81";  //Test pattern - Used as default video texture when one is missing in parcel media
key BLANK = "5748decc-f629-461c-9a36-a35a221fe21f"; //Blank texture - Used when there are no textures to display in Picture mode
string NOTECARD = "bookmarks";  //Used to host URL bookmarks for video streams

integer VIDEO_BRIGHT = TRUE;    //FULL_BRIGHT status for Video
integer PICTURE_BRIGHT = TRUE;  //FULL_BRIGHT status for Picture

integer REMOTE_CHANNEL = 9238742;

integer mode = 0;           //Freeview mode.
                            //Mode 0 - Power off
                            //Mode 1 - Picture viewer
                            //Mode 2 - Video

integer listenHandle = -1;      //Dialog menu listen handler
integer listenUrl = -1;         //listen handler for channel 1 for when a URL is being added
integer listenTimer = -1;       //Timer variable for removing all listeners after 2 minutes of listener inactivity
integer listenRemote = -1;      //listen handler for the remote during initial setup
integer encryption = 0;
integer numberofnotecardlines = 0;  //Stores the current number of detected notecard lines.
integer notecardline = 0;       //Current notecard line

integer loop_image = FALSE;     //Are we looping pictures with a timer? (picture mode)
integer current_texture = 0;    //Current texture number in inventory being displayed (picture mode)
integer chan;                   //llDialog listen channel
integer notecardcheck = 0;
key video_texture;              //Currently used video display texture for parcel media stream

string moviename;
string tempmoviename;
key notecardkey = NULL_KEY;
key tempuser;                   //Temp key storge variable
string tempurl;                 //Temp string storge variable

integer isGroup = TRUE;
key groupcheck = NULL_KEY;
key last_owner;
key XML_channel;

video()
{
    running = TRUE;
    //Change prim to Light material while coloring face 0 black to prevent light-lag generation.
    llSetPrimitiveParams([PRIM_BUMP_SHINY, DISPLAY_ON_SIDE, PRIM_SHINY_NONE, PRIM_BUMP_NONE, PRIM_COLOR, DISPLAY_ON_SIDE, <1,1,1>, 1.0, PRIM_MATERIAL, PRIM_MATERIAL_PLASTIC, PRIM_FULLBRIGHT, DISPLAY_ON_SIDE, VIDEO_BRIGHT, PRIM_TEXTURE, DISPLAY_ON_SIDE, "62dc73ca-265f-7ca0-0453-e2a6aa60bb6f", llGetTextureScale(DISPLAY_ON_SIDE), llGetTextureOffset(DISPLAY_ON_SIDE), llGetTextureRot(DISPLAY_ON_SIDE)]);
    
    report("Video mode"+moviename+": Stopped");

    video_texture = llList2Key(llParcelMediaQuery([PARCEL_MEDIA_COMMAND_TEXTURE]),0);
    if(video_texture == NULL_KEY)
    {
        video_texture = VIDEO_DEFAULT;
        llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_TEXTURE,VIDEO_DEFAULT]);
        llSay(0,"No parcel media texture found. Setting texture to default: "+(string)VIDEO_DEFAULT);
        if(llGetLandOwnerAt(llGetPos()) != llGetOwner())
            llSay(0,"Error: Cannot modify parcel media settings. "+llGetObjectName()+" is not owned by parcel owner.");
    }
    
    llSetTexture(video_texture,DISPLAY_ON_SIDE);
}

off()
{
    running = FALSE;
    report("Click to power on.");
    llSetPrimitiveParams([PRIM_BUMP_SHINY, DISPLAY_ON_SIDE, PRIM_SHINY_LOW, PRIM_BUMP_NONE, PRIM_COLOR, DISPLAY_ON_SIDE, <0.1,0.1,0.1>, 1.0,PRIM_MATERIAL, PRIM_MATERIAL_PLASTIC, PRIM_FULLBRIGHT, DISPLAY_ON_SIDE, FALSE, PRIM_TEXTURE, DISPLAY_ON_SIDE, BLANK, llGetTextureScale(DISPLAY_ON_SIDE), llGetTextureOffset(DISPLAY_ON_SIDE), llGetTextureRot(DISPLAY_ON_SIDE)]);
}


integer finditem(string name)   //Finds and returns an item's inventory number
{
    integer i;
    for(i=0;i<llGetInventoryNumber(INVENTORY_NOTECARD);i++)
        if(llGetInventoryName(INVENTORY_NOTECARD,i) == NOTECARD)
            return i;
    return -1;
}

seturl(string url, key id)  //Set parcel media URL
{
    video();
    string oldurl = llList2String(llParcelMediaQuery([PARCEL_MEDIA_COMMAND_URL]),0);
    if(oldurl != "")
        llOwnerSay("Setting new media URL. The old URL was: "+oldurl);

    llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_URL,url]);

    report("Video mode"+moviename+": Playing");
    llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_PLAY]);
       
    llOwnerSay("New media URL set: "+url);
}

string mediatype(string ext)    //Returns a string stating the filetype of a file based on file extension
{
    ext = llToLower(ext);
    if(ext == "swf")
        return "Flash";
    if(ext == "mov" || ext == "avi" || ext == "mpg" || ext == "mpeg" || ext == "smil")
        return "Video";
    if(ext == "jpg" || ext == "mpeg" || ext == "gif" || ext == "png" || ext == "pict" || ext == "tga" || ext == "tiff" || ext == "sgi" || ext == "bmp")
        return "Image";
    if(ext == "txt")
        return "Text";
    if(ext == "mp3" || ext == "wav")
        return "Audio";
    return "Unknown";
}

browse(key id)      //Image browser function for picture viewer mode
{
    integer check = llGetInventoryNumber(INVENTORY_TEXTURE);
    string header;
    if(check > 0)
        header = "("+(string)(current_texture+1)+"/"+(string)check+") "+llGetInventoryName(INVENTORY_TEXTURE,current_texture);
    else
        header = "No pictures found.";
    llDialog(id,"** Monitor Control **\n Picture Viewer mode\n- Image browser\n- "+header,["Back","Next","Menu"],chan);
    extendtimer();
}

report(string str)
{
    llSetObjectDesc(str);
}

extendtimer()       //Add another 2 minute to the Listen Removal timer (use when a Listen event is triggered)
{
    if(listenHandle == -1)
        listenHandle = llListen(chan,"","","");
    listenTimer = (integer)llGetTime() + 120;
    if(loop_image == FALSE)
        llSetTimerEvent(45);
}

config(key id)      //Configuration menu
{
    extendtimer();
    llDialog(id,"Current media URL:\n"+llList2String(llParcelMediaQuery([PARCEL_MEDIA_COMMAND_URL]),0)+"\nTip: If the picture is abit off, try 'Align ON'",["Set URL","Align ON","Align OFF","Menu","Set Remote"],chan);
}

tell_remote(string str)
{
    llShout(REMOTE_CHANNEL,llXorBase64Strings(llStringToBase64((string)encryption + str), llStringToBase64((string)encryption)));
}

integer running;

default
{
    state_entry()
    {
        llListen( 3, "", "", "" );
    }

    touch_start(integer total_number)
    {
        if ( running ) off();
        else video();
    }
    
    listen(integer _c, string _n, key _k, string _m)
    {
        if (_c == 3)
        {
            if(_m == "Play")
            {
                report("Playing");
                llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_PLAY]);
                return;
            }
            if(_m == "Stop")
            {
                report("Stopped");
                llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_STOP]);
                return;
            } else {
                seturl(_m, NULL_KEY);
                report("Playing");
                llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_PLAY]);
                return;
            }
        }
    }
}
