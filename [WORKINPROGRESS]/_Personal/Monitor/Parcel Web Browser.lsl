//Parcel Web Browser v1.0
//(C) Antonius Misfit 2008
//Licensed under the terms of the GNU GPLv3 or later
//for the "prim preview" feature to work,
//set the appropriate texture on the front
//according to your parcel's web settings
string home_url="http://secondlife.com";//home URL
string current_url;
default
{
    state_entry()
    {
        llListen(0,"",llGetOwner(),"");
    }    
    //touch to load the webpage in the in-world browser
    touch_start(integer total_number)
    {
        llLoadURL(llDetectedKey(0),"Load live browser instance for "+current_url+"?",current_url);
    }

    listen(integer channel,string name,key id,string message)
    {
        //Say "Go http://yourwebsite.com" to preview on a prim
        if(llGetSubString(message,0,1)=="Go")
        {
        current_url=llGetSubString(message,2,llStringLength(message)-1);
        llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_URL,current_url]);
        }
        //go to your home URL
        if(message=="Home")
        {
        current_url=home_url;
        llParcelMediaCommandList([PARCEL_MEDIA_COMMAND_URL,current_url]);
        }
    }
} 
