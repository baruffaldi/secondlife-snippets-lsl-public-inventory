float  nearest;
vector nearest_pos;
key    nearest_key;
vector pos;
vector origin;

integer running;

default
{
    state_entry()
    {
        origin = llGetPos();
        llSetStatus( STATUS_PHYSICS, TRUE );
        llSensorRepeat(
            "",
            NULL_KEY,
            AGENT,
            92.2,
            PI,
            0.7
        );
        
        pos = llGetPos();
    }
    
    sensor( integer _n )
    {
        if ( ! running ) running = TRUE;
        llSetPos( pos );
        integer x;
        nearest = 0;
        
        // Determino il piu' vicino
        for ( ; x < _n; ++x )
        {
            if ( llVecDist( pos, llDetectedPos( x ) ) < nearest || nearest == .0 )
            {
                nearest = llVecDist( pos, llDetectedPos( x ) );
                
                nearest_pos = llDetectedPos( x );
                nearest_pos.z += 1.0;
                
                nearest_key = llDetectedKey( x );
            }
        }
        
        //llSay(0, (string) nearest + " " + (string) nearest_pos + " " + (string) nearest_key + " " + llKey2Name( nearest_key ) );
        llLookAt( nearest_pos, .5, 0.3 );
        llSetPos( origin );
        llSleep(0.3);
    }
     
    no_sensor( )
    {
        if ( running )
        {
            llStopLookAt( );
            llResetScript( );
        }
    }
}
