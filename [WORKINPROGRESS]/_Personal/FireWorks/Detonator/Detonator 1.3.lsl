// Modularizer v0.7 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ F.B. <bornslippyRuby@coolminds.org>
// Licensed under the GNU GPLv3 License
// http://www.gnu.org
//

// ---[ Default Application Settings ]-----------------

string applicationName = "FireWorks Deluxe - Detonator";
string applicationDesc = "v0.1";
string configuration   = "Detonator-Default";
string header          = "*** FireWorks Deluxe - Detonator ***";

integer ownerOnly       = TRUE;
integer debug           = FALSE;
integer menuStatus      = TRUE;
integer permanentConf   = FALSE;
integer linkedMsgTarget = LINK_THIS;

// ----------------------------------------------------
// ---[ Environment Variables ]------------------------

    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;
 string modulesList;
   list modules;
   list modulesKeys;
integer dialogsChannel;
integer action;
 vector VNULL;

integer controllersRequestsChannel  = -1247689023;
integer controllersResponsesChannel = -1286429002;

// ----------------------------------------------------
// ---[ Custom Variables ]-----------------------------
list settings = ["example_param", 3]; // With default values

list controllers;

// ----------------------------------------------------
// ---[ Modularizer Functions ]------------------------
        
report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _text != "" )
        llInstantMessage( llGetOwner(), header + "\n" + _text );
        
    if ( _fliptitle != "" )
    {
        if ( _color == VNULL ) _color = <1.0, 1.0, 1.0>;
        llSetText( applicationName + "\n" + _fliptitle, _color, 1.0 );
        if ( _pause ) llSleep(1.5);
        if ( _last ) llSetText( "", VNULL, 0.0 );
    }
}

reportDialog( key _dest, string _text, list _buttons )
{
    if ( _dest == NULL_KEY ) _dest == llGetOwner();

    llDialog(
        _dest,
        header + "\n" + _text,
        _buttons,
        dialogsChannel );
}

reset()
{
    report(
        "",
        "Resetting application...",
        VNULL, FALSE, FALSE );

    llMessageLinked( linkedMsgTarget, 0, "RESETSCRIPT", NULL_KEY );

    llSleep(1.7); // Giving the time to reset all scripts

    report(
        "Welcome to " + applicationName + "!",
        "Welcome!!!\nInizializing...",
        VNULL, TRUE, FALSE
            );
}

detectConfiguration()
{
    report(
        "",
        "Update detected",
        VNULL, FALSE, FALSE
            );
    
    if ( llGetInventoryType( configuration ) == INVENTORY_NOTECARD )
        if ( llGetInventoryPermMask( configuration, MASK_OWNER ) )
        {
            report(
                "Configuration '" + configuration + "' found! Leeching settings in progress...",
                "Configuration reading... 0%",
                VNULL, FALSE, FALSE
                    );

            notecardLine = 0;
            notecardTotalLinesId = llGetNumberOfNotecardLines( configuration );

            return;
        }

    report(
        "Configuration '" + configuration + "' not found!\nUsing current settings...",
        "Using current settings...",
        VNULL, FALSE, TRUE
            );
            
    detectModules();
}

menu( key _target )
{
    if ( menuStatus )
    {
        if ( _target == NULL_KEY ) _target = llGetOwner( );
        
        integer length = llGetListLength( modules );
        integer i;
        
        list buttons = ["Reset", "ScanControllers"];
        string desc;
        integer c = llGetListLength( controllers );
        
        if ( c > 0 )
        {
            desc += "Click on Load to choose the rockets scheme.\n\nControllers linked: " + (string) c;
            buttons += ["Load", "Finger", "Rockets"];
        }
        else desc = "There are no detected controllers, you have to scan the area to link those!";
        
        llDialog(
            _target,
            header + "\n" + desc,
            buttons,
            dialogsChannel
        );
    }
}

integer processRequest( integer _chan, string _name, key _id, string _requestedScheme )
{
    string desc;
    list buttons;
    
    if ( _chan == controllersResponsesChannel )
        if ( _requestedScheme == "PONG" )
        {
            controllers += _id;
            
            integer l = llGetListLength( controllers );
            
            string length = (string) l;
            string assetid = (string) ( l - 1 );
            
            report( "Controller found: " + (string) _id + "\nAsset Id: " + assetid, "Controllers found: " + length, VNULL, TRUE, TRUE );
            llShout( controllersRequestsChannel, "IDASSIGN," + assetid );
            
        } else llSay( 0, "not recognized: " + _requestedScheme );
                
    else {
    
        if ( _requestedScheme == "musica maestro" && _chan == 0 && _id == llGetOwner() )
        matrimonio();
        else if ( action == 37 )
        {
            action = 0;
            requestRocket( _requestedScheme, 20, 0, 0 );
        }
//        matrimonio();
        
        else if ( _requestedScheme == "Load" && llGetOwner() == _id )
        {
            action = 1;
            integer all_notecards = llGetInventoryNumber( INVENTORY_NOTECARD );
            integer i;
            
            for ( ; i < all_notecards; ++i )
            {
                list conf = parseScriptName( llGetInventoryName( INVENTORY_NOTECARD, i ) );
                if ( llList2String( conf, 0 ) == "Modularizer" )
                    if ( llGetListLength( conf ) > 1 )
                        buttons += llList2String( conf, 1 );
                    else buttons += "Default";
            }
            
            if ( ! llGetListLength( buttons ) )
                buttons += "Default";
            
            llDialog(
                _id,
                header + "\nChoose the configuration note to load:",
                buttons,
                dialogsChannel
            );
        }
    
        else if ( _requestedScheme == "ScanControllers" )
        {
            detectControllers();
        }
    
        else if ( _requestedScheme == "Rockets" )
        {
            action = 37;
            integer i = 1;
            list buttons = ["Asssshhh", "*"];
            
            for ( ; i <= 10; ++i )
            {
                buttons += (string) i;
            }
            
            llDialog(
                _id,
                header + "\nChoose the rocket type to load:",
                buttons,
                dialogsChannel
            );
        }
    
        else if ( _requestedScheme == "Finger" )
        {
            llShout( controllersRequestsChannel, "FINGER" );
        }
        
        else if ( action == 1 && llGetOwner() == _id )
        {
            configuration = "Modularizer-" + _requestedScheme;
            detectConfiguration();
        }
        
        else if ( _requestedScheme == "Reset" )
            llDialog(
                _id,
                header + "\nAre you really really sure about what you're doing? This will reset all loaded modules settings...\n\nIf you're sure 100%, then press \"Sure!\" else just press \"ignore\"!",
                ["Sure!"],
                dialogsChannel
                );
    
        else if ( _requestedScheme == "Sure!" )
            reset();
    
        else if ( (integer) _requestedScheme > 0 )
        {
            integer section = (integer) _requestedScheme;
            integer length  = llGetListLength( modules );
            integer offset;
            integer end;
    
            if ( section == 1 )
                offset = 0;
            else offset = ( section - 1 ) * 9;
    
            end = ( offset + 9 );
    
            for ( ; offset < end; ++offset )
                if ( offset < length )
                    buttons += llList2String( modules, offset );
    
            llDialog(
                _id,
                header + "\nChoose the rockets scheme to load from those specified below:",
                buttons,
                dialogsChannel
            );
        }
    }

    return 0;
}

list parseScriptName( string _script )
{
    return llParseString2List( _script, ["-"], [] );
}
      
list ListStridedUpdate(list dest, list src, integer start, integer end, integer stride) {
    return llListReplaceList( dest, src, start * stride, ( ( end + 1 ) * stride ) - 1 );
}

integer percent( integer n, integer hundreds )
{    
    return ( n * 100 ) / hundreds;
}

key getModuleKeyByModuleName( string _moduleName )
{
    return llList2String( modulesKeys, llListFindList( modules, [_moduleName] ) );
}

requestRocket( string name, integer totalTime, float minTime, float maxTime )
{
    list params = ["ROCKET"];
    params += name;
    
    if ( totalTime > 0 ) {
        params += totalTime;
        if ( minTime > 0 ) {
            params += minTime;
            if ( maxTime > 0 ) {
                params += maxTime;
            }
        }
    }
    
    llShout( controllersRequestsChannel, llList2CSV( params ) );
    llSleep( totalTime );
}


matrimonio()
{
    llShout( 0, "Siori e Siore... blah blah blah" );
    requestRocket( "1", 15, 0, 0 );
    requestRocket( "2", 15, 0, 0 );
    requestRocket( "3", 15, 0, 0 );
    requestRocket( "4", 15, 0, 0 );
    requestRocket( "5", 15, 0, 0 );
    requestRocket( "6", 15, 0, 0 );
    requestRocket( "7", 15, 0, 0 );
    requestRocket( "8", 15, 0, 0 );
    requestRocket( "9", 15, 0, 0 );
    requestRocket( "*", 20, 0.1, 1.3 );
    requestRocket( "10", 3, 0.1, 10 );
}

detectControllers()
{
    report( "", "Sending ping...", VNULL, TRUE, TRUE );
    controllers = [];
    llShout( controllersRequestsChannel, "PING" );
}

detectModules()
{
    modules = [];
    modulesKeys = [];
    modulesList = "";

    list inventory;

    string  this = llGetScriptName();

    integer inventoryScripts = llGetInventoryNumber( INVENTORY_SCRIPT );
    integer i = 0;
    integer modulesFound = 0;

    report( "", "Searching for modules... 0%", VNULL, FALSE, FALSE );

    for ( ; i < inventoryScripts; ++i )
    {
        report( "", "Searching for modules... " + (string) percent( i, inventoryScripts ) + "%", VNULL, FALSE, FALSE );
        string script = llGetInventoryName( INVENTORY_SCRIPT, i );
        key scriptKey = llGetInventoryKey( script );
        list scriptInfo = parseScriptName( script );

        // Check:
        if( script != this && // it's not this script
            llGetInventoryPermMask( script, MASK_OWNER ) && // the owner is this scripts owner
                llListFindList( modulesKeys, [scriptKey] ) == -1 && // it's not already loaded
                    llList2String( scriptInfo, 0 ) == applicationName ) // it's a module
                    {
                        integer length = llGetListLength( scriptInfo );

                        string module = llList2String( scriptInfo, 1 );
                        string version;

                        if ( length > 2 )
                            version = llList2String( scriptInfo, 2 );
                        else version = "0.0.0";

                        ++modulesFound;

                        modules += [ module ];
                        modulesKeys += [ scriptKey ];
                        modulesList += "- " + module + " (version: " + version + ") (author: " + llKey2Name( llGetInventoryCreator( script ) ) + ")";
                        if ( debug )
                            modulesList += " (uuid: " + (string ) scriptKey + ") (script: " + script + ")";
                        modulesList += "\n";
                    }
    }

    if ( modulesFound < 1 )
        report( "Cannot find any " + applicationName + " module inside the script's inventory.", "No modules found!", VNULL, TRUE, TRUE );
    else
        report( (string) (modulesFound) + " Modules found!", "Searching for modules completed!", VNULL, TRUE, TRUE );
}

setApplication()
{
    llSetObjectName( applicationName );
    llSetObjectDesc( applicationDesc );
}

// ----------------------------------------------------
// ---[ Application States ]---------------------------

default
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
          list params = llCSV2List( _id );
           key this   = llGetInventoryKey( llGetScriptName() );
           key script = llList2Key( params, 0 );
           key target = NULL_KEY;
        string param;

        if ( llGetListLength( params ) > 1 )
        {
            if ( llGetListLength( params ) > 2 )
                param = llList2String( params, 2 );

            target = llList2Key( params, 1 );
        }

        if ( debug )
            if ( ( _str == "DIALOG" && script == this ) || ( _str != "DIALOG" ) )
            {
                string text;

                text = llGetScriptName() + "(" + (string) this + ")\n";
                text += "Command received: " + _str + "\n";
                text += "Requester: " + llKey2Name( target ) + "\n";
                text += "Parameters: " + (string) _id + "\n";

                report( text, "Request received: " + _str, <0.3, 0.7, 1.0>, TRUE, FALSE );
            }
            
        if ( _str == "FLIPTITLE" )
        {
            list params = llCSV2List( _id );

            string text = llList2String( params, 2 );
            vector color;
            integer last = FALSE;
            
            if ( llGetListLength( params ) > 3 )
                color = llList2Vector( params, 3 );
                
            if ( llGetListLength( params ) > 4 )
                last = llList2Integer( params, 4 );

            report( "", text, color, last, TRUE );
        }
    }

    on_rez( integer _n )
    {
        setApplication();
        detectModules();
        detectControllers();
    }

    changed( integer _change )
    {
        // Something has changed in inventory, gotta reset all modules informations
        if ( _change & CHANGED_INVENTORY )
        {
            if ( ! permanentConf )
                detectConfiguration();
            else detectModules();
            
            report(
                "",
                "Ready",
                VNULL, TRUE, FALSE
            );
        }
    }

    dataserver( key _queryid, string _data )
    {
        list temp;
        string name;
        string value;

        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( configuration, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    report(
                        "",
                        "Configuration reading... " + (string) percent( notecardLine, notecardTotalLines ) + "%",
                        VNULL, FALSE, FALSE
                    );

                    temp = llParseString2List( _data, ["="], [] );
                    name = llStringTrim( llToLower( llList2String( temp, 0 ) ), STRING_TRIM );
                    value = llStringTrim( llList2String( temp, 1 ), STRING_TRIM );

                    if ( name == "application_name" )
                    {
                        applicationName = value;
                        setApplication();
                    }

                    else if ( name == "application_desc" )
                    {
                        applicationDesc = value;
                        setApplication();
                    }

                    else if ( name == "application_header" )
                        header = value;

                    else if ( name == "configuration_note" )
                        configuration = value;

                    else if ( name == "owner_only" )
                        ownerOnly = (integer) value;

                    else if ( name == "debug" )
                        debug = (integer) value;

                    else if ( name == "menuStatus" )
                        menuStatus = (integer) value;

                    else if ( name == "permanent_configuration" )
                        permanentConf = (integer) value;

                    else if ( name == "linked_messages_target" )
                        linkedMsgTarget = (integer) value;

                    key target;

                    if ( ownerOnly ) target = llGetOwner();
                    else target = NULL_KEY;

                    llListenRemove( dialogsChannel );
                    llListen(
                        dialogsChannel,
                        "",
                        target,
                        ""
                            );
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( configuration, notecardLine );
            }

        else {
            report(
                "Configuration reading completed!",
                "Configuration reading completed!",
                VNULL, FALSE, TRUE
                    );
                    
            detectModules();
        }
    }

    state_entry()
    {
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        
        key target;
        if ( ownerOnly ) target = llGetOwner();
        else target = NULL_KEY;

        llListen(
            dialogsChannel,
            "",
            target,
            ""
                );

        llListen(
            controllersResponsesChannel,
            "",
            "",
            ""
                );
        
        llListen(
            0,
            "",
            llGetOwner(),
            "musica maestro"
        );
    }

    touch_start( integer _total_number )
    {
        action = 0;
        if( llDetectedKey( 0 ) == llGetOwner( ) || ! ownerOnly )
            menu( llDetectedKey( 0 ) );
    }

    listen( integer _chan, string _name, key _id, string _request )
    {
        processRequest( _chan, _name, _id, _request );
    }
}