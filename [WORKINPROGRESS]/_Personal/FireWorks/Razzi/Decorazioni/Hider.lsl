default
{
    link_message( integer sibling, integer num, string mesg, key target_key ) {
        if ( mesg == "Hide" ) { // this message isn't for me.  Bail out.
            llSetAlpha( 0.0, ALL_SIDES );
        } else if ( mesg == "Show" ) {            
            llParticleSystem([]);
            llSetText("", <1,1,1>, 1.0);
            llSetTextureAnim(FALSE | SMOOTH | LOOP, ALL_SIDES, 1, 1, 0, 0, 0.0);
            llTargetOmega(<0,0,0>, 0, 0);
            llSetAlpha( 1.0, ALL_SIDES );
        }
    }
}
