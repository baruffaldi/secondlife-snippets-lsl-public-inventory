integer debug = FALSE;
integer die = FALSE;
integer random = TRUE;
integer randomChanger = FALSE;
integer fireWorksChain = FALSE;
float duration = 3;
float durationSecond = 3;

list order_buttons(list buttons)
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4)
         + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}
 
vector getPos(float heightToAdd)
{
    vector pos   = llDetectedPos(0);
    rotation rot = llDetectedRot(0);
    vector offset = <0, 0, heightToAdd>;
    vector avOffset = offset * rot;
    pos += avOffset;
    
    return pos;
}

integer randInt(integer n)
{
    return (integer)llFrand(n + 1);
}

integer rand(integer min, integer max)
{
    return min + randInt(max - min);
}

gogogo( integer higher, float time )
{    
    vector original;
    original += llGetPos( );
    
    original += <.0,.0, (float)higher>;
    
    llMoveToTarget(
        original,
        time
    );
    
    llSleep(2.3);
    llStopMoveToTarget();
        
    llSetAlpha( 0.0, ALL_SIDES );
    llMessageLinked( -1, 0, "Hide", NULL_KEY );
    
    llMessageLinked( -1, 1, "FireWorks Deluxe", NULL_KEY );
    
    llMessageLinked( LINK_THIS, 0, "FireWorks Deluxe", NULL_KEY );
    
    llSetStatus(STATUS_PHYSICS, TRUE);
    
    
    if ( duration )
        llSleep( duration );
    else
        llSleep( 3 );
    
    original -= <.0,.0, (float)higher>;
    
    llMessageLinked( -1, 1, "End", NULL_KEY );
    llSleep(durationSecond);
    llMessageLinked( -1, 0, "FireWorks Deluxe", NULL_KEY );
    llSleep(durationSecond);
    llMessageLinked( -1, 0, "End", NULL_KEY );    
    llSetAlpha( 1.0, ALL_SIDES );
    
    if ( die ) llDie();
    else llMoveToTarget(
            original,
            ( time * 3 )
        );
    llSetAlpha( 1.0, ALL_SIDES );
    llMessageLinked( -1, 0, "Show", NULL_KEY );
}

default
{
    on_rez( integer _n )
    {
        llParticleSystem([]);
        llSetText("", <1,1,1>, 1.0);
        llSetTextureAnim(FALSE | SMOOTH | LOOP, ALL_SIDES, 1, 1, 0, 0, 0.0);
        llTargetOmega(<0,0,0>, 0, 0);
        llSetAlpha( 1.0, ALL_SIDES );
        // Reset particles effects etc etc
        llMessageLinked( -1, 0, "Show", NULL_KEY );
        
        if ( fireWorksChain )
        {
            llMessageLinked( LINK_THIS, 1, "FireWorks Deluxe", NULL_KEY );
            
            integer z;
            float time = 1.3;
            
            if ( random ) z = rand( 20, 30 );
            else z = 23;
            
            if ( debug ) z = 1;
            if ( debug ) time = 1;
            
            gogogo( z, time );
        } else {
            llMessageLinked( LINK_THIS, 0, "FireWorks Deluxe", NULL_KEY );
        }
    }
    
    state_entry()
    {
        if ( randomChanger ) llSetTimerEvent( 3 );
        else llSetTimerEvent( 0 );
        
        llSetAlpha( 1.0, ALL_SIDES );
        if ( fireWorksChain )
        {
            llMessageLinked( LINK_THIS, 1, "FireWorks Deluxe", NULL_KEY );
            
            integer z;
            float time = 1.3;
            
            if ( random ) z = rand( 10, 20 );
            else z = 23;
            
            if ( debug ) z = 1;
            if ( debug ) time = 1;
            
            gogogo( z, time );
        } else {
            llMessageLinked( LINK_THIS, 0, "FireWorks Deluxe", NULL_KEY );
        }
    }
    
    timer()
    {
        if ( random ) random = FALSE;
        else random = TRUE;
    }

    touch_start(integer total_number)
    {
        llMessageLinked( LINK_THIS, 1, "FireWorks Deluxe", NULL_KEY );
        if ( llDetectedKey(0) == llGetOwner() )
        {
            integer z;
            float time = 1.3;
            
            if ( random ) z = rand( 15, 30 );
            else z = 23;
            
            if ( debug ) z = 1;
            if ( debug ) time = 1;
            
            gogogo( z, time );
        }
    }
}