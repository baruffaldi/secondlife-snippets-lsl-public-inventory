// default heigth
float defaultHeigth = 15.0;
// default moving time
float movingTime = 1.7;
// heigth = 3
integer debug = FALSE;
// heigth = random
integer random = TRUE;
// random min
float heigthRandMin = 10.0;
// random max
float heigthRandMax = 20.0;
// -----------------------------------------------
// 1 = autostart
integer fireWorksChain = TRUE;
// 1 = llDie() - 0 = llMoveToTarget()
integer die = TRUE;
// -----------------------------------------------
// Duration first particles
float duration = 0.5;
// Duration second particles
float durationSecond = 0.5;
// First particles rotation cycles
integer firstCycles = 1;
// Second particles rotation cycles
integer secondCycles = 1;
// -----------------------------------------------
string explosionSound = "e95c96a5-293c-bb7a-57ad-ce2e785ad85f";



vector VNULL = <0.0, 0.0, 0.0>;
integer running = FALSE;
float heigth;


integer randInt(integer n)
{
    return (integer)llFrand(n + 1);
}

integer rand(integer min, integer max)
{
    return min + randInt(max - min);
}

float randFloat(float n)
{
    return llFrand(n + 1);
}

float Frand(float min, float max)
{
    return min + randFloat(max - min);
}

// -----------------------------------------------

goDown()
{
    llSetStatus( STATUS_PHYSICS, TRUE );
    vector original = llGetPos( );
    original -= <.0, .0,  ( heigth + 5.0 )>;
    
    llMoveToTarget(
        original,
        movingTime
    );
    
    llSleep( movingTime * 2 );
}

explosion( string controller, integer status )
{
    llMessageLinked( -1, status, controller, NULL_KEY );
}

visible( integer visible )
{
    if ( visible ) {
        llMessageLinked( -1, 0, "Show", NULL_KEY );
        llSetText("", <1,1,1>, 1.0);
        llSetTextureAnim(FALSE | SMOOTH | LOOP, ALL_SIDES, 1, 1, 0, 0, 0.0);
        llTargetOmega(<0,0,0>, 0, 0);
        llSetAlpha( 1.0, ALL_SIDES );
    } else {
        llMessageLinked( -1, 0, "Hide", NULL_KEY );
        llSetText("", <.0,.0,.0>, 0.0);
        llSetTextureAnim(FALSE | SMOOTH | LOOP, ALL_SIDES, 1, 1, 0, 0, 0.0);
        llTargetOmega(<0,0,0>, 0, 0);
        llSetAlpha( 0.0, ALL_SIDES );
    }
}

cycle( integer cycles )
{    
    integer i;
    integer s;
    for ( ; i < 8; ++i )
    {
        llSetRot( <(float) (i*3/5), (float) i, -0.19867, 0.98007> );
        if ( i == 7 && s < cycles ) {
            i = 0;
            ++s;
        }
    }
}

start()
{
    if ( random ) heigth = Frand( heigthRandMin, heigthRandMax );
    else heigth = defaultHeigth;
    if ( debug ) heigth = 1.0;

    running = TRUE;
    explosion( "Razzo", TRUE );
    goUp();
    llSleep( movingTime );
    
    llPlaySound( explosionSound, 1.0 );
    explosion( "Razzo", FALSE );
    visible( FALSE );
    
    explosion( "FireWorks Deluxe", TRUE );
    llSleep( duration );
    cycle( firstCycles );
    
    explosion( "End", TRUE );
    llSleep( durationSecond );
    cycle( secondCycles );
    
    explosion( "FireWorks Deluxe", FALSE );
    llSleep( durationSecond );
    explosion( "End", FALSE );

    if ( die ) llDie();
    goDown();
    stop();
}

goUp()
{    
    llSetRot( <0.00000, 90.00000, -0.19867, 0.98007> );
    llSetStatus( STATUS_PHYSICS, TRUE );
    
    vector original = llGetPos();
    original += <.0, .0, heigth>;
    
    llMoveToTarget(
        original,
        movingTime
    );
}

stop()
{
    running = FALSE;
    llSetStatus( STATUS_PHYSICS, FALSE );
    llStopMoveToTarget();
    reset();
}

reset()
{
    llParticleSystem([]);
    explosion( "Razzo", FALSE );
    explosion( "FireWorks Deluxe", FALSE );
    explosion( "End", FALSE );
    visible( TRUE );
    llResetScript();
}

default
{
    on_rez( integer _n )
    {
        llSetRot( <0.00000, 90.00000, -0.19867, 0.98007> );
        llPreloadSound( explosionSound );
        if ( fireWorksChain ) start();
    }

    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == llGetOwner() )
            if ( running ) stop();
            else start();
    }
}