// Modularizer v0.7 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ F.B. <bornslippyRuby@coolminds.org>
// Licensed under the GNU GPLv3 License
// http://www.gnu.org
//
 
// ---[ Default Application Settings ]-----------------

string moduleName    = "FireWorks Deluxe - Rockets Module";
string moduleDesc    = "version: 2.1";
string notecardName  = "Rockets-Default";
string header        = "*** Rockets Module ***";

integer ownerOnly       = FALSE;
integer debug           = FALSE;
integer menuStatus      = TRUE;
integer permanentConf   = TRUE;
integer linkedMsgTarget = -4;

// ----------------------------------------------------
// ---[ Environment Variables ]------------------------

    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;
integer dialogsChannel = -1247689023;
integer serviceStatus;
integer action;
 vector VNULL;

// ----------------------------------------------------
// ---[ Custom Variables ]-----------------------------
list settings = [];

integer detonatorChannel = -1286429002;
// First rocket throwing delay
float timez = 5.0;
// Rockets throwing delay
float time_min = 0.5;
float time_max = 3.0;
// Throw random rockets
integer randomRazzo = FALSE;
// Total throwing time
integer totalTime = 20;
// Rockets random throwing rate ( 1-10 )
integer frequency = 7;

list razzi = [];
string razzo;
// ----------------------------------------------------
// ---[ Modularizer Functions ]------------------------

integer getIntegerParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2Integer( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return 0;
}

string getStringParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2String( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return "";
}

float getFloatParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2Float( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return 0.0;
}

key getKeyParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2Key( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return NULL_KEY;
}

vector getVectorParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2Vector( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return <0.0, 0.0, 0.0>;
}

rotation getRotationParam( string param )
{
    if ( llListFindList( settings, [param] ) != -1 )
        return llList2Rot( settings, ( llListFindList( settings, [param] ) + 1 ) );
    else return <0.0, 0.0, 0.0, 0.0>;
}

integer randInt(integer n)
{
    return (integer)llFrand(n + 1);
}

integer rand(integer min, integer max)
{
    return min + randInt(max - min);
}

float randFloat(float n)
{
    return llFrand(n + 1);
}

float Frand(float min, float max)
{
    return min + randFloat(max - min);
}

menu( key _target )
{
    if ( menuStatus )
    {
        if ( _target == NULL_KEY ) _target = llGetOwner( );
    
        list buttons = ["Options", "List"];
        string desc  = "Please choose an option below:";
        
        llDialog(
            _target,
            header + "\n" + desc,
            buttons,
            dialogsChannel
        );
    }
}

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llMessageLinked( linkedMsgTarget, 0, "FLIPTITLE", llList2CSV( [NULL_KEY, NULL_KEY, notecardName + ": " + _fliptitle, _color, _last, _pause] ) );
        
    if ( _text != "" )
        llInstantMessage( llGetOwner(), header + "\n" + _text );
}

reportDialog( key _dest, string _text, list _buttons )
{
    if ( _dest == NULL_KEY ) _dest == llGetOwner();

    llDialog(
        _dest,
        header + "\n" + _text,
        _buttons,
        dialogsChannel );
}

reset()
{
    report( "The module " + llList2String( parseScriptName( llGetScriptName() ), 1 ) + "'s settings has been reset!", "", VNULL, FALSE, FALSE );
    llResetScript( );
    
}

readConfiguration()
{
    action = 0;
    razzi = [];
    integer i;
    for( ; i < llGetInventoryNumber( INVENTORY_OBJECT ); ++i )
        razzi += llGetInventoryName( INVENTORY_OBJECT, i );

    if ( llGetInventoryNumber( INVENTORY_OBJECT ) )        
        report( (string) llGetInventoryNumber( INVENTORY_OBJECT ) + " FireWorks found!", (string) llGetInventoryNumber( INVENTORY_OBJECT ) + " FireWorks found!", VNULL, TRUE, TRUE );
    
    if ( ! permanentConf )
    {
        if ( notecardName == "" )
            notecardName = llList2String( parseScriptName( llGetScriptName() ), 1 ) + "-Default";
            
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                report(
                    "Configuration found! Leeching settings in progress...",
                    "Configuration reading... 0%",
                    VNULL, FALSE, FALSE
                );

                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );

                return;
            }
    }
}

integer processRequest( integer _chan, string _name, key _id, string _request )
{
    list buttons;
    
    if ( llList2String( llCSV2List( _request ), 0 ) == "RocketRequest" )
    {
        list data = llCSV2List( _request );
        string nome = llList2String( data, 1 );
        integer random;
        
        if ( nome == "*" ) random = TRUE;
        else random = FALSE;
        
        if ( llGetListLength( data ) > 2 )
        {
            if ( llGetListLength( data ) > 3 )
            {
                time_min = llList2Integer( data, 3 );
                if ( llGetListLength( data ) > 4 )
                    time_max = llList2Integer( data, 4 );
                else time_max = ( time_min * 3 ) / 2;
            }
            
            totalTime = llList2Integer( data, 2 );
        }
        
        start( nome, random );
        
        llSay( 0, "Richiesta da detonatore ricevuta" );
    }
    
    else if ( _request == "PING" )
        llSay( detonatorChannel, "PONG" );
    
    else if ( _request == "Options" && llGetOwner() == _id )
    {
        action = 0;
        buttons = ["Show"];
        if ( ! permanentConf )
            buttons += ["Load", "Save", "Reset"];

        llDialog(
            llGetOwner(),
            header + "\nChoose an option below:",
            buttons,
            dialogsChannel
        );
    }
    
    else if ( _request == "List" ) rocketsList( _id );
    
    else if ( _request == "Load" && llGetOwner() == _id )
    {
        action = 1;
        integer allNotecards = llGetInventoryNumber( INVENTORY_NOTECARD );
        integer i;
        string this = llList2String( parseScriptName( llGetScriptName() ), 1 );
        
        for ( ; i < allNotecards; ++i )
        {
            list conf = parseScriptName( llGetInventoryName( INVENTORY_NOTECARD, i ) );
            if ( llList2String( conf, 0 ) == this )
                if ( llGetListLength( conf ) > 1 )
                    buttons += llList2String( conf, 1 );
        }
        
        if ( ! llGetListLength( buttons ) )
            buttons += "Default";
        
        llDialog(
            _id,
            header + "\nChoose the configuration note to load:",
            buttons,
            dialogsChannel
        );
    }
    
    else if ( action == 1 && llGetOwner() == _id )
    {
        readConfiguration();
    }
    
    else if ( _request == "Save" && llGetOwner() == _id )
    {
        action = 0;
        string config = "\n\n\n#\n# Module: " + moduleName + "\n# Configuration Scheme: " + notecardName + "\n#\n";
        
        config += "\nmodule_name = " + moduleName;
        config += "\nmodule_desc = " + moduleDesc;
        config += "\nmodule_header = " + header;
        config += "\nowner_only = " + (string) ownerOnly;
        config += "\ndebug = " + (string) debug;
        config += "\nmenuStatus = " + (string) menuStatus;
        config += "\npermanent_configuration = " + (string) permanentConf;
        config += "\nlinked_messages_target = " + (string) linkedMsgTarget;
        config += "\n#";
        
        integer i;
        for ( ; i < llGetListLength( settings ); ++i )
        {
            config += "\n" + llList2String( settings, i ) + " = " + llList2String( settings, ( i + 1 ) );
            ++i;
        }

        config += "\n\n\n#\n# Save all this lines in a notecard named '" + notecardName + "' and put it inside the " + moduleName + "'s inventory.\n#\n";
        
        llInstantMessage( _id, config );
    }

    else if ( _request == "Options" && llGetOwner() != _id )
        llDialog(
            _id,
            header + "\nI'm afraid, but only the owner is able to change the module settings.",
            [],
            dialogsChannel
            );

    else if ( _request == "Show" )
    {
        action = 0;
        string desc;
        desc = "Settings:\n- Name: " + moduleName
             + "\n- Header: " + header
             + "\n- Owner Only: " + (string) ownerOnly
             + "\n- Menu: " + (string) menuStatus;

        if ( debug )
            desc += "\n- Debug: " + (string) debug;
            
        if ( ! permanentConf )
            desc += "\n- Configuration Note: " + notecardName
                  + "\n- Permanent Configuration: " + (string) permanentConf 
                  + "\n- Linked Messages Target: " + (string) linkedMsgTarget;

        integer i;
        for ( ; i < llGetListLength( settings ); ++i )
        {
            desc += "\n- " + llList2String( settings, i ) + ": " + llList2String( settings, ( i + 1 ) );
            ++i;
        }
        
        report( desc, "", VNULL, FALSE, FALSE );
    }

    else if ( _request == "Reset" && ! permanentConf )
        llDialog(
            _id,
            header + "\nAre you really really sure about what you're doing? This will reset all loaded modules settings...\n\nIf you're 100% sure, then press \"Sure!\" else just press \"ignore\"!",
            ["Sure!"],
            dialogsChannel
        );

    else if ( _request == "Sure!" && ! permanentConf )
        reset();
    
    else if ( _request == "On" )    
        return 1;

    else if ( _request == "Off" )    
        return 2;
    
    else if ( _request == "OK" )
        return -2;
    
    else if ( _request == "Random" )
        return -3;
        
    else if ( action == 3 )
    {
        action = 0;
        return -1;
    }
    
    return 0;
}

list parseScriptName( string _script )
{
    return llParseString2List( _script, ["-"], [] );
}
      
list ListStridedUpdate(list dest, list src, integer start, integer end, integer stride) {
    return llListReplaceList( dest, src, start * stride, ( ( end + 1 ) * stride ) - 1 );
}

integer percent( integer n, integer hundreds )
{    
    return ( n * 100 ) / hundreds;
}

// ----------------------------------------------------
// ---[ Custom Functions ]-----------------------------

rocketsList( key _target )
{
    action = 3;
    if ( _target == NULL_KEY ) _target = llGetOwner( );

    list buttons;
    string desc  = "Please choose the rocket type:";

    if ( serviceStatus ) buttons += "Off";

    buttons += "Random";
    
    integer i;
    integer t = llGetListLength( razzi );
    
    if ( t > 0 )
        for ( ; i < t; ++i )
            buttons += llList2String( razzi, i );
    else {
        buttons = [];
        desc  = "There is no rocket inside this controller's inventory";
    }
    
    llDialog(
        _target,
        header + "\n" + desc,
        buttons,
        dialogsChannel
    );
}

start( string _rocket, integer _random )
{
    randomRazzo = _random;
    razzo = _rocket;
    serviceStatus = TRUE;
    llSetTimerEvent( timez );
    llResetTime();
}

stop( )
{
    randomRazzo = FALSE;
    razzo = "";
    serviceStatus = FALSE;
    llSetTimerEvent( 0 );
    action = 0;
}

// ----------------------------------------------------
// ---[ Application States ]---------------------------

default
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
          list params = llCSV2List( _id );
           key this   = llGetInventoryKey( llGetScriptName() );
           key script = llList2Key( params, 0 );
           key target = NULL_KEY;
        string param;

        if ( llGetListLength( params ) > 1 )
        {
            if ( llGetListLength( params ) > 2 )
                param = llList2String( params, 2 );

            target = llList2Key( params, 1 );
        }

        if ( debug )
            if ( ( _str == "DIALOG" && script == this ) || ( _str != "DIALOG" ) )
            {
                string text;

                text = llGetScriptName() + "(" + (string) this + ")\n";
                text += "Command received: " + _str + "\n";
                text += "Requester: " + llKey2Name( target ) + "(" + (string) script + ")\n";
                text += "Parameters: " + param + "\n";

                report( text, "Request received: " + _str, VNULL, FALSE, FALSE );
            }

        if ( _str == "RESETSCRIPT" )
        {
            reset();
        }

        else if ( script == this )
            if ( _str == "DIALOG" )
                menu( target );
    }

    state_entry()
    {
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            dialogsChannel,
            "",
            "",
            ""
        );
    }

    on_rez( integer _n )
    {
        
    }

    changed( integer _change )
    {
        // Something has changed in inventory, maybe we got new settings to import!
        if ( _change & CHANGED_INVENTORY )
            readConfiguration( );
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                list temp;
                string name;
                string value;

                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    integer percent;
                    percent = ( notecardLine * 100 ) / notecardTotalLines;

                    report(
                        "",
                        "Configuration reading... " + (string) percent + "%",
                        VNULL, FALSE, FALSE
                    );

                    temp = llParseString2List( _data, ["="], [] );
                    name = llStringTrim( llToLower( llList2String( temp, 0 ) ), STRING_TRIM );
                    value = llStringTrim( llList2String( temp, 1 ), STRING_TRIM );

                    if ( name == "module_name" )
                        moduleName = value;

                    else if ( name == "module_desc" )
                        moduleDesc = value;

                    else if ( name == "module_header" )
                        header = value + "\n";

                    else if ( name == "owner_only" )
                    {
                        ownerOnly = (integer) value;
                        
                        key target;
                        if ( ownerOnly ) target = llGetOwner();
                        else target = NULL_KEY;
    
                        llListenRemove( dialogsChannel );
                        llListen(
                            dialogsChannel,
                            "",
                            target,
                            ""
                        );
                    }

                    else if ( name == "debug" )
                        debug = (integer) value;

                    else if ( name == "menu_status" )
                        menuStatus = (integer) value;

                    else if ( name == "permanent_configuration" )
                        permanentConf = (integer) value;

                    else if ( name == "linked_messages_target" )
                        linkedMsgTarget = (integer) value;
                        
                    else if ( -1 != llListFindList( settings, [name] ) )
                        settings = ListStridedUpdate( settings, [name, value], 0, 0, 2);
                        
                    else settings += [name, value];
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else 
            report(
                "Configuration reading completed!",
                "Configuration reading completed!",
                VNULL, TRUE, TRUE
            );
    } 

    listen( integer _chan, string _name, key _id, string _request )
    {
        integer result = processRequest( _chan, _name, _id, _request );

        // -1 = not exists
        // 0 = action done
        // 1 = service start
        // 2 = service stop
 
        
        if ( result == -3 )
        {
            start( _request, TRUE );
            action = 0;
        }
        
        else if ( result == -1 )
        {
            start( _request, FALSE );
            action = 0;
        }
        
        else if ( result == 1 )
        {
            start( _request, FALSE );
            action = 0;
        }
    
        else if ( result == 2 )
        {
            stop( );
        }
    }
    
    timer()
    {
        if ( llGetTime() >= totalTime ) {
            stop();
            return;
        } else if ( rand( 1, 10 ) < frequency ) llSetTimerEvent( (float) Frand( time_min, time_max ) );
        
        if ( randomRazzo )
            razzo = llList2String( razzi, rand( 0, llGetListLength(razzi) - 1 ) );
        
        llRezObject( razzo, llGetPos() + <0.0,0.0,1.0>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, 0);
    }
}