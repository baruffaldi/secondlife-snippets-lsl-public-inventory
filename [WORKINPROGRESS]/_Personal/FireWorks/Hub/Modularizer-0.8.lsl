// Modularizer v0.7 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ F.B. <bornslippyRuby@coolminds.org>
// Licensed under the GNU GPLv3 License
// http://www.gnu.org
//

// ---[ Default Application Settings ]-----------------

string applicationName = "FireWorks Deluxe";
string applicationDesc = "v0.7";
string configuration   = "FireWorks-Default";
string header          = "*** FireWorks Deluxe ***";

integer ownerOnly       = TRUE;
integer debug           = FALSE;
integer menuStatus      = TRUE;
integer permanentConf   = TRUE;
integer linkedMsgTarget = -4;

// ----------------------------------------------------
// ---[ Environment Variables ]------------------------

    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;
 string modulesList;
   list modules;
   list modulesKeys;
integer dialogsChannel;
integer action;
 vector VNULL;

// ----------------------------------------------------
// ---[ Custom Variables ]-----------------------------
list settings = ["example_param", 3]; // With default values

// ----------------------------------------------------
// ---[ Modularizer Functions ]------------------------

menu( key _target )
{
    if ( menuStatus )
    {
        if ( _target == NULL_KEY ) _target = llGetOwner( );
        
        integer length = llGetListLength( modules );
        integer i;
        
        list buttons;
        
        if ( _target == llGetOwner( ) )
            buttons = ["Options"];
        
        string desc;
        
        if ( length > 0 )
        {
            if ( length <= 8 )
            {
                desc = "Choose the module to manage";
                for ( ; i < length; ++i )
                    buttons += llList2String( modules, i );
            }
        
            else
            {
                desc = "Choose the modules section";
                for ( i = 1; i <= ( ( length / 9 ) + 1 ); ++i )
                    buttons += (string) i;
            }
        
            desc += " from those specified below or click on \"Options\" to get available settings.\n";
        
        
        } else desc = "Cannot find any " + applicationName + " module inside the script's inventory.";
        
        llDialog(
            _target,
            header + "\n" + desc,
            buttons,
            dialogsChannel
        );
    }
}
        
report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _text != "" )
        llInstantMessage( llGetOwner(), header + "\n" + _text );
        
    if ( _fliptitle != "" )
    {
        if ( _color == VNULL ) _color = <1.0, 1.0, 1.0>;
        llSetText( applicationName + "\n" + _fliptitle, _color, 1.0 );
        if ( _pause ) llSleep(1.5);
        if ( _last ) llSetText( "", VNULL, 0.0 );
    }
}

reportDialog( key _dest, string _text, list _buttons )
{
    if ( _dest == NULL_KEY ) _dest == llGetOwner();

    llDialog(
        _dest,
        header + "\n" + _text,
        _buttons,
        dialogsChannel );
}

reset()
{
    report(
        "",
        "Resetting application...",
        VNULL, FALSE, FALSE );

    llMessageLinked( linkedMsgTarget, 0, "RESETSCRIPT", NULL_KEY );

    llSleep(1.7); // Giving the time to reset all scripts

    report(
        "Welcome to " + applicationName + "!",
        "Welcome!!!\nInizializing...",
        VNULL, TRUE, FALSE
            );
}

detectConfiguration()
{
    report(
        "",
        "Update detected",
        VNULL, FALSE, FALSE
            );
    
    if ( llGetInventoryType( configuration ) == INVENTORY_NOTECARD )
        if ( llGetInventoryPermMask( configuration, MASK_OWNER ) )
        {
            report(
                "Configuration '" + configuration + "' found! Leeching settings in progress...",
                "Configuration reading... 0%",
                VNULL, FALSE, FALSE
                    );

            notecardLine = 0;
            notecardTotalLinesId = llGetNumberOfNotecardLines( configuration );

            return;
        }

    report(
        "Configuration '" + configuration + "' not found!\nUsing current settings...",
        "Using current settings...",
        VNULL, FALSE, TRUE
            );
            
    detectModules();
}

integer processRequest( integer _chan, string _name, key _id, string _requestedModule )
{
    string desc;
    list buttons;
    
    if ( _requestedModule == "Options" && llGetOwner() == _id )
    {
        if ( ! permanentConf ) buttons += ["Load", "Save"];
        buttons += ["Show", "Modules", "Reset"];

        llDialog(
            _id,
            header + "\nChoose an option below:",
            buttons,
            dialogsChannel
        );
    }

    else if ( _requestedModule == "Load" && llGetOwner() == _id )
    {
        action = 1;
        integer all_notecards = llGetInventoryNumber( INVENTORY_NOTECARD );
        integer i;
        
        for ( ; i < all_notecards; ++i )
        {
            list conf = parseScriptName( llGetInventoryName( INVENTORY_NOTECARD, i ) );
            if ( llList2String( conf, 0 ) == "Modularizer" )
                if ( llGetListLength( conf ) > 1 )
                    buttons += llList2String( conf, 1 );
                else buttons += "Default";
        }
        
        if ( ! llGetListLength( buttons ) )
            buttons += "Default";
        
        llDialog(
            _id,
            header + "\nChoose the configuration note to load:",
            buttons,
            dialogsChannel
        );
    }
    
    else if ( action == 1 && llGetOwner() == _id )
    {
        configuration = "Modularizer-" + _requestedModule;
        detectConfiguration();
    }
    
    else if ( _requestedModule == "Save" && llGetOwner() == _id )
    {   
        string settings = "\n\n\n#\n# " + applicationName + " Configuration\n# Configuration Scheme: " + configuration + "\n#\n";
        
        settings += "\napplication_name = " + applicationName;
        settings += "\napplication_desc = " + applicationDesc;
        settings += "\napplication_header = " + header;
        settings += "\nowner_only = " + (string) ownerOnly;
        settings += "\ndebug = " + (string) debug;
        settings += "\nmenu_status = " + (string) menuStatus;
        settings += "\npermanent_configuration = " + (string) permanentConf;
        settings += "\linked_messages_target = " + (string) linkedMsgTarget;

        settings += "\n\n\n#\n# Save all this lines in a notecard named '" + configuration + "' and put it inside the " + applicationName + "'s inventory.\n#\n";
        
        llInstantMessage( _id, settings );
    }

    else if ( _requestedModule == "Options" && llGetOwner() != _id )
        llDialog(
            _id,
            header + "\nI'm afraid, but only the owner is able to change the application settings.",
            [],
            dialogsChannel
            );

    else if ( _requestedModule == "Modules" )
    {
        if ( llGetListLength( modulesKeys ) > 0 )
            report( "Loaded modules:\n" + modulesList, "", VNULL, FALSE, FALSE );
        else
            report( "No modules found", "", VNULL, FALSE, FALSE );
    }
    
    else if ( _requestedModule == "Show" )
    {
        desc = "Settings:\n- Name: " + applicationName + "\n- Header: " + header + "\n- Owner Only: " + (string) ownerOnly + "\n- Menu: " + (string) menuStatus;

        if ( debug )
            desc += "\n- Debug: " + (string) debug;
        if ( ! permanentConf )
            desc += "\n- Configuration Note: " + configuration + "\n- Permanent Configuration: " + (string) permanentConf + "\n- Linked Messages Target: " + (string) linkedMsgTarget;

        integer i;
        for ( ; i < llGetListLength( settings ); ++i )
        {
            desc += "\n- " + llList2String( settings, i ) + ": " + llList2String( settings, ( i + 1 ) );
            ++i;
        }

        report( desc, "", VNULL, FALSE, FALSE );
    }

    else if ( _requestedModule == "Reset" )
        llDialog(
            _id,
            header + "\nAre you really really sure about what you're doing? This will reset all loaded modules settings...\n\nIf you're sure 100%, then press \"Sure!\" else just press \"ignore\"!",
            ["Sure!"],
            dialogsChannel
            );

    else if ( _requestedModule == "Sure!" )
        reset();

    else if ( (integer) _requestedModule > 0 )
    {
        integer section = (integer) _requestedModule;
        integer length  = llGetListLength( modules );
        integer offset;
        integer end;

        if ( section == 1 )
            offset = 0;
        else offset = ( section - 1 ) * 9;

        end = ( offset + 9 );

        for ( ; offset < end; ++offset )
            if ( offset < length )
                buttons += llList2String( modules, offset );

        llDialog(
            _id,
            header + "\nChoose the module to manage from those specified below:",
            buttons,
            dialogsChannel
        );
    }

    else llMessageLinked( linkedMsgTarget, 0, "DIALOG", llList2CSV( [ getModuleKeyByModuleName( _requestedModule ), _id ] ) );

    return 0;
}

list parseScriptName( string _script )
{
    return llParseString2List( _script, ["-"], [] );
}
      
list ListStridedUpdate(list dest, list src, integer start, integer end, integer stride) {
    return llListReplaceList( dest, src, start * stride, ( ( end + 1 ) * stride ) - 1 );
}

integer percent( integer n, integer hundreds )
{    
    return ( n * 100 ) / hundreds;
}

// ----------------------------------------------------
// ---[ Custom Functions ]-----------------------------

key getModuleKeyByModuleName( string _moduleName )
{
    return llList2String( modulesKeys, llListFindList( modules, [_moduleName] ) );
}

detectModules()
{
    modules = [];
    modulesKeys = [];
    modulesList = "";

    list inventory;

    string  this = llGetScriptName();

    integer inventoryScripts = llGetInventoryNumber( INVENTORY_SCRIPT );
    integer i = 0;
    integer modulesFound = 0;

    report( "", "Searching for modules... 0%", VNULL, FALSE, FALSE );

    for ( ; i < inventoryScripts; ++i )
    {
        report( "", "Searching for modules... " + (string) percent( i, inventoryScripts ) + "%", VNULL, FALSE, FALSE );
        string script = llGetInventoryName( INVENTORY_SCRIPT, i );
        key scriptKey = llGetInventoryKey( script );
        list scriptInfo = parseScriptName( script );

        // Check:
        if( script != this && // it's not this script
            llGetInventoryPermMask( script, MASK_OWNER ) && // the owner is this scripts owner
                llListFindList( modulesKeys, [scriptKey] ) == -1 && // it's not already loaded
                    llList2String( scriptInfo, 0 ) == applicationName ) // it's a module
                    {
                        integer length = llGetListLength( scriptInfo );

                        string module = llList2String( scriptInfo, 1 );
                        string version;

                        if ( length > 2 )
                            version = llList2String( scriptInfo, 2 );
                        else version = "0.0.0";

                        ++modulesFound;

                        modules += [ module ];
                        modulesKeys += [ scriptKey ];
                        modulesList += "- " + module + " (version: " + version + ") (author: " + llKey2Name( llGetInventoryCreator( script ) ) + ")";
                        if ( debug )
                            modulesList += " (uuid: " + (string ) scriptKey + ") (script: " + script + ")";
                        modulesList += "\n";
                    }
    }

    if ( modulesFound < 1 )
        report( "Cannot find any " + applicationName + " module inside the script's inventory.", "No modules found!", VNULL, TRUE, TRUE );
    else
        report( (string) (modulesFound) + " Modules found!", "Searching for modules completed!", VNULL, TRUE, TRUE );
}

setApplication()
{
    llSetObjectName( applicationName );
    llSetObjectDesc( applicationDesc );
}

// ----------------------------------------------------
// ---[ Application States ]---------------------------

default
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
          list params = llCSV2List( _id );
           key this   = llGetInventoryKey( llGetScriptName() );
           key script = llList2Key( params, 0 );
           key target = NULL_KEY;
        string param;

        if ( llGetListLength( params ) > 1 )
        {
            if ( llGetListLength( params ) > 2 )
                param = llList2String( params, 2 );

            target = llList2Key( params, 1 );
        }

        if ( debug )
            if ( ( _str == "DIALOG" && script == this ) || ( _str != "DIALOG" ) )
            {
                string text;

                text = llGetScriptName() + "(" + (string) this + ")\n";
                text += "Command received: " + _str + "\n";
                text += "Requester: " + llKey2Name( target ) + "\n";
                text += "Parameters: " + (string) _id + "\n";

                report( text, "Request received: " + _str, <0.3, 0.7, 1.0>, TRUE, FALSE );
            }
            
        if ( _str == "FLIPTITLE" )
        {
            list params = llCSV2List( _id );

            string text = llList2String( params, 2 );
            vector color;
            integer last = _num;
            integer pause = FALSE;
            
            if ( llGetListLength( params ) > 3 )
                color = llList2Vector( params, 3 );
                
            if ( llGetListLength( params ) > 4 )
                pause = llList2Integer( params, 4 );

            report( "", text, color, last, pause );
        }
    }

    on_rez( integer _n )
    {
        setApplication();
    }

    changed( integer _change )
    {
        // Something has changed in inventory, gotta reset all modules informations
        if ( _change & CHANGED_INVENTORY )
        {
            if ( ! permanentConf )
                detectConfiguration();
            else detectModules();
            
            report(
                "",
                "Ready",
                VNULL, TRUE, FALSE
            );
        }
    }

    dataserver( key _queryid, string _data )
    {
        list temp;
        string name;
        string value;

        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( configuration, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    report(
                        "",
                        "Configuration reading... " + (string) percent( notecardLine, notecardTotalLines ) + "%",
                        VNULL, FALSE, FALSE
                    );

                    temp = llParseString2List( _data, ["="], [] );
                    name = llStringTrim( llToLower( llList2String( temp, 0 ) ), STRING_TRIM );
                    value = llStringTrim( llList2String( temp, 1 ), STRING_TRIM );

                    if ( name == "application_name" )
                    {
                        applicationName = value;
                        setApplication();
                    }

                    else if ( name == "application_desc" )
                    {
                        applicationDesc = value;
                        setApplication();
                    }

                    else if ( name == "application_header" )
                        header = value;

                    else if ( name == "configuration_note" )
                        configuration = value;

                    else if ( name == "owner_only" )
                        ownerOnly = (integer) value;

                    else if ( name == "debug" )
                        debug = (integer) value;

                    else if ( name == "menu_status" )
                        menuStatus = (integer) value;

                    else if ( name == "permanent_configuration" )
                        permanentConf = (integer) value;

                    else if ( name == "linked_messages_target" )
                        linkedMsgTarget = (integer) value;

                    key target;

                    if ( ownerOnly ) target = llGetOwner();
                    else target = NULL_KEY;

                    llListenRemove( dialogsChannel );
                    llListen(
                        dialogsChannel,
                        "",
                        target,
                        ""
                            );
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( configuration, notecardLine );
            }

        else {
            report(
                "Configuration reading completed!",
                "Configuration reading completed!",
                VNULL, FALSE, TRUE
                    );
                    
            detectModules();
        }
    }

    state_entry()
    {
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        
        key target;
        if ( ownerOnly ) target = llGetOwner();
        else target = NULL_KEY;

        llListen(
            dialogsChannel,
            "",
            target,
            ""
                );
    }

    touch_start( integer _total_number )
    {
        action = 0;
        if( llDetectedKey( 0 ) == llGetOwner( ) || ! ownerOnly )
            menu( llDetectedKey( 0 ) );
    }

    listen( integer _chan, string _name, key _id, string _request )
    {
        processRequest( _chan, _name, _id, _request );
    }
}