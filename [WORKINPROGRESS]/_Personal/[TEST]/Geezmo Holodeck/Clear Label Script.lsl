integer DEBUG = FALSE;
integer PROTOTYPE_CHANNEL = 1000;
integer TEXTURE_CHANNEL = -68193;
integer PROTOTYPE_SCRIPTS = -68192;
float gap = 15.0;
float counter = 0.0;
string object;
string ALPHA_TEXTURE = "bd7d7770-39c2-d4c8-e371-0342ecf20921";
 
default
{
 
    on_rez(integer total_number)
    {
        llShout(PROTOTYPE_CHANNEL, "CLEAR");
        object = llGetObjectName();
        llShout(TEXTURE_CHANNEL, "image " + object);
        llSetTexture(ALPHA_TEXTURE,ALL_SIDES);
        llSetTimerEvent(gap);
    }
 
    timer()
    {
        counter = counter + gap; 
        if (DEBUG) llSay(0, (string)counter+" seconds have passed i will now terminate");
        llDie();
    }
}