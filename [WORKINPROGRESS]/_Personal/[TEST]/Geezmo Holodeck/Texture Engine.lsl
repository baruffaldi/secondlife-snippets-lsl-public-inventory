 
// .::Prototype::.
//
// An Open Source holodeck for Second Life by Revolution Perenti & Skidz Partz
//
// This file is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
integer TEXTURE_CHANNEL = -68193;
string TEXTURE_NOTECARD;
string prefix = "tex_";
key TextureQuery;
list csv_commands ;
integer iLine = 0;
integer DEBUG = FALSE; // debug channel
integer key_listen;  // listen key
 
llDebugSay( string message ) 
{
    if (DEBUG) llSay(DEBUG_CHANNEL,message);   
    else
        llOwnerSay(message);
}
default {
///////////////////////////////////////////////////////////////////////////////    
    state_entry() 
    {
        key_listen = llListen(TEXTURE_CHANNEL, "", NULL_KEY, "");
        TEXTURE_NOTECARD = llGetInventoryName(INVENTORY_NOTECARD,0);
        if(DEBUG) llDebugSay("Reading Texture notecard " + TEXTURE_NOTECARD);
        TextureQuery = llGetNotecardLine(prefix + TEXTURE_NOTECARD, iLine);
    }
///////////////////////////////////////////////////////////////////////////////    
    listen(integer channel, string name, key id, string message)
    {
        csv_commands = llCSV2List( llToLower ( message ));
        string said_name = llList2String( csv_commands, 0);        
        string command = llList2String( csv_commands,1 );
        list texture = llParseString2List(message, [" "], [""]);
        if ( command == "channel") 
        {
            TEXTURE_CHANNEL = llList2Integer ( csv_commands,2 );
            llListenRemove( key_listen );
            key_listen = llListen(TEXTURE_CHANNEL, "", NULL_KEY, "");
            llOwnerSay ( "Listen Channel set to " + (string)TEXTURE_CHANNEL);
            return;
        }
        if(llList2String(texture, 0) == "image")
        {
            iLine = 0;
            TEXTURE_NOTECARD = llList2String(texture, 1);
            if(DEBUG) llDebugSay("Reading Texture notecard " + prefix + TEXTURE_NOTECARD);
            TextureQuery = llGetNotecardLine(prefix + TEXTURE_NOTECARD, iLine);
        }
    }
///////////////////////////////////////////////////////////////////////////////        
    dataserver(key query_id, string data) {
 
        if (query_id == TextureQuery) {
            // this is a line of our notecard
            if (data != EOF && prefix == "tex_") {    
 
                if(DEBUG) llDebugSay("Line " + (string)iLine + ": " + data);
                llMessageLinked(LINK_SET,0,data,NULL_KEY);
 
                // increment line count                 
 
                //request next line
                ++iLine;
                TextureQuery = llGetNotecardLine(prefix + TEXTURE_NOTECARD, iLine);
            }
            else
            {
                if(DEBUG) llDebugSay("Done reading Texture notecard " + prefix + TEXTURE_NOTECARD);
            }
        }
    }
}