// .::Prototype::.
//
// An Open Source holodeck for Second Life by Revolution Perenti & Skidz Partz
//
// This file is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
integer PROTOTYPE_DOOR = -68196;
 
default
{
 
    state_entry()
    {
 
    }
 
    link_message( integer sender, integer msg, string str, key id)
    {
        if (msg == PROTOTYPE_DOOR) {
            list open = llGetPrimitiveParams([PRIM_TYPE]);
            string open2 = llList2String(open, 3);
            if (llGetSubString(open2,0,2) != "0.0")
                llSetPrimitiveParams([PRIM_TYPE,
                    PRIM_TYPE_BOX,0,<0.0,1.0,0.0>,0.0,ZERO_VECTOR,<1.0,1.0,0.0>,ZERO_VECTOR]);
            else
                llSetPrimitiveParams([PRIM_TYPE,
                    PRIM_TYPE_BOX,0,<0.0,1.0,0.0>,0.95,ZERO_VECTOR,<1.0,1.0,0.0>,ZERO_VECTOR]);
        }
    }
}