// Skidz Partz - Open Source Holodeck Version (2.0.1)
//
// An Open Source holodeck for Second Life by Revolution Perenti & Skidz Partz
//
// This file is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 
// Debug
integer DEBUG = FALSE;
 
// Sound System
list SOUND_VALUES;
integer SOUND_TYPE;
string SOUND_UUID;
float SOUND_VOLUME;
// Texture System
list TEXTURE_VALUES;
string HOLODECK_OBJECTNAME;
string TEXTURE_UID;
vector TEXTURE_REPEATS;
vector TEXTURE_OFFSETS;
float TEXTURE_ROTATE;
vector TEXTURE_COLOR;
float TEXTURE_ALPHA;
 
// Lighting System
list LIGHT_VALUES;
integer  LIGHT_STATUS;
// Color of the light (RGB - each value between 0.0 and 1.0)
vector LIGHT_COLOR;
// Intensity of the light (values from 0.0 .. 1.0)
float  LIGHT_LEVEL;
// Radius of light cone
float  LIGHT_DISTANCE;
// Fall off (distance/intensity decrease) values from 0.0 ..1.0
float  LIGHT_FALLOFF;
 
// Particle System
integer PARTICLE_FLAGS;
//
list PARTICLE_VALUES;
key PARTICLE_SRC_TARGET_KEY = "";
string PARTICLE_SRC_TEXTURE = "";
float PARTICLE_SRC_MAX_AGE;
float PARTICLE_PART_MAX_AGE;
float PARTICLE_BURST_RATE;
integer PARTICLE_BURST_PART_COUNT;
float PARTICLE_BURST_RADIUS;
float PARTICLE_BURST_SPEED_MAX;
float PARTICLE_BURST_SPEED_MIN;
float PARTICLE_START_ALPHA;
float PARTICLE_END_ALPHA;
float PARTICLE_INNERANGLE;
float PARTICLE_OUTERANGLE;
vector PARTICLE_START_COLOR;
vector PARTICLE_END_COLOR;
vector PARTICLE_START_SCALE;
vector PARTICLE_END_SCALE;
vector PARTICLE_ACCEL;
vector PARTICLE_OMEGA;
integer PARTICLE_SRC_PATTERN;
// Link Messages Channels
integer RESET_SCRIPTS = 0x2F34B;
integer HOLODECK_LIGHT            = 0x2F34C;
integer HOLODECK_TEXTURE            = 0x2F34D;
integer LOCATE_LIGHT = 0x2F34E;
integer LOCATE_TEXTURE = 0x2F34F;
integer HOLODECK_PARTICLE = 0x2F350;
integer HOLODECK_SOUND = 0x2F351;
 
default
{
 
    state_entry()
    {
        vector eul = <0.00, 0.00, 0.00>;
        eul *= DEG_TO_RAD;
        rotation quat = llEuler2Rot( eul );
        llSetRot( quat );
    }
 
    link_message(integer sent,integer num,string message,key id)
    {
    if (num == RESET_SCRIPTS)
    {
        llResetScript();
        llOwnerSay("Resetting Holodeck Defaults");
    }
    else if (num == HOLODECK_SOUND)
    {
        SOUND_VALUES = llParseString2List(message, ["#"], []);
        HOLODECK_OBJECTNAME = llList2String(SOUND_VALUES,0);
        string self = llGetObjectName();
        if (HOLODECK_OBJECTNAME == self)
        {
            //list conversions
            SOUND_TYPE = (integer)llList2String(SOUND_VALUES,1);
            SOUND_UUID = (string)llList2String(SOUND_VALUES,1);
            SOUND_VOLUME = (float)llList2String(SOUND_VALUES,1);
 
            // Sound Change
            if(SOUND_TYPE == 1) 
            { 
               llPlaySound(SOUND_UUID, SOUND_VOLUME);
            }
            else if(SOUND_TYPE == 2) 
            { 
               llLoopSound(SOUND_UUID, SOUND_VOLUME);
            }
            else if(SOUND_TYPE == 3) 
            { 
               llTriggerSound(SOUND_UUID, SOUND_VOLUME);
            }
 
 
        }        
    }
    else if (num == LOCATE_TEXTURE)
    {
            // list conversions
            string HOLODECK_GETOBJECTNAME = llGetObjectName();
 
            //list conversions
            string LOCATE_TEXTURE_UID = llGetTexture(0);
            vector LOCATE_TEXTURE_REPEATS = llList2Vector(llGetPrimitiveParams([PRIM_TEXTURE,0]),1);
            vector LOCATE_TEXTURE_OFFSETS = llGetTextureOffset(0);
            float LOCATE_TEXTURE_ROTATE = llGetTextureRot(0) * DEG_TO_RAD;
            vector LOCATE_TEXTURE_COLOR = llGetColor(0);
            float LOCATE_TEXTURE_ALPHA = llGetAlpha(0);
 
             llSay(0, (string)HOLODECK_GETOBJECTNAME+(string)
                "#"+(string)LOCATE_TEXTURE_UID+                          // name
                "#"+(string)LOCATE_TEXTURE_REPEATS+                      // repeats
                "#"+(string)LOCATE_TEXTURE_OFFSETS+                      // offsets
                "#"+(string)LOCATE_TEXTURE_ROTATE+                       // rotation
                "#"+(string)LOCATE_TEXTURE_COLOR+                      // color
                "#"+(string)LOCATE_TEXTURE_ALPHA);                     // alpha     
    }
    else if (num == LOCATE_LIGHT)
    {          
            // list conversions
            string HOLODECK_GETOBJECTNAME = llGetObjectName();
            list LOCATE_PRIM_POINT_LIGHT =  llGetPrimitiveParams([PRIM_POINT_LIGHT]);
            list LOCATE_STATUS = (["FALSE","TRUE"]);
            vector LIGHT_COLOR; 
 
     // output the notecard to chat
     llSay(0, "locating Lighting Values for " + (string)HOLODECK_GETOBJECTNAME);
     llSleep(3);
     llSay(0, (string)HOLODECK_GETOBJECTNAME +                                                // Object Name
       "#" +(string)llList2String(LOCATE_STATUS ,llList2Integer(LOCATE_PRIM_POINT_LIGHT,0)) + // Light Status
       "#" +(string)llList2Vector(LOCATE_PRIM_POINT_LIGHT,1) +                                // Color
       "#"+(string)llList2Float(LOCATE_PRIM_POINT_LIGHT,2) +                                  // Level
       "#"+(string)llList2Float(LOCATE_PRIM_POINT_LIGHT,3) +                                  // Distance
       "#"+(string)llList2Float(LOCATE_PRIM_POINT_LIGHT,4));                                  // falloff
     LOCATE_PRIM_POINT_LIGHT = [];   
    }
    else if (num == HOLODECK_LIGHT)
    {
        LIGHT_VALUES = llParseString2List(message, ["#"], []);
        HOLODECK_OBJECTNAME = llList2String(TEXTURE_VALUES,0);
        string self = llGetObjectName();
        if (HOLODECK_OBJECTNAME == self)
        {
            //list conversions
            LIGHT_STATUS = (integer)llList2String(LIGHT_VALUES,1);
            LIGHT_COLOR = (vector)llList2String(LIGHT_VALUES,2);
            LIGHT_LEVEL = (float)llList2String(LIGHT_VALUES,3);
            LIGHT_DISTANCE = (float)llList2String(LIGHT_VALUES,4);
            LIGHT_FALLOFF = (float)llList2String(LIGHT_VALUES,5);
 
            // Lighting Change
            llSetPrimitiveParams([PRIM_POINT_LIGHT, LIGHT_STATUS, LIGHT_COLOR, LIGHT_LEVEL, LIGHT_DISTANCE, LIGHT_FALLOFF]);
        }
}
    else if (num == HOLODECK_TEXTURE)
    {    
        TEXTURE_VALUES = llParseString2List(message,["#"],[]);
        HOLODECK_OBJECTNAME = llList2String(TEXTURE_VALUES,0);
        string self = llGetObjectName();
        if (HOLODECK_OBJECTNAME == self)
        {
            //list conversions
            TEXTURE_UID = llList2String(TEXTURE_VALUES,1);
            TEXTURE_REPEATS = (vector)llList2String(TEXTURE_VALUES,2);
            TEXTURE_OFFSETS = (vector)llList2String(TEXTURE_VALUES,3);
            TEXTURE_ROTATE = ((float)llList2String(TEXTURE_VALUES,4)) * DEG_TO_RAD;
            TEXTURE_COLOR = (vector)llList2String(TEXTURE_VALUES,5);
            TEXTURE_ALPHA = (float)llList2String(TEXTURE_VALUES,6);
 
            //texture change
            llSetPrimitiveParams([PRIM_TEXTURE,0,TEXTURE_UID,TEXTURE_REPEATS,TEXTURE_OFFSETS,TEXTURE_ROTATE]);
            llSetPrimitiveParams([PRIM_COLOR,0,TEXTURE_COLOR,TEXTURE_ALPHA]);
            }
        }
    else if (num == HOLODECK_PARTICLE)
    {
        PARTICLE_VALUES = llParseString2List(message,["#"],[]);
        HOLODECK_OBJECTNAME = llList2String(PARTICLE_VALUES,0);
        string self = llGetObjectName();
        if (HOLODECK_OBJECTNAME == self)
        {
            //list conversions
            PARTICLE_PART_MAX_AGE = (float)llList2String(PARTICLE_VALUES,1);
            PARTICLE_FLAGS = (integer)llList2String(PARTICLE_VALUES,2);
            PARTICLE_START_COLOR = (vector)llList2String(PARTICLE_VALUES,3);
            PARTICLE_END_COLOR = (vector)llList2String(PARTICLE_VALUES,4);
            PARTICLE_START_SCALE = (vector)llList2String(PARTICLE_VALUES,5);
            PARTICLE_END_SCALE = (vector)llList2String(PARTICLE_VALUES,6);
            PARTICLE_SRC_PATTERN = (integer)llList2String(PARTICLE_VALUES,7);
            PARTICLE_BURST_RATE = (float)llList2String(PARTICLE_VALUES,8);
            PARTICLE_ACCEL = (vector)llList2String(PARTICLE_VALUES,9);
            PARTICLE_BURST_PART_COUNT = (integer)llList2String(PARTICLE_VALUES,10);
            PARTICLE_BURST_RADIUS = (float)llList2String(PARTICLE_VALUES,11);
            PARTICLE_BURST_SPEED_MIN = (float)llList2String(PARTICLE_VALUES,12);
            PARTICLE_BURST_SPEED_MAX = (float)llList2String(PARTICLE_VALUES,13);
            PARTICLE_INNERANGLE = (float)llList2String(PARTICLE_VALUES,14);
            PARTICLE_OUTERANGLE = (float)llList2String(PARTICLE_VALUES,15);
            PARTICLE_OMEGA = (vector)llList2String(PARTICLE_VALUES,16);
            PARTICLE_SRC_MAX_AGE = (float)llList2String(PARTICLE_VALUES,17);
            PARTICLE_START_ALPHA = (float)llList2String(PARTICLE_VALUES,18);
            PARTICLE_END_ALPHA = (float)llList2String(PARTICLE_VALUES,19);
            PARTICLE_SRC_TEXTURE = (string)llList2String(PARTICLE_VALUES,20);
            llSleep(1.5);
            // Particle Change
            llParticleSystem([  
                        PSYS_PART_MAX_AGE, PARTICLE_PART_MAX_AGE,
                        PSYS_PART_FLAGS, PARTICLE_FLAGS,
                        PSYS_PART_START_COLOR, PARTICLE_START_COLOR,
                        PSYS_PART_END_COLOR, PARTICLE_END_COLOR,
                        PSYS_PART_START_SCALE, PARTICLE_START_SCALE,
                        PSYS_PART_END_SCALE, PARTICLE_END_SCALE,
                        PSYS_SRC_PATTERN, PARTICLE_SRC_PATTERN,
                        PSYS_SRC_BURST_RATE,PARTICLE_BURST_RATE,
                        PSYS_SRC_ACCEL,PARTICLE_ACCEL,
                        PSYS_SRC_BURST_PART_COUNT,PARTICLE_BURST_PART_COUNT,
                        PSYS_SRC_BURST_RADIUS,PARTICLE_BURST_RADIUS,
                        PSYS_SRC_BURST_SPEED_MIN,PARTICLE_BURST_SPEED_MIN,
                        PSYS_SRC_BURST_SPEED_MAX,PARTICLE_BURST_SPEED_MAX,
                        PSYS_SRC_ANGLE_BEGIN,PARTICLE_INNERANGLE,
                        PSYS_SRC_ANGLE_END,PARTICLE_OUTERANGLE,
                        PSYS_SRC_OMEGA,PARTICLE_OMEGA,
                        PSYS_SRC_MAX_AGE,PARTICLE_SRC_MAX_AGE,
                        PSYS_PART_START_ALPHA,PARTICLE_START_ALPHA,
                        PSYS_PART_END_ALPHA,PARTICLE_END_ALPHA,
                        PSYS_SRC_TEXTURE, PARTICLE_SRC_TEXTURE,
                        PSYS_SRC_TARGET_KEY,PARTICLE_SRC_TARGET_KEY 
                            ]);                         
        }             
    }
    if (DEBUG) llOwnerSay("This Script Name " + (string)llGetScriptName()
        + "In Object Name " + (string)llGetObjectName()
        + "used " + (string)((16384 - llGetFreeMemory())/1024) + " kBytes");
    }
}