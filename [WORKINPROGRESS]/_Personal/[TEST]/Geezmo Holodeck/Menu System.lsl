// .::Prototype::.
//
// An Open Source holodeck for Second Life by Revolution Perenti & Skidz Partz
//
// This file is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
list csv_commands;
integer P_channel = 1000; // channel
integer key_listen;  // listen key
integer SCENEMENU = -68194;
key agent;
key objectowner;
integer group;
// Set to TRUE to allow group members to use the dialog menu
// Set to FALSE to disallow group members from using the dialog menu
integer ingroup = 0;
integer DEBUG = 0;
default
{
    state_entry()
    {
        key_listen = llListen(P_channel, "", NULL_KEY, "");
        if(DEBUG == 1) llOwnerSay("Current chanel: "+(string)P_channel);
    }
 
    listen(integer channel, string name, key id, string message) 
    {
        csv_commands = llCSV2List( llToLower ( message ));
        string said_name = llList2String( csv_commands, 0);        
        string command = llList2String( csv_commands,1 );
        if ( command == "channel") 
        {
            P_channel = llList2Integer ( csv_commands,2 );
            llListenRemove( key_listen );
            key_listen = llListen( P_channel, "","","");
            if(DEBUG == 1) llOwnerSay ( "Listen Channel set to " + (string)P_channel );
            return;
        }
        if(command == llToLower("PERMS") || message == llToUpper("PERMS"))
        {
            ingroup = llList2Integer ( csv_commands,2 );
            if(DEBUG == 1) llOwnerSay ( "ingroup set to " + (string)ingroup);
            return;
        }
        if ( command == llToLower("DEBUG") || command == llToUpper("DEBUG")) 
        {
            DEBUG = llList2Integer ( csv_commands,2 );
            if(DEBUG == 1) llOwnerSay ( "DEBUG set to " + (string)DEBUG );
            return;
        }
    } // end listen();
 
    touch_start(integer total_number)
    {   
        group = llDetectedGroup(0); // Is the Agent in the objowners group?
        agent = llDetectedKey(0); // Agent's key
        objectowner = llGetOwner(); // objowners key
        // is the Agent = the owner OR is the agent in the owners group
        if ( (objectowner == agent) || ( group && ingroup )  )  {
            llMessageLinked(LINK_SET,SCENEMENU,"",agent);
        }
    } 
}