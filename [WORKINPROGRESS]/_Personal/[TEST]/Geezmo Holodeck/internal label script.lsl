//////////////////////////////////////////////////////////////////////////////////////////
// Configurable Settings
float fTimerInterval = 0.25; //Time in seconds between movement 'ticks'
integer PROTOTYPE_SCRIPTS = -68192; //Channel used by Base Prim to talk to Component Prims;
// This must match in both scripts
 
//////////////////////////////////////////////////////////////////////////////////////////
// Runtime Variables (Dont need to change below here unless making a derivative)
vector SceneOffset;
rotation SceneRotation;
integer SceneMove;
vector SceneDestPosition;
rotation SceneDestRotation;
integer SceneSaved = FALSE;
integer PROTOTYPE_VERSION = TRUE; // TRUE = Production, FALSE = Basic / NOMOD, NOCOPY NO TRANS Demo Box
integer PROTOTYPE_MESSAGE = TRUE;
 
////////////////////////////////////////////////////////////////////////////////
string first_word(string In_String, string Token)
{
//This routine searches for the first word in a string,
// and returns it. If no word boundary found, returns
// the whole string.
    if(Token == "") Token = " ";
    integer pos = llSubStringIndex(In_String, Token);
 
    //Found it?
    if( pos >= 1 )
        return llGetSubString(In_String, 0, pos - 1);
    else
        return In_String;
}
 
////////////////////////////////////////////////////////////////////////////////
string other_words(string In_String, string Token)
{
    //This routine searches for the other-than-first words in a string,
    // and returns it. If no word boundary found, returns
    // the an empty string.
    if( Token == "" ) Token = " ";
 
    integer pos = llSubStringIndex(In_String, Token);
 
    //Found it?
    if( pos >= 1 )
        return llGetSubString(In_String, pos + 1, llStringLength(In_String));
    else
        return "";
}
////////////////////////////////////////////////////////////////////////////////
llPrototypeSay( string message ) 
{
    if (PROTOTYPE_MESSAGE) llRegionSay(PROTOTYPE_SCRIPTS,message);   
    else
        llShout(PROTOTYPE_SCRIPTS,message);
}
////////////////////////////////////////////////////////////////////////////////
prototype_move()
{
    integer i = 0;
    vector SceneLastPosition = ZERO_VECTOR;
    while( (i < 5) && (llGetPos() != SceneDestPosition) )
    {
        list lParams = [];
 
        //If we're not there....
        if( llGetPos() != SceneDestPosition )
        {
            //We may be stuck on the ground...
            //Did we move at all compared to last loop?
            if( llGetPos() == SceneLastPosition )
            {
                //Yep, stuck...move straight up 10m (attempt to dislodge)
                lParams = [ PRIM_POSITION, llGetPos() + <0, 0, 10.0> ];
                //llSetPos(llGetPos() + <0, 0, 10.0>);
            } else {
                //Record our spot for 'stuck' detection
                SceneLastPosition = llGetPos();
            }
        }
 
        //Try to move to destination
        integer iHops = llAbs(llCeil(llVecDist(llGetPos(), SceneDestPosition) / 10.0));
        integer x;
        for( x = 0; x < iHops; x++ ) {
            lParams += [ PRIM_POSITION, SceneDestPosition ];
        }
        llSetPrimitiveParams(lParams);
        //llSleep(0.1);
        ++i; // changed i++ too ++i credit goes to Simon Sugita for Speed Tweak :)
    }
 
    //Set rotation
    llSetRot(SceneDestRotation);
}
 
 
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
default
{
//////////////////////////////////////////////////////////////////////////////////////////
    state_entry()
    {
        //Open up the listener
        llListen(PROTOTYPE_SCRIPTS, "", NULL_KEY, "");
    }
 
//////////////////////////////////////////////////////////////////////////////////////////
    on_rez(integer start_param)
    {
        //Set the channel to what's specified
        if( start_param != 0 )
        {
            PROTOTYPE_SCRIPTS = start_param;
            state reset_listeners;
        }
    }
 
//////////////////////////////////////////////////////////////////////////////////////////
    listen(integer channel, string name, key id, string message)
    {
        string command = llToUpper(first_word(message, " "));
 
        if( command == "RECORD" && PROTOTYPE_VERSION)
        {
            message = other_words(message, " ");
            list lParams = llParseString2List(message, [ "|" ], []);
            vector SceneVectorBase = (vector)llList2String(lParams, 0);
            rotation SceneRotationBase = (rotation)llList2String(lParams, 1);
 
            SceneOffset = (llGetPos() - SceneVectorBase) / SceneRotationBase;
            SceneRotation = llGetRot() / SceneRotationBase;
            SceneSaved = TRUE;
            llOwnerSay("Recorded position.");
            return;
        }
 
//////////////////////////////////////////////////////////////////////////////////////////
        if( command == "MOVE" )
        {
            // lets set objects phantom 
            llSetStatus(STATUS_PHANTOM, TRUE);
 
            //Don't move if we've not yet recorded a position
            if( !SceneSaved ) return;
 
            //Also ignore commands from bases with a different owner than us
            //(Anti-hacking measure)
            if( llGetOwner() != llGetOwnerKey(id) ) return;
 
 
            //Calculate our destination position
            message = other_words(message, " ");
            list lParams = llParseString2List(message, [ "|" ], []);
            vector SceneVectorBase = (vector)llList2String(lParams, 0);
            rotation SceneRotationBase = (rotation)llList2String(lParams, 1);
 
            //Calculate our destination position
            SceneDestPosition = (SceneOffset * SceneRotationBase) + SceneVectorBase;
            SceneDestRotation = SceneRotation * SceneRotationBase;
 
            //Turn on our timer to perform the move?
            if( !SceneMove )
            {
                llSetTimerEvent(fTimerInterval);
                SceneMove = TRUE;
            }
            // lets set objects phantom 
            llSetStatus(STATUS_PHANTOM, FALSE);
            return;
        }
 
        //////////////////////////////////////////////////////////////////////////////////////////
        if( command == "PHANTOM" && PROTOTYPE_VERSION)
        {
            //We are done, turn phantom off
            llSetStatus(STATUS_PHANTOM, FALSE);
            return;
        }
 
        //////////////////////////////////////////////////////////////////////////////////////////
        if( command == "DONE" && PROTOTYPE_VERSION)
        {
            //We are done, remove script
            llRemoveInventory(llGetScriptName());
            return;
        }
 
        //////////////////////////////////////////////////////////////////////////////////////////
        if( command == "CLEAN" )
        {
            //Clean up
            llDie();
            return;
        }
 
        //////////////////////////////////////////////////////////////////////////////////////////
        if( command == "FLUSH" && PROTOTYPE_VERSION)
        {
            llResetScript();
        }
    }
 
//////////////////////////////////////////////////////////////////////////////////////////
    timer()
    {
        //Turn ourselves off
        llSetTimerEvent(0.0);
 
        //Do we need to move?
        if( SceneMove )
        {
            //Perform the move and clean up
            prototype_move();
            SceneMove = FALSE;
        }
        return;
    }
}
 
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
state reset_listeners
{
//////////////////////////////////////////////////////////////////////////////////////////
    state_entry()
    {
        state default;
    }
}