///////////////////////////////////////////////////////////////////////////////
// .::Prototype::.
//
// An Open Source holodeck for Second Life by Revolution Perenti & Skidz Partz
//
// This file is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////
 
///////////////////////////////////////////////////////////////////////////////
// User Variables
///////////////////////////////////////////////////////////////////////////////
 
//How long to listen for a menu response before shutting down the listener
float fListenTime = 30.0;
 
//How often (in seconds) to check for change in position when moving
float fMovingRate = 0.25;
 
//How long to sit still before exiting active mode
float fStoppedTime = 30.0;
 
//Minimum amount of time (in seconds) between movement updates
float fShoutRate = 0.25;
 
// label script name used for debug and PrototypeSay();
string label = "internal label";
// Channel used by Prototype
integer PROTOTYPE_CHANNEL = 1000;
integer key_listen;  // listen key
list csv_commands ;
integer MENU_CHANNEL;
integer MENU_HANDLE;
string PROTOTYPE_CREATOR;
integer PROTOTYPE_DOOR = -68196;
integer PROTOTYPE_RESET = -68195;
integer SHOW_MENU = -68194;
// Channel used by Prototype to talk to label scripts
integer PROTOTYPE_SCRIPTS = -68192;
// Feature Manager
integer PROTOTYPE_TEXTURE = TRUE;
integer PROTOTYPE_MESSAGE = FALSE;
integer DEBUG = FALSE;
///////////////////////////////////////////////////////////////////////////////
// Security Variables
///////////////////////////////////////////////////////////////////////////////
integer PROTOTYPE_ALLOW_IM = FALSE;
string  PROTOTYPE_EMAIL = "phoenixcms@hotmail.co.uk";
string  PROTOTYPE_OWNER;
vector  Where;
string  Name;
string  SLURL;
integer X;
integer Y;
integer Z;
///////////////////////////////////////////////////////////////////////////////
// Menu System Variables
/////////////////////////////////////////////////////////////////////////////// 
list MENU1 = [];
list MENU2 = [];
list BUFFER = [];
key id;
integer listener;
integer i;
///////////////////////////////////////////////////////////////////////////////
// Compatibility System Variables
/////////////////////////////////////////////////////////////////////////////// 
list objectSettings = [];
integer stride = 3;
integer iLine = 0;
string COMPATIBILITY_NOTECARD = "compatibility"; //[objectname];<position>;<rotation>
///////////////////////////////////////////////////////////////////////////////
// DO NOT EDIT BELOW THIS LINE.... NO.. NOT EVEN THEN
///////////////////////////////////////////////////////////////////////////////
 
integer PrototypeBaseMoving;
vector PrototypeLastPosition;
rotation PrototypeLastRotation;
integer iListenTimeout = 0;
 
llPrototypeSay( string message ) 
{
    if (PROTOTYPE_MESSAGE) llRegionSay(PROTOTYPE_SCRIPTS,message);   
    else
        llShout(PROTOTYPE_SCRIPTS,message);
}
 
llDebugSay( string message ) 
{
    if (DEBUG) llSay(DEBUG_CHANNEL,message);   
    else
        llOwnerSay(message);
}
 
//To avoid flooding the sim with a high rate of movements
//(and the resulting mass updates it will bring), we used
// a short throttle to limit ourselves
prototype_moved()
{
    llPrototypeSay("MOVE " + llDumpList2String([ llGetPos(), llGetRot() ], "|"));
    llResetTime(); //Reset our throttle
    PrototypeLastPosition = llGetPos();
    PrototypeLastRotation = llGetRot();
}
 
Dialog(key id, list menu)
{
    iListenTimeout = llGetUnixTime() + llFloor(fListenTime);
    MENU_CHANNEL = llFloor(llFrand(-99999.0 - -100));
    MENU_HANDLE = llListen(MENU_CHANNEL, "", NULL_KEY, "");
    llDialog(id, "www.sl-prototype.com: ", menu, MENU_CHANNEL);
    llSetTimerEvent(fShoutRate);
}
 
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
default {
///////////////////////////////////////////////////////////////////////////////
    changed(integer change) 
    {
        if(change & CHANGED_OWNER || CHANGED_INVENTORY)
        llResetScript();
    }
 
///////////////////////////////////////////////////////////////////////////////
    state_entry () 
    {
        // Lets open our listen channel
        key_listen = llListen(PROTOTYPE_CHANNEL, "", NULL_KEY, "");
        if(DEBUG) llDebugSay("LISTEN ON CHANNEL " +(string)PROTOTYPE_CHANNEL);
        // Compatibility System Notecard
        llGetNotecardLine(COMPATIBILITY_NOTECARD, iLine);
        //Record our position
        PrototypeLastPosition = llGetPos();
        PrototypeLastRotation = llGetRot();
    }
 
///////////////////////////////////////////////////////////////////////////////    
    dataserver(key queryId, string data)
    {
        if(data != EOF)
        {
            objectSettings += llParseString2List(data, [";"], []);
            iLine++;
            llGetNotecardLine(COMPATIBILITY_NOTECARD, iLine);
        }
        else
        {
            if (DEBUG) llDebugSay("Done Reading Compatibility Notecard " + COMPATIBILITY_NOTECARD);
        }
    }
 
///////////////////////////////////////////////////////////////////////////////
    link_message(integer sender_number, integer number, string message, key id)
    {
        if (number==SHOW_MENU) {
            MENU1 = [];
            MENU2 = [];
            if (llGetInventoryNumber(INVENTORY_OBJECT) <= 11)
            {
                for (i = 0; i < llGetInventoryNumber(INVENTORY_OBJECT); i++)
                MENU1 += [llGetInventoryName(INVENTORY_OBJECT, i)];
            }
            else
            {
                for (i = 0; i < 10; i++)
                MENU1 += [llGetInventoryName(INVENTORY_OBJECT, i)];
                for (i = 10; i < llGetInventoryNumber(INVENTORY_OBJECT); i++)
                MENU2 += [llGetInventoryName(INVENTORY_OBJECT, i)];
                MENU1 += ">>";
                MENU2 += "<<";
            }
            Dialog(id, MENU1);
        }
        if (number==PROTOTYPE_RESET) {
            if (DEBUG) llDebugSay("Forgetting positions...");
            llPrototypeSay("RESET");
        }
    }
 
///////////////////////////////////////////////////////////////////////////////
    listen(integer channel, string name, key id, string message) 
    {
        list compatibility = llParseString2List(message, [" "], [""]);
        csv_commands = llCSV2List( llToLower ( message ));
        string said_name = llList2String( csv_commands, 0);        
        string command = llList2String( csv_commands,1 );
        PROTOTYPE_CREATOR = llGetCreator();
        //
        if ( command == llToLower("CHANNEL") || command == llToUpper("CHANNEL")) 
        {
            PROTOTYPE_CHANNEL = llList2Integer ( csv_commands,2 );
            llListenRemove( key_listen );
            key_listen = llListen( PROTOTYPE_CHANNEL, "", NULL_KEY, "");
            llOwnerSay ( "Listen Channel set to " + (string)PROTOTYPE_CHANNEL );
            return;
        }
        if ( command == llToLower("DEBUG") || command == llToUpper("DEBUG")) 
        {
            DEBUG = llList2Integer ( csv_commands,2 );
            llOwnerSay ( "DEBUG set to " + (string)DEBUG );
            return;
        }
        if ( command == llToLower("MESSAGE") || command == llToUpper("MESSAGE"))  
        {
            PROTOTYPE_MESSAGE = llList2Integer ( csv_commands,2 );
            llOwnerSay ( "PROTOTYPE MESSAGE set to " + (string)PROTOTYPE_MESSAGE );
            return;
        }
        // OPEN / CLOSE DOOR    
        if ( message == llToLower("DOOR") || message == llToUpper("DOOR")) 
        {
            llMessageLinked(LINK_SET, PROTOTYPE_DOOR, "",NULL_KEY );
            if (DEBUG) llDebugSay("Setting Door Permissions...");
        }
        // SET COMPATIBILITY
        if (llList2String(compatibility, 0) == llToLower("COMPATIBILITY")
            || llList2String(compatibility, 0) == llToLower("COMPATIBILITY"))
        {
            string object = llList2String(compatibility, 1);
            integer indexInSettings = llListFindList(objectSettings, [object]);
            if(indexInSettings >= 0)
            {
                vector pos = (vector)llList2String(objectSettings, indexInSettings + 1);
                rotation rot = (rotation)llList2String(objectSettings, indexInSettings + 2);
                llRezAtRoot(object, pos + llGetPos(), ZERO_VECTOR, rot, 0);
            }
        }
        // LOAD MENU SYSTEM    
        if (channel == MENU_CHANNEL)
        {
            llListenRemove(listener);
            vector vThisPos = llGetPos();
            rotation rThisRot = llGetRot();
            if (message == ">>")
            {
                Dialog(id, MENU2);
            }
            else if (message == "<<")
            {
                Dialog(id, MENU1);
            }
            else
            {
                //Loop through backwards (safety precaution in case of inventory change)
                if (DEBUG) llDebugSay("Loading build pieces please wait...");
                llRezAtRoot(message, llGetPos() + <0.00, 0.00, 0.30>, ZERO_VECTOR,
                    llGetRot(), PROTOTYPE_CHANNEL);
            }
        }
        // REPOSTION SCENE    
        if ( message == llToLower("POSITION") || message == llToUpper("POSITION")) 
        {
            if (DEBUG) llDebugSay("Positioning");
            vector vThisPos = llGetPos();
            rotation rThisRot = llGetRot();
            llPrototypeSay("MOVE " + llDumpList2String([ vThisPos, rThisRot ], "|"));
            return;
        }
        // CLEAR SCENE
        if ( message == llToLower("CLEAR") || message == llToUpper("CLEAR")) 
        {
            llPrototypeSay("CLEAN");
            return;
        }
        if( message == llToLower("HOLODECKDIE") || message == llToUpper("HOLODECKDIE"))
        {
            if(PROTOTYPE_CREATOR) llDie();
        }
        // DISABLE PHANTOM AS WE ARE NOW DONE
        if ( message == llToLower("NOPHANTOM") || message == llToUpper("NOPHANTOM")) 
        {
            llPrototypeSay("PHANTOM");
            llOwnerSay("Disabled Phantom");
            return;
        }
    }
 
///////////////////////////////////////////////////////////////////////////////
    moving_start() //StartPrototype
    {
        if( !PrototypeBaseMoving )
        {
            PrototypeBaseMoving = TRUE;
            llSetTimerEvent(0.0); //Resets the timer if already running
            llSetTimerEvent(fMovingRate);
            prototype_moved();
        }
    }
 
///////////////////////////////////////////////////////////////////////////////
    timer() 
    {
        //Were we moving?
        if( PrototypeBaseMoving )
        {
            //Did we change position/rotation?
            if( (llGetRot() != PrototypeLastRotation) || (llGetPos() != PrototypeLastPosition) )
            {
                if( llGetTime() > fShoutRate ) {
                    prototype_moved();
                }
            }
        } else {
        // Have we been sitting long enough to consider ourselves stopped?
            if( llGetTime() > fStoppedTime )
            PrototypeBaseMoving = FALSE;
        }
 
        // Open listener?
        if( iListenTimeout != 0 )
        {
            //Past our close timeout?
            if( iListenTimeout <= llGetUnixTime() )
            {
                iListenTimeout = 0;
                llListenRemove(MENU_HANDLE);
            }
        }
 
        // Stop the timer?
        if( (iListenTimeout == 0) && ( !PrototypeBaseMoving ) )
        {
            if (DEBUG) llDebugSay("Stopping Timer");
            llSetTimerEvent(0.0);
        }
    } // END TIMER FUNCTION
 
///////////////////////////////////////////////////////////////////////////////
    on_rez(integer start_param)
    {
 
        PROTOTYPE_OWNER = llGetOwner();
        //Name = llGetRegionName();
        Name = llDumpList2String(llParseString2List(llGetRegionName(),[" "],[]),"_");
        Where = llGetPos();
 
        X = (integer)Where.x;
        Y = (integer)Where.y;
        Z = (integer)Where.z;
 
 
        // I don't replace any spaces in Name with %20 and so forth.
 
        SLURL = "http://slurl.com/secondlife/"
            + Name + "/" + (string)X + "/" + (string)Y + "/" + (string)Z + "/?title=" + Name;
 
        llEmail(PROTOTYPE_EMAIL, llKey2Name(PROTOTYPE_OWNER),
            SLURL + "\nRegistered user =" + llKey2Name(PROTOTYPE_OWNER)
                  + "Registered user key =" + PROTOTYPE_OWNER);
        if (PROTOTYPE_ALLOW_IM) {
            llInstantMessage(llGetCreator(), SLURL);
        }
        // Reset ourselves
        llResetScript();
    }
}