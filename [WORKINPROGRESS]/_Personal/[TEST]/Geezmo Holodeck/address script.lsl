integer DEBUG = FALSE;
integer PROTOTYPE_CHANNEL = 1000;
integer TEXTURE_CHANNEL = -68193;
integer PROTOTYPE_SCRIPTS = -68192;
integer PROTOTYPE_MESSAGE = FALSE;
float gap = 10.0;
float counter = 0.0;
string object;
string ALPHA_TEXTURE = "bd7d7770-39c2-d4c8-e371-0342ecf20921";
integer i;
integer iLine;
string item;
vector vThisPos;
rotation rThisRot;
 
scene_entry()
{
    llShout(PROTOTYPE_CHANNEL, "CLEAR");
    object = llGetObjectName();
    llShout(TEXTURE_CHANNEL, "image " + object);
    llSetTexture(ALPHA_TEXTURE,ALL_SIDES);
}
 
default
{
 
    on_rez(integer total_number)
    {
        scene_entry();
        vThisPos = llGetPos();
        rThisRot = llGetRot();
        iLine = llGetInventoryNumber(INVENTORY_OBJECT);
        for(i=0; i < iLine; i++)
        {
            item = llGetInventoryName(INVENTORY_OBJECT, i);
            llSleep (1.00);
            llRezObject(item, vThisPos + <0.00, 0.00, 1.00>, ZERO_VECTOR, rThisRot, 0);
            llShout(PROTOTYPE_SCRIPTS, "MOVE " + llDumpList2String([ vThisPos, rThisRot ], "|"));
            if (DEBUG) llShout (DEBUG_CHANNEL, "Rezzing " + item);
        }
        llSetTimerEvent(gap);
    }
 
    timer()
    {
        counter = counter + gap; 
        if (DEBUG) llSay(DEBUG_CHANNEL, (string)counter+" seconds have passed i will now terminate");
        llShout(PROTOTYPE_CHANNEL, "POSITION");
        llDie();
    }
}