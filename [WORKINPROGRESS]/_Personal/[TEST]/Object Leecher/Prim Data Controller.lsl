
list DictGetItem(list dict, string dkey)
{
    if (dkey == "") return [];
    integer elements = llList2Integer(dict,0);
    integer loc = llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    if (loc<0)
        return [];
    else
        return llList2List(llList2List(dict,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements);
}

integer DictCount(list dict)
{
    return (llGetListLength(dict)-1)/(llList2Integer(dict,0)+1);
}

string stackdump(list x) {
    return "["+llDumpList2String(x, "] | [")+"]";
}

default
{
    state_entry()
    {
        llListen( -123456789, "", "", "" );
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        // Recognize type
        list tmp = llParseString2List( _m, [" "], [] );
        
        string type = llList2String( tmp, 0 );
        list data = llCSV2List( llGetSubString( _m, llStringLength( type ) + 1, llStringLength( _m ) ) );

        llSay(0, "Starting cloning: " + type );
        
        integer i;
        for( ; i<DictCount( data ); ++i )
        {
            list params = DictGetItem( data, (string)i );
            llSetPrimitiveParams( [PRIM_TEXTURE, i, llList2Key(params, 0), (vector)llList2String(params, 1), (vector)llList2String(params, 2), llList2Float(params, 3)] );
        }
        
        
        // Reproduce the Prim
        
        // Object
        // Status psy, temp, phant
        // position, size, rotation
        // prim options
        
        // Features
        //  - Flexible path - soft grav drag wind tens forceX forceY forceX
        //  - Light - color intensity radius falloff
        // Textures
        //  - texture
        //  - color
        //  - bright
        //  - transp
        //  - map shine bump
        //  - rotation
        //  - repeat per meter
        //  - X offset - Y offset
        
        // llRemoveInventory(llGetScriptName());
    }
}
