
report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
        
    if ( _text != "" )
        llOwnerSay( _text );
}

integer like( string value, string mask ) {
    integer tmpy = ( llGetSubString( mask,  0,  0 ) == "%" ) | 
                   ( ( llGetSubString( mask, -1, -1 ) == "%" ) << 1 );
    if( tmpy )
        mask = llDeleteSubString(mask, (tmpy / -2), -(tmpy == 2));
 
    integer tmpx = llSubStringIndex( value, mask );
    if( ~tmpx ) {
        integer diff = llStringLength( value ) - llStringLength( mask );
        return  ( ( !tmpy && !diff)
             || ( ( tmpy == 1 ) && ( tmpx == diff ) )
             || ( ( tmpy == 2 ) && !tmpx )
             ||   ( tmpy == 3 ) );
    }
    return FALSE;
}

integer DictCount(list dict)
{
    return (llGetListLength(dict)-1)/(llList2Integer(dict,0)+1);
}

integer DictFindKey(list dict, string dkey)
{
    if (dkey == "") 
        return -1;
    else {
        integer elements = llList2Integer(dict,0);
        return llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    }
}
 
list DictGetItem(list dict, string dkey)
{
    if (dkey == "") return []; //dkey = llToLower(dkey);
    integer elements = llList2Integer(dict,0);
    integer loc = llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]); 
    if (loc<0)
        return [];
    else
        return llList2List(llList2List(dict,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements);
}

string stackdump(list x) {
    return "["+llDumpList2String(x, "] | [")+"]"; 
}

list GrabTextures()
{
    list ret = [4]; 
    integer i;
    for ( ; i < 10; ++i )
    {
        ret += i;
        ret += llGetPrimitiveParams([PRIM_TEXTURE, i]);
    }
    return ret;
} 
 
default
{ 
    state_entry()
    {
        //llRezObject();
        list textures = GrabTextures();
        llSay(-123456789, "TEXTURES " + llList2CSV(textures));
    }
    
    touch_start(integer _c)
    {
        list textures = GrabTextures();
        llSay(-123456789, "TEXTURES " + llList2CSV(textures));
    }
}