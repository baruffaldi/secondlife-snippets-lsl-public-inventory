default
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
        list keys = llCSV2List( _id );
        if ( llList2Key( keys, 0 ) == llGetInventoryKey( llGetScriptName() ) )
        {
            string text;

            text = llGetScriptName() + "("+(string)llGetInventoryKey( llGetScriptName() )+")\n";
            text += "Requester: " + llKey2Name( llList2Key( keys, 1 ) ) + "(" + llList2String( keys, 1 ) + ")\n";

            llOwnerSay( text );
        }
    }
}
