// Modularizer v0.5 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ F.B. <bornslippy@coolminds.org>
// Licensed under the GNU GPLv3 License
// http://www.gnu.org
//

string applicationName = "Modularizer";
string header = "*** Modularizer ***\n";
string configuration = "configuration";

integer ownerOnly = FALSE;
integer debug = FALSE;
integer permanentConfiguration = FALSE;

// -----------------------------------------------------------------

integer notecardLine;
key queryId;

string modulesList;

list modules;
list modulesKeys;

integer dialogsChannel;

list parseScriptName( string script )
{
    return llParseString2List( script, ["-"], [] );
}

key getModuleKeyByModuleName( string moduleName )
{
    return llList2String( modulesKeys, llListFindList( modules, [moduleName] ) );
}

// TODO: migliorare gli autoaggiornamenti come state_exit, on_rez, state_entry, etc etc

report( string text, string fliptitle, integer red, integer last )
{
    if ( text != "" )
        llOwnerSay( "\n" + header + text );

    if ( fliptitle != "" )
    {
        vector color;
        if ( red ) color = <1.0, 0.0, 0.0>;
        else color = <0.0, 1.0, 0.0>;

        llSetText( fliptitle, color, 1.0 );

        if ( last )
        {
            llSleep(3);
            llSetText( "", color, 0.0 );
        }
    }
}

detectModules()
{
    modules = [];
    modulesKeys = [];
    modulesList = "";

    list inventory;

    string  this = llGetScriptName();
    
    integer inventoryScripts = llGetInventoryNumber( INVENTORY_SCRIPT );
    integer i = 0;
    integer modulesFound = 0;

    report( "", "Searching for modules...", FALSE, FALSE );

    for ( ; i < inventoryScripts; ++i )
    {
        string script = llGetInventoryName( INVENTORY_SCRIPT, i );
        key scriptKey = llGetInventoryKey( script );
        list scriptInfo = parseScriptName( script );

        // Check:
        if( script != this && // it's not this script
            llGetInventoryPermMask( script, MASK_OWNER ) && // the owner is this scripts owner
            llListFindList( modulesKeys, [scriptKey] ) == -1 && // it's not already loaded
            llList2String( scriptInfo, 0 ) == applicationName ) // it's a module
        {
            integer length = llGetListLength( scriptInfo );
            
            string module = llList2String( scriptInfo, 1 );
            string version;

            if ( length > 2 )
                version = llList2String( scriptInfo, 2 );
            else version = "0.0.0"; 
            
            ++modulesFound;

            modules += [ module ];
            modulesKeys += [ scriptKey ];
            modulesList += "- " + module + " (version: " + version + ") (author: " + llKey2Name( llGetInventoryCreator( script ) ) + ")";
            if ( debug )
                modulesList += " (uuid: " + (string ) scriptKey + ") (script: " + script + ")";
            modulesList += "\n";
        }
    }

    if ( modulesFound < 1 )
        report( "Cannot find any " + applicationName + " module inside the script's inventory.", "No modules found!", TRUE, TRUE );
    else
        report( (string) (modulesFound) + " Modules found!", "Done!", FALSE, TRUE );
}

detectInventoryChanges()
{
    if ( ! permanentConfiguration )
    {
        if ( llGetInventoryType( configuration ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( configuration, MASK_OWNER ) )
            {
                report(
                    "Configuration found!\nLeeching settings in progress...",
                    "Configuration reading...",
                    FALSE, FALSE
                );
                
                notecardLine = 0;
                queryId = llGetNotecardLine( configuration, notecardLine );
                
                return;
            }
    
        report(
            "There is no configuration to import.",
            "Using default settings...",
            FALSE, FALSE
        );
    }
    
    detectModules();
}

reset()
{
    report(
        "",
        "Resetting application...",
        FALSE, FALSE
    );
    
    llMessageLinked( LINK_THIS, 0, "RESETSCRIPT", NULL_KEY );
    
    llSleep(1.7); // Giving the time to reset all scripts
    
    report(
        "Welcome to the " + applicationName + "!",
        "Welcome!!!\n" + applicationName + "\nInizializing...",
        FALSE, FALSE
    );

    detectInventoryChanges();
}

default
{
    link_message(integer sender_num, integer num, string str, key id)
    {
        // This could be useful to get modules callbacks
    }
    
    on_rez( integer n )
    {
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llSetObjectName( applicationName );
    }

    changed( integer change )
    {
        if ( change & CHANGED_OWNER )
            reset();

// Something has changed in inventory, gotta reset all modules informations
        if ( change & CHANGED_INVENTORY )
        {
            detectInventoryChanges();
        }
        llSetObjectName( applicationName );
    }

    dataserver( key queryid, string data )
    {
        list temp;
        string name;
        string value;
 
        if ( queryid == queryId )
            if ( data != EOF )
            {
                if ( llGetSubString( data, 0, 0 ) != "#" && llStringTrim( data, STRING_TRIM ) != "" )
                {
                    temp = llParseString2List( data, ["="], [] );
                    name = llStringTrim( llToLower( llList2String( temp, 0 ) ), STRING_TRIM );
                    value = llStringTrim( llList2String( temp, 1 ), STRING_TRIM );

                    if ( name == "application_name" )
                        applicationName = value;
                        
                    else if ( name == "application_header" )
                        header = value + "\n";
                        
                    else if ( name == "configuration_note" )
                        configuration = value;
                        
                    else if ( name == "owner_only" )
                        ownerOnly = (integer) value;
                        
                    else if ( name == "debug" )
                        debug = (integer) value;
                        
                    else if ( name == "permanent_configuration" )
                        permanentConfiguration = (integer) value;
                        
                    key target;
                    
                    if ( ownerOnly ) target = llGetOwner();
                    else target = NULL_KEY;

                    llListenRemove( dialogsChannel );                    
                    llListen(
                        dialogsChannel,
                        "",
                        target,
                        ""
                    );
                }
                
                notecardLine++;
                queryId = llGetNotecardLine( configuration, notecardLine );
            }
            
            else detectModules();
    }
    
    state_entry()
    {
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        key target;
        
        if ( ownerOnly ) target = llGetOwner();
        else target = NULL_KEY;
        
        llListen(
            dialogsChannel,
            "",
            target,
            ""
        );
    }

    touch_start( integer total_number )
    {
        if( llDetectedKey( 0 ) == llGetOwner( ) || ! ownerOnly )
        {
            integer length = llGetListLength( modules );
            integer i;

            list buttons = ["Options"];

            string desc;

            if ( length > 0 )
            {
                if ( length <= 8 )
                {
                    desc = "Choose the module to manage";
                    for ( ; i < length; ++i )
                        buttons += llList2String( modules, i );
                }
                
                else
                {
                    desc = "Choose the modules section";
                    for ( i = 1; i <= ( ( length / 9 ) + 1 ); ++i )
                        buttons += (string) i;
                }
                
                desc += " from those specified below or click on \"Options\" to get available settings.\n";

            } else desc = "There isn't any module inside the " + applicationName + "'s inventory.";

            llDialog(
                llDetectedKey( 0 ),
                header + "\n" + desc,
                buttons,
                dialogsChannel
            );
        }
    }

    listen( integer _chan, string _name, key _id, string _requestedModule )
    {
        if ( _requestedModule == "Options" && llGetOwner() == _id )
            llDialog(
                llGetOwner(),
                header + "\nChoose an option below:",
                ["Versions", "ShowConf", "Reset"],
                dialogsChannel
            );
            
        else if ( _requestedModule == "Options" && llGetOwner() != _id )
            llDialog(
                _id,
                header + "\nI'm afraid, but only the owner is able to change the application settings.",
                [],
                dialogsChannel
            );
                    
        else if ( _requestedModule == "Versions" )
            report( "\nLoaded modules:\n" + modulesList, "", FALSE, TRUE );
                    
        else if ( _requestedModule == "ShowConf" )
        {
            string desc;
            desc = "\nSettings:\n- Name: " + applicationName + "\n- Header: " + header + "\n- Owner Only: " + (string) ownerOnly + "\n- Debug: " + (string) debug;
            if ( ! permanentConfiguration ) desc += "\n- Configuration Note: " + configuration + "\n- Permanent Configuration: " + (string) permanentConfiguration;
            
            report( desc, "", FALSE, TRUE );
        }
           
        else if ( _requestedModule == "Reset" )
            llDialog(
                _id,
                header + "\nAre you really really sure about what you're doing? This will reset all loaded modules settings...\n\nIf you're sure 100%, then press \"Sure!\" else just press \"ignore\"!",
                ["Sure!"],
                dialogsChannel
            );
        
        else if ( _requestedModule == "Sure!" )
            reset();

        else if ( (integer) _requestedModule > 0 )
        {
            integer section = (integer) _requestedModule;
            integer length  = llGetListLength( modules );
            integer offset;
            integer end;

            list buttons;

            if ( section == 1 )
                offset = 0;
            else offset = ( section - 1 ) * 9;
            
            end = ( offset + 9 );

            for ( ; offset < end; ++offset )
                if ( offset < length )
                    buttons += llList2String( modules, offset );

            llDialog(
                _id,
                header + "\nChoose the module to manage from those specified below:",
                buttons,
                dialogsChannel
            );
        }

        //else llMessageLinked( 1, 0, "DIALOG", getModuleKeyByModuleName( _requestedModule ) );
        else llMessageLinked( LINK_THIS, 0, "DIALOG", (string) getModuleKeyByModuleName( _requestedModule ) + "," + (string ) _id );
    }
}