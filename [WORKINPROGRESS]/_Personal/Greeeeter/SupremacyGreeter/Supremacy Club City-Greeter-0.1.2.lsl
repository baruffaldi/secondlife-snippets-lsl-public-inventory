// TODO: Scelta colore fliptext dei join ( usando 3 valori rgb stampati ) e la possibilita' di scegliere a random
// TODO: Completare i testi dei dialogs

string  header = "*** Greeter Module ***";
integer debug = FALSE;

float   radius = 92.0; // normal sensor radius

integer dialogsChannel;

integer mode;
integer action;
integer greeterStatus;
integer greeterPrivate;
integer greeterFliptitle;
integer greeterWarning;
integer running;

string region;
string greeterString = "Welcome [USER] on [REGION]! Today we have a [WIND] powerful wind and a cloud of [CLOUD]. Have a good time...";

menu( key target )
{
    list buttons = ["Options"];
    action = 0;
    
    if ( greeterStatus ) buttons += ["Off"];
    else buttons += ["On"];

    reportDialog( target, "The greeter functionality gives you the chance to make me say something whenever someone join the land\nTo choose the string you have to click on \"SetString\" that only appears when this functionality is enabled.", buttons );
}

report( string _text, string _fliptitle, integer _red, integer _last )
{
    if ( _text != "" )
        llInstantMessage( llGetOwner(), "\n" + header + "\n" + _text );

    if ( _fliptitle != "" )
    {
        vector color;
        if ( _red ) color = <1.0, 0.0, 0.0>;
        else color = <0.0, 1.0, 0.0>;

        llSetText( _fliptitle, color, 1.0 );
 
        if ( _last )
        {
            llSleep(3);
            llSetText( "", color, 0.0 );
        }
    }
}

reportDialog( key _dest, string _text, list _buttons )
{
    if ( _dest == NULL_KEY ) _dest == llGetOwner();

    llDialog(
        _dest,
        header + "\n" + _text,
        _buttons,
        dialogsChannel );
}

integer processRequest( integer _chan, string _name, key _id, string _option )
{
    list buttons;
    string desc;

    // *** Local-Chat specified settings
    if ( _chan == 3 )
    {
        // *** Greeter
        if ( action == 2 )
        {
            action = 0;
            greeterString = _option;
            llListenRemove(3);
            report( "The greeter string has been set to: " + _option, "Done!", FALSE, TRUE );
        }
        return 0;
    }
    
    if ( _option == "Options" )
    {
        buttons = ["SetString", "WarnJoins", "FlipTitle"];

        if ( greeterStatus && greeterPrivate ) buttons += "Public";
        else if ( greeterStatus && ! greeterPrivate ) buttons += "Private";

        reportDialog( _id, "Please choose an option below:", buttons );
    }

    else if ( _option == "SetString" )
    {
        report( "You can type your greet string on channel 3.", "Waiting...", FALSE, FALSE );

        llListenRemove(3);
        llListen(
            3,
            "",
            _id,
            "" );
    }
    
    else if ( _option == "WarnJoins" )
    {
        action = 1;
        if ( greeterWarning ) buttons += "Disable";
        else buttons += "Enable";
        
        reportDialog( _id, "Please choose an option below:", buttons );
    }

    else if ( _option == "FlipTitle" )
    {
        action = 2;
        if ( greeterFliptitle ) buttons += "Disable";
        else buttons += "Enable";
        
        reportDialog( _id, "Please choose an option below:", buttons );
    }

    else if ( action == 1 )
    {
        action = 0;
        if ( _option == "Enable" )
            greeterWarning = TRUE;
        else if ( _option == "Disable" )
            greeterWarning = FALSE;

        if ( greeterWarning ) _option = "enabled";
        else _option = "disabled";

        report( "The greeter join warnings are now " + _option, "Done!", FALSE, TRUE );
    }

    else if ( action == 2 )
    {
        action = 0;
        if ( _option == "Enable" )
            greeterFliptitle = TRUE;
        else if ( _option == "Disable" )
            greeterFliptitle = FALSE;
        
        if ( greeterFliptitle ) _option = "enabled";
        else _option = "disabled";

        report( "The greeter join fliptitle is now " + _option, "Done!", FALSE, TRUE );
    }
    
    else
    {
        action = 0;
        
        if ( _option == "Public" )
            greeterPrivate = FALSE;

        else if ( _option == "Private" )
            greeterPrivate = TRUE;
        
        if ( greeterStatus ) _option = "enabled";
        else _option = "disabled";

        if ( greeterPrivate ) _option += " and private.";
        else _option += " and public.";

        report( "The greeter is now " + _option + "\nString: " + greeterString, "Done!", FALSE, TRUE );

        if ( _option == "On")
            return 2;

        else if ( _option == "Off" )
            return 1;
    }

    return 0;
}

string parseString( string text, string name )
{
    list   tmp;
    string ret;

    tmp = llParseString2List( text, ["[USER]"], []);
    ret = llDumpList2String( tmp, name );

    tmp = llParseString2List( ret, ["[REGION]"], []);
    ret = llDumpList2String( tmp, region );

    tmp = llParseString2List( ret, ["[WIND]"], []);
    ret = llDumpList2String( tmp, (string)llWind(ZERO_VECTOR) );

    tmp = llParseString2List( ret, ["[CLOUD]"], []);
    ret = llDumpList2String( tmp, (string)llCloud(ZERO_VECTOR) );

    return ret;
}

default
{
    changed( integer change )
    {
        if (change & CHANGED_REGION)
            region = llGetRegionName();
    }
    
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
        list params = llCSV2List( _id );
        
        key this   = llGetInventoryKey( llGetScriptName() );
        key script = llList2Key( params, 0 );
        key target = NULL_KEY;
        string param;
        
        if ( llGetListLength( params ) > 1 )
        {
            if ( llGetListLength( params ) > 2 )
                param = llList2String( params, 2 );
                
            target = llList2Key( params, 1 );
        }
    
        if ( debug )
            if ( ( _str == "DIALOG" && script == this ) || ( _str != "DIALOG" ) )
            {
                llSay(0, "OK" );
                string text;
        
                text = llGetScriptName() + "(" + (string) this + ")\n";
                text += "Command received: " + _str + "\n";
                text += "Requester: " + llKey2Name( target ) + "(" + (string) script + ")\n";
                text += "Parameter: " + param + "\n";
        
                report( text, "Request received: " + _str, FALSE, TRUE );
            }
            
        if ( _str == "RESETSCRIPT" )
        {
            report( "Restarting module Greeter...", "Resetting...", FALSE, TRUE );
            llResetScript( );
        }
        
        else if ( _str == "JOIN" )
        {
            string warning;
            string fliptitle;
            
            if ( greeterFliptitle ) fliptitle = "Welcome " + param + "!";
            else fliptitle = "";
            
            if ( debug ) param += "(" + (string) target + ")";
            
            if ( greeterWarning ) warning = "[" + region + "]: " + param + " has joined!";
            else warning = "";
            
            report( warning, fliptitle, FALSE, TRUE );
        }
         
        else if ( script == this )
             if ( _str == "DIALOG" )
                menu( target );
    }
    
    state_entry()
    {
        if ( region == "" )
            region = llGetRegionName();
            
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            dialogsChannel,
            "",
            "",
            ""
        );
    }

    listen( integer _chan, string _name, key _id, string _option )
    {
        integer result = processRequest( _chan, _name, _id, _option );
        
        // -1 = not exists
        // 0 = action done
        // 1 = service stop
        // 2 = service start normal
        // 3 = service start collision

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "", TRUE, TRUE );
            
        else if ( result == 1 )
            running = FALSE;
            
        else if ( result == 2 )
            running = TRUE;
    }
} 