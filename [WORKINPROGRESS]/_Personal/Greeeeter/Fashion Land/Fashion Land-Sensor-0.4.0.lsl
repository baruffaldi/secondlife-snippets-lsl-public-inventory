string  header = "*** Sensor Module ***";
integer debug = FALSE;

float   radius = 92.0; // normal sensor radius

integer dialogsChannel;

integer mode;
integer running;
integer action;

list greetedCache;

menu( key target )
{
    list buttons = ["Options"];
    action = 2;
    
    if ( running ) buttons += ["Off"];
    else buttons += ["On"];

    reportDialog( target, "The greeter functionality gives you the chance to make me say something whenever someone join the land\nTo choose the string you have to click on \"SetString\" that only appears when this functionality is enabled.", buttons );
}

report( string _text, string _fliptitle, integer _red, integer _last )
{
    if ( _text != "" )
        llInstantMessage( llGetOwner(), "\n" + header + "\n" + _text );

    if ( _fliptitle != "" )
    {
        vector color;
        if ( _red ) color = <1.0, 0.0, 0.0>;
        else color = <0.0, 1.0, 0.0>;

        llSetText( _fliptitle, color, 1.0 );
 
        if ( _last )
        {
            llSleep(3);
            llSetText( "", color, 0.0 );
        }
    }
}

reportDialog( key _dest, string _text, list _buttons )
{
    if ( _dest == NULL_KEY ) _dest == llGetOwner();

    llDialog(
        _dest,
        header + "\n" + _text,
        _buttons,
        dialogsChannel );
}

integer isPositionNear( vector pos )
{
    vector range = <radius, radius, radius> - <30, 30, 30>;
    vector diff = ( llGetPos( ) - pos );
    llSay(0, (string) diff);
//    if ( diff >= range )
        return FALSE;
    return TRUE;
}

integer processRequest(integer _chan, string _name, key _id, string _option)
{
    list buttons;
    string desc;

    if ( _option == "Options" )
    { 
        buttons = ["Sensor"];
        if ( ! mode ) buttons += "SetRadius";

        reportDialog( _id, "Please choose an option below:", buttons );
    }

    else if ( _option == "Sensor" )
    {
        if ( mode ) {
            buttons = ["Normal"];
            desc = "\nCurrent sensor: collision";
        }

        else {
            desc = "\nCurrent sensor: normal";
            buttons = ["Collision"];
        }

        reportDialog( _id, desc + "\n\nThere are two available sources where I can find the joining avatars. The normal sensor will catch every 2 seconds all near avatars on a specified radius. The collision sensor will catch only that avatars which pulls the greeters.", buttons );
    }

    else if ( _option == "Normal" )
    {
        mode = 0;
        report( "The normal sensor has been set.\nRadius: " + (string) radius + "", "Action done!", FALSE, TRUE );
    }

    else if ( _option == "Collision" )
    {
        mode = 1;
        report( "The collision sensor has been set.", "Action done!", FALSE, TRUE );
    }

    else if (_option == "SetRadius"){
        action = 1;
        reportDialog( _id, "Please choose the radius:", ["5","10","15","30","40","50","60","75","92"] );
    }

    else if ( action == 1 )
    {
        report( "The normal sensor radius has been set to: " + (string) _option +"!\n\n", "", FALSE, FALSE );

        action = 0;
        radius = (float) _option;
    }

    else if ( _option == "On")
        if ( mode ) return 3;
    else return 2;

    else if ( _option == "Off" )
        return 1;

    else return -1;

    return 0;
}

linkMessage( integer _sender_num, integer _num, string _str, key _id )
{
      list params = llCSV2List( _id );
       key this   = llGetInventoryKey( llGetScriptName() );
       key script = llList2Key( params, 0 );
       key target = NULL_KEY;
    string param;
    
    if ( llGetListLength( params ) > 1 )
    {
        if ( llGetListLength( params ) > 2 )
            param = llList2String( params, 2 );
            
        target = llList2Key( params, 1 );
    }
    
    if ( debug )
        if ( ( _str == "DIALOG" && script == this ) || ( _str != "DIALOG" ) )
        {
            llSay(0, "OK" );
            string text;
    
            text = llGetScriptName() + "(" + (string) this + ")\n";
            text += "Command received: " + _str + "\n";
            text += "Requester: " + llKey2Name( target ) + "(" + (string) script + ")\n";
            text += "Parameter: " + param + "\n";
    
            report( text, "Request received: " + _str, FALSE, TRUE );
        }
        
    if ( _str == "RESETSCRIPT" )
    {
        report( "Restarting module Greeter...", "Resetting...", FALSE, TRUE );
        llResetScript( );
    }
        
    else if ( script == this )
        if ( _str == "DIALOG" )
            menu( target );
}    

list getOnlineAgents( list detected )
{
    integer x;
    list onlineAgents;
    // Clean lost visitors
    for( x = 0; x < llGetListLength( detected ); ++x )
        if ( llListFindList( greetedCache, [llList2Key( detected, x )] ) != -1 )
        onlineAgents += llList2Key( detected, x );

    return onlineAgents;
}

default
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
        linkMessage( _sender_num, _num, _str, _id );
    }
    
    state_entry()
    {            
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            dialogsChannel,
            "",
            "",
            ""
        );
    }

    listen(integer _chan, string _name, key _id, string _option)
    {
        integer result = processRequest( _chan, _name, _id, _option );

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "", TRUE, TRUE );

        else if ( result == 2 ) state NormalSensor;

        else if ( result == 3 ) state CollisionSensor;

        else if ( result ) state default;
    }
}

state NormalSensor
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
        linkMessage( _sender_num, _num, _str, _id );
    }
    
    state_entry()
    {
        running = TRUE;
        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            radius,
            PI,
            1.0
                );

        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            dialogsChannel,
            "",
            llGetOwner(),
            ""
                );

        report( "Sensor ready and working. (radius:" + (string) radius + ")", "Service started\n[Normal Mode]", FALSE, TRUE );
    }

    state_exit()
    {
        greetedCache = [];
        running = FALSE;
        report( "Hey, you just shutted me up! :\\ \n\n", "Service stopped", FALSE, TRUE );
        llSensorRemove();
        llListenRemove(dialogsChannel);
    }

    listen( integer _chan, string _name, key _id, string _option )
    {
        integer result = processRequest( _chan, _name, _id, _option );

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "", TRUE, TRUE );

        else if ( result == 2 ) state NormalSensor;

        else if ( result == 3 ) state CollisionSensor;

        else if ( result ) state default;
    }

    sensor( integer _total_number )
    {
        key agent;
        string url;
        list detected;

        integer x;
        for( x = 0; x < _total_number; ++x )
        {
            agent = llDetectedKey(x);
            detected += agent;

            if ( llDetectedVel(x) == <0,0,0> && llListFindList( greetedCache, [agent] ) == -1 && isPositionNear( llDetectedPos( x ) ) )
            {
                greetedCache += agent;
                llMessageLinked( LINK_THIS, 0, "JOIN", llList2CSV( [ NULL_KEY, agent, llDetectedName(x) ] ) );
                // GOTTA GREET
            }
            // TOTEST: ( with normal and collision sensors )
            //else if ( llListFindList( greeted, [agent] ) == -1 ) detected += llDetectedKey( x );
        }
        // TODO: Notifica players left
        greetedCache = getOnlineAgents( detected );
    }

    no_sensor()
    {
        // notify the last greeted and then clean the list
        greetedCache = [];
    }
}
 
state CollisionSensor
{
    link_message( integer _sender_num, integer _num, string _str, key _id )
    {
        linkMessage( _sender_num, _num, _str, _id );
    }
    
    state_entry()
    {
        running = TRUE;
        dialogsChannel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            dialogsChannel,
            "",
            "",
            ""
        );

        report( "Collision sensor ready and working.", "Service started\n[Collision Mode]", FALSE, TRUE );
    }

    state_exit()
    {
        greetedCache = [];
        running = FALSE;
        report( "Hey, you just shutted me up! :\\ \n\n", "Service stopped", FALSE, TRUE );
        llListenRemove( dialogsChannel );
    }

    listen( integer _chan, string _name, key _id, string _option )
    {
        integer result = processRequest( _chan, _name, _id, _option );

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "", TRUE, TRUE );

        else if ( result == 3 ) state CollisionSensor;

        else if ( result == 2 ) state NormalSensor;

        else if ( result ) state default;
    }

// OTTIMIZZARE ALGORITMO
    collision_start( integer total_number )
    {
        key agent;
        string url;
        list detected;

        integer x;
        for( x = 0; x < total_number; ++x )
        {
            agent = llDetectedKey(x);
            detected += agent;

            if ( llDetectedVel(x) == <0,0,0> && llListFindList( greetedCache, [agent] ) == -1 )
            {
                greetedCache += agent;
                llMessageLinked( LINK_THIS, 0, "JOIN", llList2CSV( [ NULL_KEY, agent, llDetectedName(x) ] ) );
                // GOTTA GREET
            }
            // TOTEST: ( with normal and collision sensors )
            //else if ( llListFindList( greeted, [agent] ) == -1 ) detected += llDetectedKey( x );
        }
        // TODO: Notifica players left
        greetedCache = getOnlineAgents( detected );
    }
}