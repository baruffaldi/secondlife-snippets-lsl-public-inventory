string header;

integer channel;
integer radius;
integer actionStatus;

integer name;
integer born;
integer info;
integer payi;
integer rate;

list order_buttons(list c)
{
    return c;
}

default
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llDialog(
            llGetOwner(),
            header + "Please choose the radius:",
            order_buttons(["5","10","15","20","30","40","50","70","90"]),
            channel
        );
    }
    
    state_exit()
    {
        llSay(
            0,
            header + "Hey, I was getting fine by repeating 1000 times the same stuff %)\n\n"
        );
    }

    touch_start(integer total_number)
    {
        string button;
        
        if(actionStatus) button = "Stop";
        else button = "Start";
        llDialog(
            llDetectedKey(0),
            header + "Please choose the radius:",
            order_buttons([button, "SetRadius"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if (_option == "SetRadius"){
            llDialog(
                llGetOwner(),
                header + "Please choose the radius:",
                order_buttons(["5","10","15","20","30","40","50","70","90"]),
                channel
            );
        }
        else if (!actionStatus && _option == "Start")
        {
            actionStatus = TRUE;
            llSensorRepeat( "",
                NULL_KEY,
                AGENT,
                radius,
                PI,
                5
            );
        
            llSay(
                0,
                header + "Right now I begin to greet everyone join near to me!\n\n"
            );
        }
        else if( actionStatus && _option == "Stop")
        {
            actionStatus = FALSE;
            llSensorRemove();
        
            llSay(
                0,
                header + "Hey, you just shutted me up! :\\ \n\n"
            );
        } else {
            llSay(
                0,
                header + "The new radius is: " + (string) _option +"!\n\n"
            );

            radius = (integer)_option;
        }
    } 
     
    dataserver(key queryid, string data)
    {
        if ( queryid == queryid ) {
            data = data;
            data += "\nName:           ";
            data += "\nBirthday:       ";
            data += "\nRich:           ";
            data += "\nBank:           ";
            data += "\nRating:         ";
        }
    }
    
    sensor(integer total_number)
    {
        if(actionStatus)
        {
            llInstantMessage(llGetOwner(), "" );
            integer x;
            for(x=0;x<total_number;x++)
            {
                string data;
                key agent = llDetectedKey(x);
                
                 llRequestAgentData( agent, DATA_NAME);
                 llRequestAgentData( agent, DATA_BORN);
                 llRequestAgentData( agent, PAYMENT_INFO_USED);
                 llRequestAgentData( agent, DATA_PAYINFO);
                 llRequestAgentData( agent, DATA_RATING);
                
                data = "\n\nUUID:           " + (string)agent;
                data += "\nLanguage:       " + llGetAgentLanguage( agent );
                
                integer buf = llGetAgentInfo(agent);
                if(buf & AGENT_FLYING)
                    data += "\nThe Pirla is flying";
                if(buf & AGENT_ATTACHMENTS)
                    data += "\nThe Pirla got something back";
                if(buf & AGENT_MOUSELOOK)
                    data += "\nThe Pirla is watching some boobs because he/she got mouselook mode enabled!";
                if(buf & AGENT_SCRIPTED)
                    data += "\nThe Pirla is a robot";
                if(buf & AGENT_SITTING)
                    data += "\nThe Pirla is sit";
                if(buf & AGENT_ON_OBJECT)
                    data += "\nThe Pirla is an object";
                if(buf & AGENT_AWAY)
                    data += "\nThe Pirla is away";
                if(buf & AGENT_BUSY)
                    data += "\nThe Pirla is busy";
                if(buf & AGENT_WALKING)
                    data += "\nThe Pirla is walking";
                if(buf & AGENT_IN_AIR)
                    data += "\nThe Pirla is in the air";
                if(buf & AGENT_TYPING)
                    data += "\nThe Pirla is writing some bullshit!";
                if(buf & AGENT_CROUCHING)
                    data += "\nThe Pirla is crouching";
                if(buf & AGENT_ALWAYS_RUN)
                    data += "\nThe Pirla is doped because he/she got always-run mode enabled!";
            }

        }
        else if(actionStatus == 2)
        {
            llInstantMessage(llGetOwner(), "" );
        }
    }
}