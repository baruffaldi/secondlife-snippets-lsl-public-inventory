integer requestsChannel = -1479788288;

default
{
    state_entry()
    {
        llListenRemove(requestsChannel);
        llSay(0, "Requests channel: " + (string) requestsChannel );
        llListen(
            requestsChannel,
            "",
            "",
            ""
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        llOwnerSay("ok");
        llOwnerSay((string) _chan + " " + _name + " " + (string) _id + " " + _option );
    }
}
 