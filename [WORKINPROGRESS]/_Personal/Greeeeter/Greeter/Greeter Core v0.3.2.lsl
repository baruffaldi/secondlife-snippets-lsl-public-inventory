//***************************** TODO: Funzione per inserire il nick al posto della macro [USER]
//***************************** TODO: Enhanching user informations

// TODO: Uso changed inventory per settare la presenza dei plugins
// TODO: Back-End store algoritms
// TODO: Inserire quando escono dalla land
// TODO: Funzione tappetino ( lista di destinatari per warnings + gestione )


// TODO: Dialog greeter
// TODO: Uso listen puntato sui vari e precisi _id usando canali differenti


// TODO: Promo Spamming ( Say, Shout, RegionSay ) ( Interval, String )


// TODO: Uso unixtime con timeout su greeted ( con timeout impostabile da dialog )
// TODO: Uso notecard per i testi ( con funzione per prelevare dalla list la stringa giusta )


// TODO: Funzione Auth-Key per back-end con generazione nuova key ogni minuto
// TODO: funzioni backend per autospegnere dal pannello web  (callbacks)

// TODO: Bug cambio canale...

// TODO: Compressione codice tipo JS
// TODO: Ottimizzazione logiche stati

float   radius = 92.0; // normal sensor radius

integer unspent = 16384;
integer wasted  = 313;
integer mode    = 0; // 0 - normal sensor | 1 - collision sensor
integer channel = 0;
integer action  = 0; // Defines which type of action has been taken

integer dataReceiverChannel = -1479788288; // Data receiver comunication channel
integer dialogSenderChannel = -1479788289; // Dialog sender comunication channel

integer running            = FALSE;
integer dataStoreStatus    = FALSE;
integer dGreeterStatus     = FALSE;
integer greeterStatus      = FALSE;
integer loggerStatus       = FALSE;
integer ownerWarningStatus = TRUE;

string  region;
string  header = "*** Fancy Greeter v0.3.2 ***\n";

string  greeterType   = "Private";
string  greeterString = "Welcome [USER] on [REGION]! Today we have a [WIND] powerful wind and a cloud of [CLOUD]. Have a good time...";

string  loggerUrl     = "http://supremacyclubcity.com/lsl/backend/logger";

list greeted;

vector  VNULL   = <0.0, 0.0, 0.0>;
vector  white   = <1.0, 1.0, 1.0>;
vector  grey    = <0.5, 0.5, 0.5>;
vector  black   = <0.0, 0.0, 0.0>;
vector  red     = <1.0, 0.0, 0.0>;
vector  green   = <0.0, 1.0, 0.0>;
vector  blue    = <0.0, 0.0, 1.0>;
vector  yellow  = <1.0, 1.0, 0.0>;
vector  cyan    = <0.0, 1.0, 1.0>;
vector  magenta = <1.0, 0.0, 1.0>;
  
integer getSpentBytes()
{
    return ( unspent - wasted ) - llGetFreeMemory();
}

menu()
{
    string changeStatus;
    if ( running ) changeStatus = "Stop";
    else changeStatus = "Start";

    llDialog(
        llGetOwner(),
        header + "\n\n\nClick on \"Stop\" to shut me up or \"Start\" to begin the greetings!",
        [changeStatus, "Settings", "Show"],
        channel
            );
}

string parseString( string text, string name )
{
    list   tmp;
    string ret;

    tmp = llParseString2List( text, ["[USER]"], []);
    ret = llDumpList2String( tmp, name );

    tmp = llParseString2List( ret, ["[REGION]"], []);
    ret = llDumpList2String( tmp, region );

    tmp = llParseString2List( ret, ["[WIND]"], []);
    ret = llDumpList2String( tmp, (string)llWind(ZERO_VECTOR) );

    tmp = llParseString2List( ret, ["[CLOUD]"], []);
    ret = llDumpList2String( tmp, (string)llCloud(ZERO_VECTOR) );

    return ret;
}

report( string text, string fliptitle )
{
    llOwnerSay( "\n" + header + text );
    if ( fliptitle != "" )
    {
        llSetText( fliptitle, green, 1.0 );
        llSleep(2);
        llSetText( "", VNULL, 0.0 );
    }
}

reportDialog( string text, list buttons )
{
    llDialog(
        llGetOwner(),
        header + "\n" + text,
        buttons,
        channel
    );
}

list getOnlineAgents( list detected )
{
    integer x;
    list onlineAgents;
    // Clean lost visitors
    for( x = 0; x < llGetListLength( detected ); ++x )
        if ( llListFindList( greeted, [llList2Key( detected, x )] ) != -1 )
        onlineAgents += llList2Key( detected, x );

    return onlineAgents;
}

string buildAgentInfoGetParams( integer buf )
{
    string url;
    
    if( buf & AGENT_FLYING )      url += "&fly=1";
    if( buf & AGENT_ATTACHMENTS ) url += "&haveAttachments=1";
    if( buf & AGENT_MOUSELOOK )   url += "&mouseLook=1";
    if( buf & AGENT_SCRIPTED )    url += "&scripted=1";
    if( buf & AGENT_SITTING )     url += "&sit=1";
    if( buf & AGENT_ON_OBJECT )   url += "&onObject=1";
    if( buf & AGENT_AWAY )        url += "&away=1";
    if( buf & AGENT_BUSY )        url += "&busy=1";
    if( buf & AGENT_WALKING )     url += "&walking=1";
    if( buf & AGENT_IN_AIR )      url += "&air=1";
    if( buf & AGENT_TYPING )      url += "&typing=1";
    if( buf & AGENT_CROUCHING )   url += "&crouched=1";
    if( buf & AGENT_ALWAYS_RUN )  url += "&alwaysRun=1";

    return url;
}

integer processRequest(integer _chan, string _name, key _id, string _option)
{
    list buttons;
    string desc;

    // *** Local-Chat specified settings
    if ( _chan == 3 )
    {
        // *** Greeter
        if ( action == 2 )
        {
            action = 0;
            greeterString = _option;
            llListenRemove(3);
            report( "The greeter string has been set to: " + _option, "Action done!" );
        }

        // *** Logger
        else if ( action == 4 )
        {
            action = 0;
            loggerUrl = _option;
            llListenRemove(3);
            report( "The logger back-end URL has been set to: " + _option, "Action done!" );
        }
        return 0;
    }

    // *** llDialog specified settings
    if ( _option == "Settings" )
    {
        buttons += ["D-Greeter", "Greeter", "Logger", "DataStore", "PromoShouter", "Options", "Reset"];

        reportDialog( "NOTE: you can change the settings even if the sensor is active.\n\nPlease choose an option below:", buttons );
    }

    else if ( _option == "Show" ) 
    {
        desc += "\n** Memory free: " + (string) llGetFreeMemory() + "\n";
        desc += "\n** Memory used: " + (string) getSpentBytes() + "\n";

        if ( mode ) desc += "\n** Sensor: collision";
        else desc += "\n** Sensor: normal";

        if ( ! mode ) desc += "\n* Radius: " + (string) radius;

        if ( greeterStatus ) {
            desc += "\n\n** Greeter";
            desc += "\n* Type: " + greeterType;
            desc += "\n* String: " + greeterString;
        }

        else desc += "\n\n** Greeter disabled";

        if ( dGreeterStatus ) {
            desc += "\n\n** Dialog Greeter enabled";
        }

        else desc += "\n\n** Dialog Greeter disabled";

        if ( loggerStatus ) {
            desc += "\n\n** Logger";
            desc += "\n* URL: " + loggerUrl;
        }

        else desc += "\n\n** Logger disabled";

        if ( dataStoreStatus ) {
            desc += "\n\n** Data Store enabled";
            //            desc += "\n* URL: " + dataStoreUrl;
        }

        else desc += "\n\n** Data Store disabled";

        report( desc, "" );
    }

    else if ( _option == "Options" )
    {
        buttons = ["Sensor"];
        if ( ! mode ) buttons += "SetRadius";

        reportDialog( "Please choose an option below:", buttons );
    }

    else if ( _option == "Sensor" )
    {
        if ( mode ) {
            buttons = ["Normal"];
            desc = "\nCurrent sensor: collision";
        }

        else {
            desc = "\nCurrent sensor: normal";
            buttons = ["Collision"];
        }

        reportDialog( desc + "\n\nThere are two available sources where I can find the joining avatars. The normal sensor will catch every 2 seconds all near avatars on a specified radius. The collision sensor will catch only that avatars which pulls the greeters.", buttons );
    }

    else if ( _option == "Normal" )
    {
        mode = 0;
        report( "The normal sensor has been set.\nRadius: " + (string) radius + "", "Action done!" );
    }

    else if ( _option == "Collision" )
    {
        mode = 1;
        report( "The collision sensor has been set.", "Action done!" );
    }

    else if ( _option == "Reset" )
    {
        action = -30;
        reportDialog( "*** WARNING ***\n\nBy resetting the script, you will lost all your settings.\n\nAre you really really really sure about what you're doing?", ["Sure!"] );
    }

    else if ( _option == "Sure!" && action == -30 )
    {
        action = 0;
        report( "The script configuration has been reset.", "Action done!" );
        llResetScript();
    }

    else if (_option == "SetRadius"){
        action = 1;
        reportDialog( "Please choose the radius:", ["5","10","15","30","40","50","60","75","92"] );
    }

    else if ( action == 1 )
    {
        report( "The normal sensor radius has been set to: " + (string) _option +"!\n\n", "" );

        action = 0;
        radius = (float) _option;
    }

    else if ( _option == "Greeter" )
    {
        action = 2;
        if ( greeterStatus ) buttons = ["Off", "SetString"];
        else buttons = ["On"];

        if ( greeterStatus && greeterType == "Private" ) buttons += "Public";
        else if ( greeterStatus && greeterType == "Public" ) buttons += "Private";

        reportDialog( "The greeter functionality gives you the chance to make me say something whenever someone join the land\nTo choose the string you have to click on \"SetString\" that only appears when this functionality is enabled.", buttons );
    }

    else if ( _option == "SetString" )
    {
        report( "You can type your greet string on channel 3.", "Waiting..." );

        llListenRemove(3);
        llListen(
            3,
            "",
            llGetOwner(),
            ""
                );
    }

    else if ( action == 2 )
    {
        action = 0;

        if ( _option == "On" )
            greeterStatus = TRUE;
        else if ( _option == "Off" )
            greeterStatus = FALSE;

        else if ( _option == "Public" )
            greeterType = _option;

        else if ( _option == "Private" )
            greeterType = _option;

        if ( greeterStatus ) _option = "enabled";
        else _option = "disabled";

        if ( greeterType == "Public" ) _option += " and public.";
        else _option += " and private.";

        report( "The greeter is now " + _option + "\nType: " + greeterType + "\nString: " + greeterString, "" );
    }

    else if ( _option == "D-Greeter" )
    {
        action = 3;
        if ( dGreeterStatus ) buttons = ["Off"];
        else buttons = ["On"];

        reportDialog( "The dialog greeter functionality gives you the chance to make me send an useful dialog with all region's informations", buttons );
    }

    else if ( action == 3 )
    {
        action = 0;

        if ( _option == "On" )
            dGreeterStatus = TRUE;
        else if ( _option == "Off" )
            dGreeterStatus = FALSE;

        if ( dGreeterStatus ) _option = "enabled.";
        else _option = "disabled.";

        report( "The dialog greeter is now " + _option, "Action done!" );
    }

    else if ( _option == "Logger" )
    {
        action = 4;
        if ( loggerStatus ) buttons = ["Off", "SetURL"];
        else buttons = ["On"];

        reportDialog( "The logger functionality gives you the chance to make me keep tracked all land vitors through a webserver back-end.", buttons );
    }

    else if ( action == 4 )
    {
        if ( _option == "SetURL" )
        {
            report( "You can type the logger back-end URL on channel 3.", "Waiting..." );

            llListenRemove(3);
            llListen(
                3,
                "",
                llGetOwner(),
                ""
                    );
        } else {
                action = 0;
            if ( _option == "On" )
                loggerStatus = TRUE;
            else if ( _option == "Off" )
                loggerStatus = FALSE;

            if ( loggerStatus ) _option = "enabled.";
            else _option = "disabled.";

            report( "The logger is now " + _option + "\nURL: " + loggerUrl, "Action done!" );
        }
    }

    else if ( _option == "DataStore" )
    {
        action = 5;
        if ( dataStoreStatus ) buttons = ["Off"];
        else buttons = ["On"];

        reportDialog( "The data store functionality gives you the chance to make me keep tracked all land vitors informations like birthday, payment type and some others stuff.", buttons );
    }

    else if ( action == 5 )
    {
        action = 0;

        if ( _option == "On" )
            dataStoreStatus = TRUE;
        else if ( _option == "Off" )
            dataStoreStatus = FALSE;

        if ( dataStoreStatus ) _option = "enabled.";
        else _option = "disabled.";

        report( "The data store is now " + _option, "Action done!" );
    }

    else if ( _option == "Start")
        if ( mode ) return 3;
    else return 2;

    else if ( _option == "Stop" )
        return 1;

    else return -1;

    return 0;
}

default
{
    changed(integer change)
    {
        if (change & CHANGED_OWNER)
        {
            llResetScript();
        }
        if (change & CHANGED_REGION)
        {
            region = llGetRegionName();
        }
    }

    on_rez(integer bho)
    {
    }

    state_entry()
    {
        if ( region == "" )
            region = llGetRegionName();

        channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
                );
    }

    touch_start(integer total_number)
    {
        if( llDetectedKey( 0 ) == llGetOwner( ) )
        {
            action = 0;
            menu();
        }
    }

    listen(integer _chan, string _name, key _id, string _option)
    {
        integer result = processRequest( _chan, _name, _id, _option );
        // -1 = not exists
        // 0 = action done
        // 1 = service stop
        // 2 = service start normal
        // 3 = service start collision

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "" );

        else if ( result == 2 ) state NormalSensor;

        else if ( result == 3 ) state CollisionSensor;

        else if ( result ) state default;
    }
}

state NormalSensor
{
    state_entry()
    {
        running = TRUE;
        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            radius,
            PI,
            1.0
                );

        channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
                );

        report( "Sensor ready and working. (radius:" + (string) radius + ")", "Service started\n[Normal Mode]" );
    }

    state_exit()
    {
        running = FALSE;
        report( "Hey, you just shutted me up! :\\ \n\n", "Service stopped" );
        llSensorRemove();
        llListenRemove(channel);
    }

    touch_start(integer total_number)
    {
        if( llDetectedKey( 0 ) == llGetOwner( ) )
        {
            action = 0;
            menu();
        }
    }

    listen(integer _chan, string _name, key _id, string _option)
    {
        integer result = processRequest( _chan, _name, _id, _option );

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "" );

        else if ( result == 2 ) state NormalSensor;

        else if ( result == 3 ) state CollisionSensor;

        else if ( result ) state default;
    }

    sensor(integer total_number)
    {
        key agent;
        string url;
        list detected;

        integer x;
        for( x = 0; x < total_number; ++x )
        {
            agent = llDetectedKey(x);
            detected += agent;

            if ( llDetectedVel(x) == <0,0,0> && llListFindList( greeted, [agent] ) == -1 )
            {
                greeted += agent;
                if ( dGreeterStatus )  llSay( dialogSenderChannel, (string) agent );
                if ( dataStoreStatus ) llSay( dataReceiverChannel, (string) agent );

                if ( greeterStatus )
                    if ( greeterType == "Public" ) llSay( 0, parseString( greeterString, llDetectedName(x) ) );
                else llInstantMessage( agent, parseString( greeterString, llDetectedName(x) ) );

                
                url = loggerUrl + "?uuid=" + (string) agent;
                url += "&name=" + llDetectedName(x);
                url += "&pos=" + (string) llDetectedPos(x);
                url += "&lang=" + llGetAgentLanguage( agent );
                url += "&region=" + region;
                url += buildAgentInfoGetParams( llGetAgentInfo( agent ) );

                if ( loggerStatus ) llHTTPRequest( url, [], "" );
                report( url, "" );
            }
            // TOTEST: ( with normal and collision sensors )
            //else if ( llListFindList( greeted, [agent] ) == -1 ) detected += llDetectedKey( x );
        }
        // TODO: Notifica players left
        greeted = getOnlineAgents( detected );
    }

    no_sensor()
    {
        // notify the last greeted and then clean the list
        greeted = [];
    }
}

state CollisionSensor
{
    state_entry()
    {
        running = TRUE;
        channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );

        report( "Collision sensor ready and working.", "Service started\n[Collision Mode]" );
    }

    state_exit()
    {
        running = FALSE;
        report( "Hey, you just shutted me up! :\\ \n\n", "Service stopped" );
        llListenRemove( channel );
    }

    touch_start(integer total_number)
    {
        if( llDetectedKey( 0 ) == llGetOwner( ) )
        {
            action = 0;
            menu();
        }
    }

    listen(integer _chan, string _name, key _id, string _option)
    {
        integer result = processRequest( _chan, _name, _id, _option );

        if ( result == -1 )
            report( "I'm afraid but the functionality " + _option + " is still under development", "" );

        else if ( result == 3 ) state CollisionSensor;

        else if ( result == 2 ) state NormalSensor;

        else if ( result ) state default;
    }

// OTTIMIZZARE ALGORITMO
    collision_start( integer total_number )
    {
        key agent;
        string url;
        list detected;

        integer x;
        for( x = 0; x < total_number; ++x )
        {
            agent = llDetectedKey(x);
            detected += agent;

            if ( llDetectedVel(x) == <0,0,0> && llListFindList( greeted, [agent] ) == -1 )
            {
                greeted += agent;
                if ( dGreeterStatus )  llSay( dialogSenderChannel, (string) agent );
                if ( dataStoreStatus ) llSay( dataReceiverChannel, (string) agent );

                if ( greeterStatus )
                    if ( greeterType == "Public" ) llSay( 0, parseString( greeterString, llDetectedName(x) ) );
                else llInstantMessage( agent, parseString( greeterString, llDetectedName(x) ) );

                
                url = loggerUrl + "?uuid=" + (string) agent;
                url += "&name=" + llDetectedName(x);
                url += "&pos=" + (string) llDetectedPos(x);
                url += "&lang=" + llGetAgentLanguage( agent );
                url += "&region=" + region;
                url += buildAgentInfoGetParams( llGetAgentInfo( agent ) );

                if ( loggerStatus ) llHTTPRequest( url, [], "" );
                if ( ownerWarningStatus ) llOwnerSay( llDetectedName(x) + " has just joined the land" );
            }
            // TOTEST: ( with normal and collision sensors )
            //else if ( llListFindList( greeted, [agent] ) == -1 ) detected += llDetectedKey( x );
        }
        // TODO: Notifica players left
        greeted = getOnlineAgents( detected );
    }
}