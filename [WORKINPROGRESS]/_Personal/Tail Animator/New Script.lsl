list Gestures = [1];
integer Channel;
integer Listen;
string Owner;

// ---------// ---------// ---------// ---------// ---------
 
 string notecardName = "Gestures";
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

// --------------------------------------------------------

report( string _fliptitle, vector _color )
{
    llSetText( _fliptitle, _color, 1.0 );
}

talk( string Text )
{    
    string Name = llGetObjectName();
    llSetObjectName("");
    llSay(PUBLIC_CHANNEL, "/me " + Text);
    llSetObjectName(Name);
}


list DictGetElems(list dict, integer elem)
{
    integer elements = llList2Integer(dict,0);
    return llList2ListStrided(llList2List(dict,elem+2,-1),0,-1,elements+1);
}

list DictGetKeys(list dict)
{
    return DictGetElems(dict, -1);
}

string StrReplace(string source, string pattern, string replace) {
    while (llSubStringIndex(source, pattern) > -1) {
        integer len = llStringLength(pattern);
        integer pos = llSubStringIndex(source, pattern);
        if (llStringLength(source) == len) { source = replace; }
        else if (pos == 0) { source = replace+llGetSubString(source, pos+len, -1); }
        else if (pos == llStringLength(source)-len) { source = llGetSubString(source, 0, pos-1)+replace; }
        else { source = llGetSubString(source, 0, pos-1)+replace+llGetSubString(source, pos+len, -1); }
    }
    return source;
}

default
{
    state_entry()
    {
        Owner = llKey2Name( llGetOwner() );
        Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        Listen = llListen( Channel, "", "", "" );
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
        {
            if ( ! notecardTotalLines )
                state UpdateGestures;
            report( "", ZERO_VECTOR );
        }
        else report(
                    "I'm sorry, I'm out of order.. retry later.",
                    <1.0,1.0,1.0>
                );
    }
    
    changed( integer _change )
    {
        if ( _change & CHANGED_INVENTORY )
            if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
                state UpdateGestures;  
    }
    
    touch_start(integer _c)
    {
        if ( notecardTotalLines )
        {
            if ( llDetectedKey(0) == llGetOwner() )
                llDialog( llGetOwner(), "\nChoose an option below", DictGetKeys(Gestures), Channel );
            else
                llDialog( llDetectedKey(0), "\nChoose an option below", DictGetKeys(Gestures), Channel );
        }
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        integer Index = llListFindList( Gestures, [_m] ) + 1;
        if ( Index > 1 )
            talk( StrReplace( StrReplace( llList2String(Gestures, Index), "[OWNER]", Owner ), "[TARGET]", _n ) );
    }
}

state UpdateGestures
{
    state_entry()
    {
        notecardName = llGetInventoryName( INVENTORY_NOTECARD, 0 );
        
        if ( notecardName == "" ) {
            llOwnerSay( "You have to create a notecard with all targets" );
            state default;
        }
            
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                report(
                    "Notecard reading... 0%",
                    <1.0,1.0,1.0>
                );

                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
            }
    }
    
    changed( integer _change )
    {
        if ( _change & CHANGED_INVENTORY )
            if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
                state UpdateGestures;  
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                report(
                    "Notecard reading... " + (string) (( notecardLine * 100 ) / notecardTotalLines) + "%",
                    <1.0,1.0,1.0>
                );
                
                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )                    
                    Gestures += llParseString2List( _data, ["|"], [] );

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else {
            report(
                "Notecard reading completed!",
                <1.0,1.0,1.0>
            );
            llSleep(1.0);
            state default;
        }
    } 
}
