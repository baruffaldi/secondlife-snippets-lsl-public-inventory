// Set Text v0.4 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ Filippo Baruffaldi <filippo@baruffaldi.info>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
//

integer timeToChange = 1800;
integer cycle = FALSE;

vector white = <1.0, 1.0, 1.0>;
vector grey = <0.5, 0.5, 0.5>;
vector black = <0.0, 0.0, 0.0>;
vector red = <1.0, 0.0, 0.0>;
vector green = <0.0, 1.0, 0.0>;
vector blue = <0.0, 0.0, 1.0>;
vector yellow = <1.0, 1.0, 0.0>;
vector cyan = <0.0, 1.0, 1.0>;
vector magenta = <1.0, 0.0, 1.0>;

list colors = [white,grey,black,red,green,blue,yellow,cyan,magenta];
list bannedColors = [3,9];

string text;
vector color = white;
integer random;

integer randInt(integer n)
{
     return (integer)llFrand(n + 1);
}

integer rand(integer min, integer max)
{
    return min + randInt(max - min);
}

doCycle()
{
    integer r = rand(0,8);
    integer statez = TRUE;
    
    while(statez)
    {
        if(!(~llListFindList(bannedColors, [llList2Vector(colors, r)])))
        {
            color = llList2Vector(colors, r);
            statez = FALSE;
        } else r = rand(0,8);
    }
}


doRandom()
{
    color = <llFrand(1.0), llFrand(1.0), llFrand(1.0)>;
}

default 
{
    on_rez(integer x)
    {
        llSetText( text, black, 1 );
    }
    
    state_entry()
    {
        llSetTimerEvent(0.5);
        llSetText(text, color, 1);
        llListen(1, "", llGetOwner(), "");
        llListen(2, "", llGetOwner(), "");
        llListen(3, "", llGetOwner(), "");
    }
    
    timer() 
    {
        if(cycle) doCycle();
        else if (random) doRandom();
        
        llSetText(text, color, 1);
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if( _chan == 1 )
        {
            if ( cycle && (float)_option == .0 ) return;
            else if ( random && (float)_option == .0 ) return;

            llSetTimerEvent( (float) _option );
        }
        
        else if( _chan == 2 ) {
                 if(_option == "white")   color = white;
            else if(_option == "grey")    color = grey;
            else if(_option == "black")   color = black;
            else if(_option == "red")     color = red;
            else if(_option == "green")   color = green;
            else if(_option == "blue")    color = blue;
            else if(_option == "yellow")  color = yellow;
            else if(_option == "cyan")    color = cyan;
            else if(_option == "magenta") color = magenta;
            
            else if(_option == "list")
                llInstantMessage(llGetOwner(), "The available colors are: white, grey, black, red, green, blue, yellow, cyan, magenta");
                
            else if(_option == "help")
                llInstantMessage(llGetOwner(), "In order to change the color you have to type /2 color, example: /2 red");
            
            if(_option == "random")   random = TRUE;
            else random = FALSE;
            
            if(_option == "cycle")   cycle = TRUE;
            else cycle = FALSE;
            
            if ( random == FALSE && cycle == FALSE ) llSetTimerEvent(.0);
        }
        
        else if( _chan == 3 )
        {
            if( _option == "help" )
                llInstantMessage(
                    llGetOwner(),
                    "In order to change the text you have to type /3 text, example: /2 I'm a real noob"
                );
            else text = _option;
        }
    }
} 