string text = "Dumb who reads :)";

float baseColorR = 0.25;
float baseColorG = 1.0;
float baseColorB = 0.64;

integer reverse = FALSE;

default
{
    on_rez(integer x)
    {
        llResetScript();
        llSetText("", <baseColorR,baseColorG,baseColorB>, 1);
    }
    
    state_entry()
    {
        llSetTimerEvent(1);
    }
    
    
    timer()
    {
        if( baseColorR >= 9 || baseColorG >= 9 || baseColorB >= 9 ) reverse = TRUE;
        else if( baseColorR <= 1.00 ||baseColorG <= 1.00 ||baseColorB <= 1.00 ) reverse = FALSE;
        
        if(reverse)
        {
            baseColorR - 1.00;
            baseColorG - 1.00;
            baseColorB - 1.00;
        } else {
            baseColorR + 1.00;
            baseColorG + 1.00;
            baseColorB + 1.00;
        }
        
        llSetText(text, <baseColorR,baseColorG,baseColorB>, 1);
    }
} 