// Set Text v0.4 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ Filippo Baruffaldi <filippo@baruffaldi.info>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
//

vector white = <1.0, 1.0, 1.0>;
vector grey = <0.5, 0.5, 0.5>;
vector black = <0.0, 0.0, 0.0>;
vector red = <1.0, 0.0, 0.0>;
vector green = <0.0, 1.0, 0.0>;
vector blue = <0.0, 0.0, 1.0>;
vector yellow = <1.0, 1.0, 0.0>;
vector cyan = <0.0, 1.0, 1.0>;
vector magenta = <1.0, 0.0, 1.0>;

//*** CONFIGURE HERE THE TEXT AND COLOR
string text = "Dumb who reads :)";
vector color = green;
//*** CONFIGURATION ENDS HERE

default
{
    on_rez(integer x)
    {
        llResetScript();
        llSetText("", black, 1);
    }
    
    state_entry()
    {
        llSetTimerEvent(1);
    }
    
    
    timer()
    {   
        llSetText(text, color, 1);
    }
} 