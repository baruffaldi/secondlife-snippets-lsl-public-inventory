integer Listen;
integer Listen2;

Talk( string String, integer Type, key Target )
{
    string Name = llGetObjectName();
    llSetObjectName("");
    if ( Type == 0 )
        llOwnerSay("/me " + String);
    else if ( Type == 1 )
        llWhisper(0, "/me " + String);
    else if ( Type == 2 )
        llSay(0, "/me " + String);
    else if ( Type == 3 )
        llShout(0, "/me " + String);
    else if ( Type == 4 )
        llRegionSay(0, "/me " + String);
    else if ( Type == 5 )
        llInstantMessage(Target, "/me " + String);
    llSetObjectName(Name);
}

default
{
    state_entry()
    {
        Listen = llListen(0, "", "", "!help" );
        Listen2 = llListen(0, "", "", "!usage" );
        
        llSetText( "Type on localchat: !help", <0.38324, 0.01298, 0.99862>, 1 );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        Talk( "\n\n" +
        "!google - Perform a google search\n"+
        "!flickr - Perform a flickr search\n"+
        "!youtube - Perform a youtube search\n"+
        "---\n"+
        "!blogsearch - Perform an official secondlife blog's search by tags\n"+
        "!blogreader - Start an official secondlife specific blog reading\n"+
        "---\n"+
        "!lindenpodcast - Get the official LindenLab Podcast\n"+
        "!lindenblogs - Get the official LindenLab blogs latest posts\n"+
        "!lindenfeatures - Get the official LindenLab blogs latest features\n"+
        "!lindenjira - Get the last submitted tickets\n"+
        "!gridstatus - Get the grid status\n"+
        "!griddetailedstatus - Get the detailed grid status\n"+
        "---\n"+
        "!feedreader - Start a feed reading by feed url\n"+ 
        "!feedtitlesreader - Start a feed titles reading by feed url", 
        5, _k );
    }
}