key HttpRequestId;

string SearchBaseURL = "http://geezmo-dbal.appspot.com/sl/feeds/titles-reader?t=csv&q=http%3A%2F%2Ffeeds2.feedburner.com%2FLindenLabALL";
string Search;

integer Listen;
integer ListenSearch;

Talk( string String, integer Type, key Target )
{
    string Name = llGetObjectName();
    llSetObjectName("");
    if ( Type == 0 )
        llOwnerSay("/me " + String);
    else if ( Type == 1 )
        llWhisper(0, "/me " + String);
    else if ( Type == 2 )
        llSay(0, "/me " + String);
    else if ( Type == 3 )
        llShout(0, "/me " + String);
    else if ( Type == 4 )
        llRegionSay(0, "/me " + String);
    else if ( Type == 5 )
        llInstantMessage(Target, "/me " + String);
    llSetObjectName(Name);
}

default
{
    state_entry()
    {
        Listen = llListen(0, "", "", "!lindenblogs" );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        Talk( "Reading Linden Blogs", 1, NULL_KEY);
        HttpRequestId = llHTTPRequest( SearchBaseURL, [], "");
        Search = _m;
    }
    
    http_response( key _r, integer _s, list _m, string _b )
    {
        if (_r == HttpRequestId)
        {
            integer Length = llStringLength(_b);
llSay(0,_b);
            list lines = llParseString2List(_b, ["\n"], []);
            integer n = llGetListLength(lines);
            integer i = 1;

            for(;i<n;i++)
            {
                list Entry = llCSV2List(llList2String(lines, i));
                Talk( "\nTitle: " + llList2String(Entry, 0) + "\nURL: " + llList2String(Entry, 1), 1, NULL_KEY);
            }

            Talk( "Linden Blogs reading done with " + (string)(llGetListLength(lines)-1 )+ " items.", 1, NULL_KEY);
        }
    }
}