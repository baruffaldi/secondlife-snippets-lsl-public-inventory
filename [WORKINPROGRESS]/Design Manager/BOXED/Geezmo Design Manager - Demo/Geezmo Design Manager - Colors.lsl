// Geezmo Design Manager - Colors Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
    
key Creator = "730d4a21-c72b-4a3a-b1d5-099868bf31ec";
string PrimsTag = "GZM.DM";

integer Channel;
integer Listen;

string ModuleName;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 202;
integer GZM_REQUEST = 320;

list PrimsToUpdate = [1];
list PrimsToUpdateNames;
list AvailableColors = [1];

integer Start;
integer Checked;

string PrimToChange;

 string notecardName = "Colors";
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

ColorsMenu(key _Id, string Prims)
{
    Dialog( _Id, "Please choose a texture below to apply on the prims called: " + Prims, List2Buttons( GetDictButtons(AvailableColors, Start, 1) ), Channel );
}

PrepareColor( string Color )
{
    integer i = 1;
    
    for(;i<llGetListLength( PrimsToUpdate );i+=2)
        if ( ( PrimToChange == "All" && llList2Integer( PrimsToUpdate, i+1 ) != -1 )
            || PrimToChange != "All" && llList2String( PrimsToUpdate, i ) == PrimToChange )
            llSetLinkColor( llList2Integer( PrimsToUpdate, i+1 ), rgb2sl( (vector)llList2String( DictGetItem( AvailableColors, Color ), 0 ) ), ALL_SIDES );

    PrimToChange = "";
}

vector sl2rgb( vector sl )
{
    sl *= 255;
    return <(integer)sl.x, (integer)sl.y, (integer)sl.z>;
}

vector rgb2sl( vector rgb )
{
    return rgb / 255;
}

// -------------------------------------------------------------------

Menu(key _Id)
{
    PrimToChange = "";
    list Primsets = List2Buttons( GetButtons(PrimsToUpdateNames, Start) );
    integer Length = llGetListLength(Primsets);
    string text;
    if ( Length > 2 ) text = "Please choose a primset below:";
    else {
        text = "No one primset found.";
        Primsets = ["Ok", "Back <<"];
    }
    
    Dialog( _Id, text, List2Buttons(Primsets), Channel );

    if ( ! Length ) 
        llInstantMessage( _Id, "You have to add the prefix \""+PrimsTag+"\" on at least one object's prim name, like: "+PrimsTag+"object primset1 blah blah" );
}

list UpdateInventoryData( integer _Type )
{
    integer _i;
    integer _Length = llGetInventoryNumber(_Type);
    
    list _List;
    for(_i=0;_i<_Length;++_i)
        _List += [llGetInventoryName(_Type, _i), llGetInventoryKey(llGetInventoryName(_Type, _i))];

    return _List;
}

list UpdateObjectData( )
{
    PrimsToUpdateNames = [];
    integer NPrims = llGetObjectPrimCount(llGetKey());
    
    list _List;
    string Name;
    integer _i;
    
    if ( NPrims ) {
        PrimsToUpdateNames += "All";
        _List += ["All", -2];
    
        for(;_i<=NPrims;++_i)
        {
            Name = llGetLinkName(_i);
            if ( llStringTrim( llList2String( llParseString2List( Name, [" "],[] ), 0), STRING_TRIM ) == PrimsTag )
            {
                _List += [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) ), _i];
                if ( !~llListFindList( PrimsToUpdateNames, [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) )] ) )
                    PrimsToUpdateNames += llStringTrim( llGetSubString( Name, llStringLength(PrimsTag), llStringLength( Name ) ), STRING_TRIM );
            }
        }
    } else PrimsToUpdateNames = [1];

    return _List;
}

list GetButtons(list _List, integer _Start)
{
    integer i = _Start;
    list Return;
    integer Length = llGetListLength(_List);

    if ( Length )
    for (;i<=(_Start+9);++i)
    {
        if ( i < Length )
            Return += llList2String(_List, i);
            
        if ( ( (_Start+8 ) == i && _Start + 9 < Length ) || i == Length )
            Return += "Back <<";

        else if ( (_Start+9) == i && (_Start+10) < Length )
            Return += ">> Next";

        if ( i >= Length ) i = (_Start+11);
    }
    
    return Return;
}

list GetDictButtons(list _List, integer _Start, integer _StrideIndex)
{
    integer i = (_Start+1);
    list Return;
    integer _Length = llGetListLength(_List);
    integer _StrideLength = llList2Integer( _List, 0 );
    
    ++_StrideLength;
    ++_Start;


    if ( _Length ) 
    for (;i<=(_Start+(9*_StrideLength));)
    {
        if ( i < _Length )
            Return += llList2String(_List, i + (_StrideIndex-1));
            
        if ( ( (_Start+(8*_StrideLength)) == i || i == _Length ) && _Start != 0 )
            Return += "Back <<";

        else if ( (_Start+(9*_StrideLength)) == i && (_Start+(9*_StrideLength)) < _Length )
            Return += ">> Next";

        if ( i >= _Length ) i = (_Start+(11*_StrideLength));
        else i += _StrideLength;
    }
    
    return Return;
}

list DictGetItem(list dict, string dkey)
{
    if (dkey == "") return [];
    integer elements = llList2Integer(dict,0);
    integer loc = llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    if (loc<0)
        return [];
    else
        return llList2List(llList2List(dict,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements);
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += String2Button( llList2String( List, i ) );
        
    return Ret;
}

string String2Button( string String )
{
    return (llStringTrim( llGetSubString( String, 0, 23 ), STRING_TRIM ));
}

Report( string _fliptitle )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, <1,1,1>, 1.0 );
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Design Manager :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

string Button2String( string ButtonText, list Stack )
{
    integer i;
    integer Length = llGetListLength( Stack );
    for( ;i<Length;++i)
        if ( llStringTrim( llGetSubString( llList2String( Stack, i ), 0, 23 ), STRING_TRIM ) == ButtonText )
            return llList2String( Stack, i );
    
    return "";
}

Init()
{
    Start = 0;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
    Checked = TRUE;
}

default
{
    on_rez( integer _p )
    {
        Init();
        
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        //if (Creator != llGetCreator()) state Disabled;
        if ( ! Checked )
            state Update;

        Listen = llListen(Channel, "", "", "");
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_LINK )
            PrimsToUpdate = [1] + UpdateObjectData();
            
        if ( _c & CHANGED_INVENTORY )
            state Update;
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "");
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
        
        if ( _n == GZM_REQUEST )
        {
            PrimToChange = (string)_k;
            PrepareColor( _m );
        }
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            if ( PrimToChange != "" )
            {
                Start += 20;
                ColorsMenu( _i, _m );
            } else {
                Start += 10;
                Menu(_i);
            }
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) llMessageLinked( LINK_THIS, 210, "Core", _i );
            else {
                if ( PrimToChange != "" )
                {
                    Start -= 20;
                    ColorsMenu( _i, _m );
                } else {
                    Start -= 10;
                    Menu(_i);
                }
            }
        } else {
            if ( PrimToChange == "" )
            {
                integer index = llListFindList( PrimsToUpdateNames, [_m] );
                if ( ~index )
                {
                    Start = 0;
                    PrimToChange = _m;
                    ColorsMenu( _i, llList2String( PrimsToUpdateNames, index ) );
                    return;
                }
            }
            
            string CompleteString = Button2String( _m, AvailableColors );
            if ( CompleteString != "" )
            {
                Start = 0;
                PrepareColor( CompleteString );
                Menu(_i);
            }
        }
    }
}

state Update
{
    state_entry()
    {
        Init();
        PrimsToUpdate = [1] + UpdateObjectData();
        AvailableColors = [1, "White", <255,255,255>, "Black", ZERO_VECTOR];
        
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
        {
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                Report( "Notecard reading... 0%" );
    
                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
            } else state default;
        } else state default;
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_INVENTORY )
            state Update;
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }
        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                integer percent;
                percent = ( notecardLine * 100 ) / notecardTotalLines;

                Report( "Colors notecard reading... " + (string) percent + "%" );
                    
                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    list Note = llParseString2List( _data, ["|"], [] );
                    AvailableColors += [llList2String(Note, 1), llList2String(Note, 0)];
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else {
            Report( "Colors notecard reading completed!" );
            llSleep(1.0);
            llSetText( "", ZERO_VECTOR, 0 );
            state default;
        }
    } 
}state Disabled{state_entry(){}}