// Geezmo Design Manager - Transparency Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
    
key Creator = "730d4a21-c72b-4a3a-b1d5-099868bf31ec";
string PrimsTag = "GZM.DM";

integer Channel;
integer Listen;

string ModuleName;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 203;
integer GZM_REQUEST = 320;

list PrimsToUpdate = [1];
list PrimsToUpdateNames;

integer Start;

string PrimToChange;

AlphasMenu(key _Id, string Prims)
{
    Dialog( _Id, "Please choose the transparency level for the primset: " + Prims, ["Visible", "10", "20", "30", "40", "HalfVisible", "60","70","80","90", "Transparent","Back <<"], Channel );
}

PrepareAlpha( string Alpha )
{
    integer i = 1;
    integer x;            
    for(;i<llGetListLength( PrimsToUpdate );i+=2)
        if ( ( PrimToChange == "All" )
            || PrimToChange != "All" && llList2String( PrimsToUpdate, i ) == PrimToChange )
            SetAlpha( llList2Integer( PrimsToUpdate, i+1 ), Alpha );
    
    PrimToChange = "";
}

SetAlpha( integer Side, string Alpha )
{
    if ( Side != -1 )
    {
        if ( Alpha == "Transparent" )
            llSetLinkAlpha( Side, 0, ALL_SIDES );
        else if ( Alpha == "HalfVisible" )
            llSetLinkAlpha( Side, .5, ALL_SIDES );
        else if ( Alpha == "Visible" )
            llSetLinkAlpha( Side, 1, ALL_SIDES );
        else
            llSetLinkAlpha( Side, (float)("0." + (string)(100-(integer)Alpha)), ALL_SIDES );
    }
}

// -------------------------------------------------------------------

Menu(key _Id)
{
    PrimToChange = "";
    list Primsets = List2Buttons( GetButtons(PrimsToUpdateNames, Start) );
    integer Length = llGetListLength(Primsets);
    string text;
    if ( Length > 2 ) text = "Please choose a primset below:";
    else {
        text = "No one primset found.";
        Primsets = ["Ok", "Back <<"];
    }
    
    Dialog( _Id, text, List2Buttons(Primsets), Channel );

    if ( ! Length ) 
        llInstantMessage( _Id, "You have to add the prefix \""+PrimsTag+"\" on at least one object's prim name, like: "+PrimsTag+"object primset1 blah blah" );
}

list UpdateObjectData( )
{
    PrimsToUpdateNames = [];
    integer NPrims = llGetObjectPrimCount(llGetKey());
    
    list _List;
    string Name;
    integer _i;
    
    if ( NPrims ) {
        PrimsToUpdateNames += "All";
        _List += ["All", -2];
    
        for(;_i<=NPrims;++_i)
        {
            Name = llGetLinkName(_i);
            if ( llStringTrim( llList2String( llParseString2List( Name, [" "],[] ), 0), STRING_TRIM ) == PrimsTag )
            {
                _List += [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) ), _i];
                if ( !~llListFindList( PrimsToUpdateNames, [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) )] ) )
                    PrimsToUpdateNames += llStringTrim( llGetSubString( Name, llStringLength(PrimsTag), llStringLength( Name ) ), STRING_TRIM );
            }
        }
    } else PrimsToUpdateNames = [1];

    return _List;
}

list GetButtons(list _List, integer _Start)
{
    integer i = _Start;
    list Return;
    integer Length = llGetListLength(_List);

    if ( Length )
    for (;i<=(_Start+9);++i)
    {
        if ( i < Length )
            Return += llList2String(_List, i);
            
        if ( ( (_Start+8 ) == i && _Start + 9 < Length ) || i == Length )
            Return += "Back <<";

        else if ( (_Start+9) == i && (_Start+10) < Length )
            Return += ">> Next";

        if ( i >= Length ) i = (_Start+11);
    }
    
    return Return;
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += String2Button( llList2String( List, i ) );
        
    return Ret;
}

string String2Button( string String )
{
    return (llStringTrim( llGetSubString( String, 0, 23 ), STRING_TRIM ));
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Design Manager :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

Init()
{
    Start = 0;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
    PrimsToUpdate = [1] + UpdateObjectData();
}

default
{
    on_rez( integer _p )
    {
        Init();

        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        //if (Creator != llGetCreator()) state Disabled;
        Init();
        
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_LINK )
            PrimsToUpdate = [1] + UpdateObjectData();
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "");
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
        
        if ( _n == GZM_REQUEST )
        {
            PrimToChange = (string)_k;
            PrepareAlpha( _m );
        }
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            if ( PrimToChange != "" )
            {
                Start += 20;
                AlphasMenu( _i, _m );
            } else {
                Start += 10;
                Menu(_i);
            }
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) llMessageLinked( LINK_THIS, 210, "Core", _i );
            else {
                if ( PrimToChange != "" )
                {
                    Start -= 20;
                    AlphasMenu( _i, _m );
                } else {
                    Start -= 10;
                    Menu(_i);
                }
            }
        } else {
            if ( PrimToChange == "" )
            {
                integer index = llListFindList( PrimsToUpdateNames, [_m] );
                if ( ~index )
                {
                    Start = 0;
                    PrimToChange = _m;
                    AlphasMenu( _i, llList2String( PrimsToUpdateNames, index ) );
                    return;
                }
            } else {
                Start = 0;
                PrepareAlpha( _m );
                Menu(_i);
            }
        }
    }
}state Disabled{state_entry(){}}