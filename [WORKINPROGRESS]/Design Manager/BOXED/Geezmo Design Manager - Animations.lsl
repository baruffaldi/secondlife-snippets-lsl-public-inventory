// Geezmo Design Manager - Animations Script v1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

// TODO ANIMAZIONE|PRIMSET|OFFSET|ROTOFFSET
    
key Creator = "730d4a21-c72b-4a3a-b1d5-099868bf31ec";
string PrimsTag = "GZM.DM";

integer Channel;
integer Listen;

string ModuleName;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;
integer GZM_MENUREQUEST = 206;
integer GZM_REQUEST = 310;

list AvailableAnimations;

integer Start;
integer Checked;

integer GotAvatar;
integer CurrentAnimation = 0;
vector SitOffset = <0, 0, 1.50>;

Menu(key _Id)
{
    list Animations = List2Buttons( GetButtons(AvailableAnimations, Start) );
    integer Length = llGetListLength(Animations);
    
    if ( ! GotAvatar )
        Length = 0;
    
    string text;
    if ( Length ) text = "Please choose a animation:";
    else Animations = ["Ok", "Back <<"];
    

    if ( ! GotAvatar )
        text = "No one avatar sitted or attached found.";
    else if ( ! Length ) text = "No one animation found.";

    Dialog( _Id, text, Animations, Channel );
    
    if ( ! Length && llAvatarOnSitTarget() != NULL_KEY ) 
        llInstantMessage( _Id, "You have to add some animations to the object's content in order to change the animations." );
}

SetAnimation( string Animation )
{
    Stop();
    CurrentAnimation = llListFindList(AvailableAnimations, [Animation]);
    Play();
}

// -------------------------------------------------------------------

list UpdateInventoryData( integer _Type )
{
    integer _i;
    integer _Length = llGetInventoryNumber(_Type);
    
    list _List;
    for(_i=0;_i<_Length;++_i)
        if ( llGetSubString( llGetInventoryName(_Type, _i), 0, 7 ) != "SCULPMAP" )
            _List += [llGetInventoryName(_Type, _i)];

    return _List;
}

UpdateObjectData( )
{    
    key av = llAvatarOnSitTarget(); 
    if(av != NULL_KEY)
    {
        GotAvatar = TRUE;
        llRequestPermissions(av, PERMISSION_TRIGGER_ANIMATION);
    }
    else 
    {
        if((llGetPermissions() & PERMISSION_TRIGGER_ANIMATION) && GotAvatar) 
        {
            GotAvatar = FALSE;
            Stop();
        }
    }
}

list GetButtons(list _List, integer _Start)
{
    integer i = _Start;
    list Return;
    integer Length = llGetListLength(_List);
    if ( Length )
    for (;i<=(_Start+9);++i)
    {
        if ( i < Length )
            Return += llList2String(_List, i);
            
        if ( ( (_Start+8 ) == i && _Start + 9 < Length ) || i == Length )
            Return += "Back <<";

        else if ( (_Start+9) == i && (_Start+10) < Length )
            Return += ">> Next";

        if ( i >= Length ) i = (_Start+11);
    }
    
    return Return;
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += String2Button( llList2String( List, i ) );
        
    return Ret;
}

string String2Button( string String )
{
    return (llStringTrim( llGetSubString( String, 0, 23 ), STRING_TRIM ));
}

Report( string _fliptitle )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, <1,1,1>, 1.0 );
}

Dialog(key _Id, string _Text, list _Buttons, integer _Channel)
{
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    llDialog( _Id, "\n :: Geezmo * Design Manager :: "+ModuleName+"\n"+_Text, _Buttons, _Channel );
}

Stop()
{
    if ( llList2String(AvailableAnimations, CurrentAnimation) != "" )
        llStopAnimation(llList2String(AvailableAnimations, CurrentAnimation));
}

Play()
{
    if ( llList2String(AvailableAnimations, CurrentAnimation) != "" )
    {
        llStopAnimation("sit_generic");
        llStopAnimation("sit");
        llStartAnimation(llList2String(AvailableAnimations, CurrentAnimation));
    }
}

string Button2String( string ButtonText, list Stack )
{
    integer i;
    integer Length = llGetListLength( Stack );
    for( ;i<Length;++i)
        if ( llStringTrim( llGetSubString( llList2String( Stack, i ), 0, 23 ), STRING_TRIM ) == ButtonText )
            return llList2String( Stack, i );
    
    return "";
}

integer IsUuid( string Id )
{
    list tmp = llParseString2List( Id, ["-"], [] );
    if ( llGetListLength( tmp ) == 5 )
    {
        if ( llStringLength( llList2String( tmp, 0 ) ) == 8
          && llStringLength( llList2String( tmp, 1 ) ) == 4
          && llStringLength( llList2String( tmp, 2 ) ) == 4
          && llStringLength( llList2String( tmp, 3 ) ) == 4
          && llStringLength( llList2String( tmp, 4 ) ) == 12 )
            return TRUE;
    }

    return FALSE;
}

Init()
{
    CurrentAnimation = 0;
    Start = 0;
    Checked = TRUE;
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );
}

default
{
    on_rez( integer _p )
    {
        Init();
        llListenRemove(Listen);
        Listen = llListen(Channel, "", "", "");
    }
    
    state_entry()
    {
        //if (Creator != llGetCreator()) state Disabled;
        if ( ! Checked )
            state Update;

        Listen = llListen(Channel, "", "", "");
        
        llSitTarget(SitOffset,ZERO_ROTATION); 
        
        key av = llAvatarOnSitTarget();
        if ( av != NULL_KEY )
        {
            llUnSit(av);
            llPushObject(av, <0,0,10>, ZERO_VECTOR, FALSE);
        }
    }
     
    run_time_permissions(integer perm) 
    {
        if(perm & PERMISSION_TRIGGER_ANIMATION) 
        {
            Play();
        }
    }
    
    attach( key _ao )
    {
        if ( _ao != NULL_KEY )
        {
            GotAvatar = TRUE;
            llRequestPermissions(_ao, PERMISSION_TRIGGER_ANIMATION);
        } else GotAvatar = FALSE;
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_LINK )
            UpdateObjectData();
            
        if ( _c & CHANGED_INVENTORY )
            state Update;
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "");
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
        
        if ( _n == GZM_REQUEST )
            SetAnimation( _m );
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            Start += 10;
            Menu( _i );
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) llMessageLinked( LINK_THIS, 210, "Core", _i );
            else {
                Start -= 10;
                Menu( _i );
            }
        } else {
            
            string CompleteString = Button2String( _m, AvailableAnimations );
            if ( CompleteString != "" )
            {
                Start = 0;
                SetAnimation( CompleteString );
                Menu( _i );
            }
        }
    }
}

state Update
{
    state_entry()
    {
        Init();
        UpdateObjectData();
        AvailableAnimations = UpdateInventoryData( INVENTORY_ANIMATION );

        state default;
    }
}state Disabled{state_entry(){}}