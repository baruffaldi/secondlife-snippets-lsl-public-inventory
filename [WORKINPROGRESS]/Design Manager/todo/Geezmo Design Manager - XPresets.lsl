
key Creator = "730d4a21-c72b-4a3a-b1d5-099868bf31ec";

integer Start;
integer Channel;
integer Listen;

integer GZM_SETCHANNEL = 100;
integer GZM_SETOWNER = 101;

// --------------------------------------------------------

string ModuleName;
integer GZM_MENUREQUEST = 200;

// --------------------------------------------------------

list PresetsFullList;

 string notecardName;
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

// --------------------------------------------------------

list KnownNotecards = ["Textures", "Colors", "SculptMaps", "Sizes"];

Report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
}

Menu(key _Id)
{
    integer i;
    list Presets;
    integer Length = llGetListLength(PresetsFullList);
    integer SetBack;
    integer SetNext;

    if ( Length ) 
    for (;i<=Length;++i)
    {
            if ( ! SetBack && ( (Start+9) == i || i == Length ) )
            {
                --i;
                Presets += "Back <<";
                SetBack = TRUE;
            } else if ( ( ( (Start+10) < Length && (Start+10) == i && Start > 0 ) || ( Start == 0 && i == 11 ) ) && ! SetNext ) {
                --i;
                Presets += ">> Next";
                SetNext = TRUE;
            } else if ( i >= Start && i < (Start+10) && i < Length ) {
                if ( Start > 0 )
                    Presets += llStringTrim( llGetSubString( llList2String(PresetsFullList, i), 0, 23 ), STRING_TRIM );
                else Presets += llStringTrim( llGetSubString( llList2String(PresetsFullList, i), 0, 23 ), STRING_TRIM ); 
            }
        }
        
    if ( _Id == NULL_KEY ) _Id = llGetOwner();
    string text;
    if ( Length ) text = "Please choose a notecard below:";
    else {
        text = "No one preset notecard found.";
        Presets = ["Ok", "Back <<"];
    }
    
    llDialog( _Id, "\nGeezmo Texture&Color Switcher\n"+text, Presets, Channel );
    
    if ( ! Length ) 
        llInstantMessage( _Id, "You have to create a preset notecard in order to change the all prims styles." );
}

list UpdateInventoryData( integer _Type )
{
    ModuleName = llStringTrim( llList2String( llParseString2List( llGetScriptName(), ["-"], [] ), 1 ), STRING_TRIM );

    integer _i;
    integer _Length = llGetInventoryNumber(_Type);
    
    list _List;
    string Name;
    for(_i=0;_i<_Length;++_i)
    {
        Name = llGetInventoryName(_Type, _i);
        if ( llListFindList( KnownNotecards, [Name] ) == -1  )
            _List += Name;
    }

    return _List;
}

Init()
{
    PresetsFullList = UpdateInventoryData( INVENTORY_NOTECARD );
    Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
    Listen  = llListen(Channel, "", "", "");
}

default
{
    on_rez( integer _p )
    {
        Init();
    }
    
    state_entry()
    {
        if (Creator != llGetCreator()) state Disabled;
        Init();
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_INVENTORY )
            PresetsFullList = UpdateInventoryData( INVENTORY_NOTECARD );
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _n == GZM_SETCHANNEL )
        {
            Channel = (integer)_m;
            llListenRemove(Listen);
            Listen = llListen( Channel, "", _k, "" );
            return;
        }
        
        if ( _n == GZM_SETOWNER )
        {
            llListenRemove(Listen);
            Listen = llListen(Channel, "", _k, "");
            return;
        }
        
        if ( _n == GZM_MENUREQUEST || _m == ModuleName )
        {
            Start = 0;
            Menu(_k);
            return;
        }
    }
    
    state_exit()
    {
        llListenRemove(Listen);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            Start += 10;
            Menu(_i);
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) llMessageLinked( LINK_THIS, 210, "", _i );
            else {
                Start -= 10;
                Menu(_i);
            }
        } else if ( _m != "Ok" ) {
            notecardName = _m;
            state UpdateStyle;
        }
    }
}

state UpdateStyle
{
    state_entry()
    {
        integer i;
        for(;i<llGetListLength(PresetsFullList);++i)
            if ( llStringTrim( llGetSubString( llList2String(PresetsFullList, i), 0, 23 ), STRING_TRIM ) == notecardName )
                notecardName = llList2String(PresetsFullList, i);
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                Report(
                    "Notecard! Leeching notecard in progress...",
                    "Notecard reading... 0%",
                    <1.0,1.0,1.0>, FALSE, FALSE
                );

                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
            }
        else state default;
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }
        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                list temp;
                string name;
                string value;

                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    integer percent;
                    percent = ( notecardLine * 100 ) / notecardTotalLines;

                    Report(
                        "",
                        "Notecard reading... " + (string) percent + "%",
                        <1.0,1.0,1.0>, FALSE, FALSE
                    );
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else {
            Report(
                "Notecard reading completed!",
                "Notecard reading completed!",
                <1.0,1.0,1.0>, TRUE, TRUE
            );
            llSleep(1.0);
            llSetText( "", ZERO_VECTOR, 0 );
            state default;
        }
    } 
}state Disabled{state_entry(){}}