
Log( string Text, integer Type, key Id )
{
    string Name = llGetObjectName();
    llSetObjectName("");
    
    if ( Type == 0 )
        llOwnerSay( "/me \n" + Text );
    else if ( Type == 1 )
        llInstantMessage( Id, "/me \n" + Text );
    else if ( Type == 2 )
        llWhisper( 0, "/me \n" + Text );
    else if ( Type == 3 )
        llSay( 0, "/me \n" + Text );
    else if ( Type == 4 )
        llShout( 0, "/me \n" + Text );
    else if ( Type == 5 )
        llRegionSay( 0, "/me \n" + Text );
    
    llSetObjectName(Name);
}

list PrimsToUpdate = [1];
list PrimsToUpdateNames = [1];
string PrimsTag = "GZM.DM";
list UpdateObjectData( )
{
    PrimsToUpdateNames = [];
    integer NPrims = llGetObjectPrimCount(llGetKey());
    
    list _List;
    string Name;
    integer _i;
    
    if ( NPrims ) {
        PrimsToUpdateNames += "All";
        _List += ["All", -2];
    
        for(;_i<=NPrims;++_i)
        {
            Name = llGetLinkName(_i);
            if ( llStringTrim( llList2String( llParseString2List( Name, [" "],[] ), 0), STRING_TRIM ) == PrimsTag )
            {
                _List += [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) ), _i];
                if ( !~llListFindList( PrimsToUpdateNames, [llGetSubString( Name, llStringLength(PrimsTag)+1, llStringLength( Name ) )] ) )
                    PrimsToUpdateNames += llStringTrim( llGetSubString( Name, llStringLength(PrimsTag), llStringLength( Name ) ), STRING_TRIM );
            }
        }
    } else PrimsToUpdateNames = [1];

    return _List;
}

default
{
    touch_start(integer total_number)
    {
        PrimsToUpdate += UpdateObjectData( );
        integer i = 1;
        for(;i<llGetListLength(PrimsToUpdateNames);i+=2)
            if ( llList2String( PrimsToUpdateNames, i ) != "All" )
                Log( "[*] Geezmo Design Manager for the object: " + llGetObjectName() +
                "\n[*]\n[*][*] Available Primsets Detected: " +
                "\n[+] Primset: " +
                llList2String( PrimsToUpdateNames, i ) +
                "\n[-] Changes Permissions: " 
                + llList2String( PrimsToUpdateNames, i ) +
                "\n[*]\n[*] Available Primsets Detected: " +
                llList2String( PrimsToUpdateNames, i ) +
                ", " + llList2String( PrimsToUpdateNames, i ) +
                ", " + llList2String( PrimsToUpdateNames, i )
                
                , 0, llDetectedKey(0) );
    }
}
