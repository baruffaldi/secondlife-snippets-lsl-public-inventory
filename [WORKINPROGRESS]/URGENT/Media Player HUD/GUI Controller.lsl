// Geezmo LinkedMessagesSpy FreeBie Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
    
default
{
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        string text = "Sender: " + (string) _sn;
        text += "\nNumber: " + (string) _n;
        text += "\nMessage: " + _m;
        text += "\nKey: " + (string) _k;
        
        llOwnerSay( text );
    }
}
