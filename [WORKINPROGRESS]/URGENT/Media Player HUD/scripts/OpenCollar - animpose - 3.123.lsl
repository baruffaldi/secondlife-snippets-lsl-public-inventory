//Licensed under the GPLv2, with the additional requirement that these scripts remain "full perms" in Second Life.  See "OpenCollar License" for details.

//needs to handle anim requests from sister scripts as well
//this script as essentially two layers
//lower layer: coordinate animation requests that come in on link messages.  keep a list of playing anims disable AO when needed
//upper layer: use the link message anim api to provide a pose menu

list anims;
integer num_anims;//the number of anims that don't start with "~"
integer pagesize = 9;//number of anims we can fit on one page of a multi-page menu

//for the height scaling feature
key dataid;
string card = "~heightscalars";
integer line = 0;
list anim_scalars;//a 3-strided list in form animname,scalar,delay
integer adjustment = 0;

string currentpose = "";
integer lastrank = 0; //in this integer, save the rank of the person who posed the av, according to message map.  0 means unposed
string rootmenu = "Main";
string animmenu = "Animations";
string posemenu = "Pose";
string aomenu = "AO";
list animbuttons = ["AO", "Pose"];
string giveao = "Give AO";
string triggerao = "AO Menu";

//MESSAGE MAP
integer COMMAND_NOAUTH = 0;
integer COMMAND_OWNER = 500;
integer COMMAND_SECOWNER = 501;
integer COMMAND_GROUP = 502;
integer COMMAND_WEARER = 503;
integer COMMAND_EVERYONE = 504;
integer CHAT = 505;
integer COMMAND_SAFEWORD = 510;  // new for safeword

//integer SEND_IM = 1000; deprecated.  each script should send its own IMs now.  This is to reduce even the tiny bt of lag caused by having IM slave scripts
integer POPUP_HELP = 1001;

integer HTTPDB_SAVE = 2000;//scripts send messages on this channel to have settings saved to httpdb
                            //str must be in form of "token=value"
integer HTTPDB_REQUEST = 2001;//when startup, scripts send requests for settings on this channel
integer HTTPDB_RESPONSE = 2002;//the httpdb script will send responses on this channel
integer HTTPDB_DELETE = 2003;//delete token from DB
integer HTTPDB_EMPTY = 2004;//sent by httpdb script when a token has no value in the db

integer MENUNAME_REQUEST = 3000;
integer MENUNAME_RESPONSE = 3001;
integer SUBMENU = 3002;
integer MENUNAME_REMOVE = 3003;

integer RLV_CMD = 6000;

integer ANIM_START = 7000;
integer ANIM_STOP = 7001;

//5000 block is reserved for IM slaves

//string UPMENU = "↑";
//string MORE = "→";
string UPMENU = "^";
string MORE = ">";

integer page = 0;
integer animmenuchannel = 2348207;
integer posemenuchannel = 2348208;
integer aomenuchannel = 2348209;
integer timeout = 60;
integer listener;
integer aochannel = -782690;
string AO_ON = "ZHAO_STANDON";
string AO_OFF = "ZHAO_STANDOFF";
string AO_MENU = "ZHAO_MENU";

debug(string str)
{
    //llOwnerSay(llGetScriptName() + ": " + str);
}

AnimMenu(key id)
{
    string prompt = "Choose an option.";
    prompt += "  (This menu will expire in " + (string)timeout + " seconds.)\n";
    list buttons = llListSort(animbuttons, 1, TRUE) + [UPMENU];
    llSetTimerEvent(timeout);
    animmenuchannel = - llRound(llFrand(999999)) - 9999;
    llListenRemove(listener);
    listener = llListen(animmenuchannel, "", id, "");
    buttons = RestackMenu(FillMenu(buttons));
    llDialog(id, prompt, buttons, animmenuchannel);     
}

RefreshAnim()
{ //anims can get lost on TP, so re-play anims[0] here, and call this function in "changed" event on TP
    if (llGetListLength(anims))
    {
        if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION)
        {
            string anim = llList2String(anims, 0);
            if (llGetInventoryType(anim) == INVENTORY_ANIMATION)
            { //get and stop currently playing anim
                if (llGetListLength(anims))
                {
                    string current = llList2String(anims, 0);
                    llStopAnimation(current);
                }
                //add anim to list
                anims = [anim] + anims;//this way, anims[0] is always the currently playing anim
                llStartAnimation(anim);
                llSay(aochannel, AO_OFF);                      
            }
            else
            {
                //Popup(llGetOwner(), "Error: Couldn't find anim: " + anim);            
            }                     
        }
        else
        {
            Popup(llGetOwner(), "Error: Somehow I lost permission to animate you.  Try taking me off and re-attaching me.");
        }        
    }
}

StartAnim(string anim)
{
    if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION)
    {
        if (llGetInventoryType(anim) == INVENTORY_ANIMATION)
        {   //get and stop currently playing anim
            if (llGetListLength(anims))
            {
                string current = llList2String(anims, 0);
                llStopAnimation(current);
            }
            
            //stop any currently playing height adjustment
            if (adjustment)
            {
                llStopAnimation("~" + (string)adjustment);
                adjustment = 0;
            }
            
            //add anim to list
            anims = [anim] + anims;//this way, anims[0] is always the currently playing anim
            llStartAnimation(anim);
            llSay(aochannel, AO_OFF);       
            
            //adjust height for anims in anim_scalars
            integer index = llListFindList(anim_scalars, [anim]);
            if (index != -1)
            {//we just started playing an anim in our adjustment list
                //pause to give certain anims time to ease in
                llSleep((float)llList2String(anim_scalars, index + 2));
                vector avscale = llGetAgentSize(llGetOwner());
                float scalar = (float)llList2String(anim_scalars, index + 1);
                adjustment = llRound(avscale.z * scalar);
                if (adjustment > -30)
                {
                    adjustment = -30;
                }
                else if (adjustment < -50)
                {
                    adjustment = -50;
                }
                llStartAnimation("~" + (string)adjustment);
            }
        }
        else
        {
            //Popup(llGetOwner(), "Error: Couldn't find anim: " + anim);            
        }                    
    }
    else
    {
        Popup(llGetOwner(), "Error: Somehow I lost permission to animate you.  Try taking me off and re-attaching me.");
    }
}

StopAnim(string anim)
{
    if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION)
    {
        if (llGetInventoryType(anim) == INVENTORY_ANIMATION)
        {   //remove all instances of anim from anims
            //loop from top to avoid skipping
            integer n;
            for (n = llGetListLength(anims) - 1; n >= 0; n--)
            {
                if (llList2String(anims, n) == anim)
                {
                    anims = llDeleteSubList(anims, n, n);
                }
            }
            llStopAnimation(anim);  
            
            //stop any currently-playing height adjustment
            if (adjustment)
            {
                llStopAnimation("~" + (string)adjustment);
                adjustment = 0;
            }                
            //play the new anims[0]
            //if anim list is empty, turn AO back on
            if (llGetListLength(anims))
            {
                string newanim = llList2String(anims, 0);
                llStartAnimation(newanim);
                
                //adjust height for anims in anim_scalars
                integer index = llListFindList(anim_scalars, [newanim]);
                if (index != -1)
                {//we just started playing an anim in our adjustment list
                    //pause to give certain anims time to ease in
                    llSleep((float)llList2String(anim_scalars, index + 2));
                    vector avscale = llGetAgentSize(llGetOwner());
                    float scalar = (float)llList2String(anim_scalars, index + 1);
                    adjustment = llRound(avscale.z * scalar);
                    if (adjustment > -30)
                    {
                        adjustment = -30;
                    }
                    else if (adjustment < -50)
                    {
                        adjustment = -50;
                    }
                    llStartAnimation("~" + (string)adjustment);
                }                
            }
            else
            {
                llSay(aochannel, AO_ON);
            }
        }
        else
        {
            //Popup(llGetOwner(), "Error: Couldn't find anim: " + anim);            
        }        
    }
    else
    {
        Popup(llGetOwner(), "Error: Somehow I lost permission to animate you.  Try taking me off and re-attaching me.");
    }
}

Popup(key id, string message)
{ //one-way popup message.  don't listen for these anywhere
    llDialog(id, message, [], 298479);
}

AOMenu(key id)
{
    string prompt = "Choose an option.";
    prompt += "  (This menu will expire in " + (string)timeout + " seconds.)\n";
    list buttons = [triggerao, giveao, UPMENU];
    buttons += ["AO ON", "AO OFF"];
    llSetTimerEvent(timeout);
    aomenuchannel = - llRound(llFrand(999999)) - 9999;
    llListenRemove(listener);
    listener = llListen(aomenuchannel, "", id, "");
    llDialog(id, prompt, buttons, aomenuchannel);    
}

PoseMenu(key id)
{ //create a list
    list buttons = ["*Release*"];
    string prompt = "Choose an anim to play.  (This menu will expire in " + (string)timeout + " seconds.)\n";
    //build a button list with the dances, and "More"
    //get number of anims
    integer n;
    if (num_anims <= pagesize + 1)
    {  //if pagesize + 1 or less, just put them all in the list
        for (n=0;n<num_anims;n++)
        {
            string name = llGetInventoryName(INVENTORY_ANIMATION,n);
            if (name != "" && llGetSubString(name, 0, 0) != "~")
            {
                prompt += "\n" + (string)(n + 1) + " - " + name;
                buttons += [(string)(n + 1)];
            }
        }  
    }
    else
    {  //there are more than 12 dances, use page number in adding buttons
        for (n=0;n<pagesize;n++)
        {   //check for anim existence, add to list if it exists
            string name = llGetInventoryName(INVENTORY_ANIMATION, n + (page * pagesize));
            if (name != ""  && llGetSubString(name, 0, 0) != "~")
            {
                prompt += "\n" + (string)(n + (page * pagesize) + 1) + " - " + name;
                buttons += [(string)(n + (page * pagesize) + 1)];                
            }
        }
        //add the More button
        buttons = buttons + [MORE];
    }
    buttons += [UPMENU];
    buttons = RestackMenu(FillMenu(buttons));    
    posemenuchannel = - llRound(llFrand(999999)) - 9999;
    llListenRemove(listener);
    listener = llListen(posemenuchannel, "", id, "");
    llDialog(id, prompt, buttons, posemenuchannel);
    //the menu needs to time out
    llSetTimerEvent((float)timeout);
}

list FillMenu(list in)
{ //adds empty buttons until the list length is multiple of 3, to max of 12
    while (llGetListLength(in) != 3 && llGetListLength(in) != 6 && llGetListLength(in) != 9 && llGetListLength(in) < 12)
    {
        in += [" "];
    }
    return in;
}

list RestackMenu(list in)
{ //re-orders a list so dialog buttons start in the top row
    list out = llList2List(in, 9, 11);
    out += llList2List(in, 6, 8);
    out += llList2List(in, 3, 5);    
    out += llList2List(in, 0, 2);    
    return out;
}

DeliverAO(key id)
{
    string name = "OpenCollar Sub AO";
    string version = "0.0";
    
    string url = "http://collardata.appspot.com/updater/check?";
    url += "object=" + llEscapeURL(name);
    url += "&version=" + llEscapeURL(version);
    llHTTPRequest(url, [HTTP_METHOD, "GET",HTTP_MIMETYPE,"text/plain;charset=utf-8"], "");     
    llInstantMessage(id, "Queuing delivery of " + name + ".  It should be delivered in about 30 seconds.");
}

integer startswith(string haystack, string needle) // http://wiki.secondlife.com/wiki/llSubStringIndex
{
    return llDeleteSubString(haystack, llStringLength(needle), -1) == needle;
}

RequestPerms()
{
    if (llGetAttached())
    {
        llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
    }
}

default
{
    on_rez(integer num)
    {
        if (currentpose != "")
        {
            llMessageLinked(LINK_THIS, ANIM_START, currentpose, NULL_KEY);
        }
        RequestPerms();        
    }
    state_entry()
    {
        RequestPerms();
        
        num_anims = llGetInventoryNumber(INVENTORY_ANIMATION);
        while (llGetSubString(llGetInventoryName(INVENTORY_ANIMATION, num_anims - 1), 0, 0) == "~")
        {
            num_anims--;
        }
        
        llMessageLinked(LINK_THIS, MENUNAME_REQUEST, animmenu, NULL_KEY);
        llMessageLinked(LINK_THIS, MENUNAME_RESPONSE, rootmenu + "|" + animmenu, NULL_KEY);
        
        //start reading the ~heightscalars notecard
        dataid = llGetNotecardLine(card, line);
    }
    
    dataserver(key id, string data)
    {
        if (id == dataid)
        {
            if (data != EOF)
            {
                anim_scalars += llParseString2List(data, ["|"], []);
                line++;
                dataid = llGetNotecardLine(card, line);
            }
        }
    }
    
    changed(integer change)
    {
        if (change & CHANGED_OWNER)
        {
            llResetScript();
        }
        
        if (change & CHANGED_TELEPORT)
        {
            RefreshAnim();
        }
        
        if (change & CHANGED_INVENTORY)
        {
            num_anims = llGetInventoryNumber(INVENTORY_ANIMATION);
            while (llGetSubString(llGetInventoryName(INVENTORY_ANIMATION, num_anims - 1), 0, 0) == "~")
            {
                num_anims--;
            }       
        }
    }
    
    attach(key id)
    {
        if (id == NULL_KEY)
        {
            debug("detached");
            //we were just detached.  clear the anim list and tell the ao to play stands again.
            llSay(aochannel, AO_ON);
            anims = [];
        }
    }
    
    link_message(integer sender, integer auth, string str, key id)
    {  //ignore "hug" and "kiss"
        if (str == "hug" || str == "kiss")
        {
            return;
        }
        //only respond to owner, secowner, group, wearer
        if (auth >= COMMAND_OWNER && auth <= COMMAND_WEARER)
        {
            list params = llParseString2List(str, [" "], []);
            string command = llToLower(llList2String(params, 0));
            string value = llToLower(llList2String(params, 1));
            if (str == "release")
            { //only release if person giving command outranks person who posed us
                if (auth <= lastrank)
                {
                    lastrank = auth;
                    llMessageLinked(LINK_THIS, ANIM_STOP, currentpose, NULL_KEY);                    
                    currentpose = "";
                }  
            }            
            else if (str == "settings")
            {
                if (currentpose != "")
                {
                    llInstantMessage(id, "Current Pose: " + currentpose);
                }
            }
            else if ((str == "runaway" || str == "reset") && (auth == COMMAND_OWNER || auth == COMMAND_WEARER))            
            {   //stop pose
                if (currentpose != "")
                {
                    StopAnim(currentpose);
                }
                //reset script
                llResetScript();
            }
            else if (str == "pose")
            {  //do multi page menu listing anims
                PoseMenu(id);
            }
//            else if (str == "triggerao")
//            {
//                llSay(aochannel, AO_MENU + "|" + (string)id);
//                llInstantMessage(id, "Attempting to trigger the AO menu.  This will only work if " + llKey2Name(llGetOwner()) + " is wearing the OpenCollar Sub AO.");
//            }
            else if(command == "ao")
            {
                if(value == "off")
                {
                    llSay(aochannel, "ZHAO_AOOFF");
                }
                else if(value == "on")
                {
                    llSay(aochannel, "ZHAO_AOON");
                }
                else if(value == "menu")
                {
                    llSay(aochannel, AO_MENU + "|" + (string)id);
                }
                else if(value == "hide")
                {
                    llSay(aochannel, "ZHAO_AOHIDE");
                }
                else if(value == "show")
                {
                    llSay(aochannel, "ZHAO_AOSHOW");
                }
            }
            else if (llGetInventoryType(str) == INVENTORY_ANIMATION)
            {
                if (currentpose == "")
                {
                    currentpose = str;
                    //not currently in a pose.  play one
                    lastrank = auth;
                    //StartAnim(str);                
                    llMessageLinked(LINK_THIS, ANIM_START, currentpose, NULL_KEY);
                }            
                else
                {  //only change if command rank is same or higher (lower integer) than that of person who posed us
                    if (auth <= lastrank)
                    {
                        lastrank = auth;
                        llMessageLinked(LINK_THIS, ANIM_STOP, currentpose, NULL_KEY);
                        currentpose = str;                        
                        llMessageLinked(LINK_THIS, ANIM_START, currentpose, NULL_KEY);
                    }
                }
            }            
        }
        else if (auth == ANIM_START)
        {
            StartAnim(str);
        }
        else if (auth == ANIM_STOP)
        {
            StopAnim(str);            
        }
        else if (auth == MENUNAME_REQUEST && str == rootmenu)
        {
            llMessageLinked(LINK_THIS, MENUNAME_RESPONSE, rootmenu + "|" + animmenu, NULL_KEY);          
        }
        else if (auth == MENUNAME_RESPONSE)
        {
            if (startswith(str, animmenu + "|"))
            {
                string child = llList2String(llParseString2List(str, ["|"], []), 1);
                if (llListFindList(animbuttons, [child]) == -1)
                {
                    animbuttons += [child];
                }
            }
        }
        else if (auth == SUBMENU && str == posemenu)
        {//we don't know the authority of the menu requester, so send a message through the auth system
            llMessageLinked(LINK_THIS, COMMAND_NOAUTH, "pose", id);
        }        
        else if (auth == SUBMENU && str == aomenu)
        {   //give menu
            AOMenu(id);
        }
        else if (auth == SUBMENU && str == animmenu)
        {   //give menu
            AnimMenu(id);
        }        
        else if (auth == COMMAND_SAFEWORD)
        { // saefword command recieved, release animation
            if(llGetInventoryType(currentpose) == INVENTORY_ANIMATION)
            {
                llMessageLinked(LINK_THIS, ANIM_STOP, currentpose, NULL_KEY);                    
                currentpose = "";
            }
        } 
    }
    
    listen(integer channel, string name, key id, string message)
    {
        llSetTimerEvent(0);
        llListenRemove(listener);        
        if (channel == animmenuchannel)
        {
            if (message == UPMENU)
            {
                llMessageLinked(LINK_THIS, SUBMENU, rootmenu, id);
            }
            else if (message == "Pose")
            {
                PoseMenu(id);
            }
            else if (message == "AO")
            {
                AOMenu(id);
            }
            else if (~llListFindList(animbuttons, [message]))
            {
                llMessageLinked(LINK_THIS, SUBMENU, message, id);
            }
        }
        else if (channel == posemenuchannel)
        {
            if (message == MORE)
            { //increment page number
                if (num_anims > (pagesize * (page + 1)))
                {  //there are more pages
                    page++;
                }
                else
                {
                    page = 0;
                }                    
            }
            else if (message == "*Release*")
            {
                llMessageLinked(LINK_THIS, COMMAND_NOAUTH, "release", id);            
            }
            else if ((integer)message)
            { //we don't know any more what the speaker's auth is, so pass the command back through the auth system.  then it will play only if authed
                string animname = llGetInventoryName(INVENTORY_ANIMATION, (integer)message - 1);
                llMessageLinked(LINK_THIS, COMMAND_NOAUTH, animname, id);
            }
            else if (message == UPMENU)
            { //return on parent menu, so the animmenu below doesn't come up
                llMessageLinked(LINK_THIS, SUBMENU, animmenu, id);
                return;
            }
            PoseMenu(id);             
        }         
        else if (channel == aomenuchannel)
        {
            if (message == triggerao)
            {
                llSay(aochannel, AO_MENU + "|" + (string)id);
                llInstantMessage(id, "Attempting to trigger the AO menu.  This will only work if " + llKey2Name(llGetOwner()) + " is wearing the OpenCollar Sub AO.");
//                llMessageLinked(LINK_THIS, COMMAND_NOAUTH, "triggerao", id);                
            }
            else if (message == giveao)
            {    //queue a delivery
                DeliverAO(id);
                AOMenu(id);
            }
            else if (message == UPMENU)
            {
                llMessageLinked(LINK_THIS, SUBMENU, animmenu, id);                
            }
            else if(message == "AO ON")
            {
                llSay(aochannel, "ZHAO_AOON");
                AOMenu(id);
            }
            else if(message == "AO OFF")
            {
                llSay(aochannel, "ZHAO_AOOFF");
                AOMenu(id);
            }
        }
    }
    
    timer()
    {
        llSetTimerEvent(0);
        llListenRemove(listener);
    }
}