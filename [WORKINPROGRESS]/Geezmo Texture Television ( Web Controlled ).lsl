// Geezmo TextController FreeBie Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
// todo: autostart
// todo: notecard Playlists
// todo: distinzione textures normali dalle animate sia su notecard che su web

integer AutoWebUpdate = FALSE; // DEPRECATED: too many web requests
integer AutoStart = TRUE;

key GetPlaylists;
key GetPlaylist;
key GetPlaylistMovie;

integer Channel;
integer Listen;

integer Start;

key CurrentTexture;
string CurrentTextureName;
integer CurrentTextureWidth;
integer CurrentTextureHeight;
float CurrentTextureDuration;
float CurrentTextureFramesPerSecond;

list Playlists;
list PlaylistMovies;

// ------------------------------------------------

string Playlist = "Default";

Menu( key Target )
{
    llDialog( Target, "#################\nCurrent playlist: " + Playlist + "\n#################\n\nChoose an option below:", ["Play", "Stop", "Playlists"], Channel );
}   

PlaylistsMenu( key Target )
{
    llDialog( Target, "#################\nCurrent playlist: " + Playlist + "\n#################\n\nChoose a playlist below:", List2Buttons( GetButtons(Playlists, Start) ), Channel );
}

Play()
{
    if ( AutoWebUpdate )
        GetPlaylistMovie = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylistMovie?playlist="+llEscapeURL(Playlist )+"&current="+llEscapeURL(CurrentTexture), [], "" );
    else {
        list Temp = GetNext();
        CurrentTextureName = llList2String( Temp, 0 );
        CurrentTexture = llList2Key( Temp, 1 );
        CurrentTextureWidth = llList2Integer( Temp, 2 );
        CurrentTextureHeight = llList2Integer( Temp, 3 );
        CurrentTextureDuration = llList2Float( Temp, 4 );
        CurrentTextureFramesPerSecond = llList2Float( Temp, 5 );
        llWhisper( 0, "Now playing: \"" + CurrentTextureName + "\" of playlist: \"" + Playlist + "\"" );
        llSetTexture( CurrentTexture, ALL_SIDES );
        llSetTextureAnim( ( 0 | ANIM_ON | LOOP ), ALL_SIDES,  CurrentTextureWidth, CurrentTextureHeight, 0.0, 0, CurrentTextureFramesPerSecond ); 
        llSetTimerEvent( CurrentTextureDuration );
    }
}

Stop()
{
    llSetObjectName( "Geezmo Texture Television" );
    llSetObjectDesc( "Scripted for iViD AMENiC" );
    llSetTexture( "1b38a35d-317d-ce17-8250-af0dfe16cc57", ALL_SIDES );
    llSetTextureAnim(   0 , ALL_SIDES,  0,0,   0.0, 0,   0.0  );  
    llSetTimerEvent( 0 );
}

list GetNext()
{
    integer Length = llList2Integer( PlaylistMovies, 0 );
    integer NewIndex = llListFindList( PlaylistMovies, [CurrentTextureName] );
    
    if ( NewIndex == -1 ) NewIndex = 0;
    else NewIndex += Length;
    
    if ( llList2List( PlaylistMovies, ( NewIndex + 1 ), ( NewIndex + Length + 1 ) ) == [] )
        NewIndex = 0;
    
    return llList2List( PlaylistMovies, ( NewIndex + 1 ), ( NewIndex + Length + 1 ) );
}

list List2Buttons( list List )
{
    list Ret;
    
    integer i;
    integer Length = llGetListLength( List );
    
    for( ;i<Length;++i)
        Ret += llGetSubString( llStringTrim( llList2String( List, i ), STRING_TRIM ), 0, 23 );
        
    return Ret;
}

list GetButtons(list _List, integer _Start)
{
    integer i = _Start;
    list Return;
    integer Length = llGetListLength(_List);

    if ( Length )
    for (;i<=(_Start+9);++i)
    {
        if ( i < Length )
            Return += llList2String(_List, i);
            
        if ( ( (_Start+8 ) == i && _Start + 9 < Length ) || i == Length )
            Return += "Back <<";

        else if ( (_Start+9) == i && (_Start+10) < Length )
            Return += ">> Next";

        if ( i >= Length ) i = (_Start+11);
    }

    return Return;
}

integer dictCount(list dict)
{
    return (llGetListLength(dict)-1)/(llList2Integer(dict,0)+1);
}

default
{
    on_rez( integer _c )
    {
        llResetScript();
    }
    
    state_entry()
    {
        Channel = (integer) ( llFrand( -1000000000.0 ) - 1000000000.0 );
        Listen = llListen(Channel, "", "", "");
        Stop();
        GetPlaylist = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylist?playlist="+llEscapeURL(Playlist ), [], "" );
    }
    
    touch_start( integer _c )
    {
        if ( llDetectedKey( 0 ) == llGetOwner() )
            Menu( llDetectedKey( 0 ) );
        else llInstantMessage( llDetectedKey( 0 ), "Forbidden." );
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        if ( _m == ">> Next" ) {
            Start += 10;
            PlaylistsMenu(_i);
            return;
        } else if ( _m == "Back <<" ) {
            if ( ! Start ) Menu( _i );
            else {
                Start -= 10;
                PlaylistsMenu(_i);
            }
            return;
        } else if ( _m == "Play" ) {
            GetNext();
            Play();
            return;
        } else if ( _m == "Stop" ) {
            Stop();
            return;
        } else if ( _m == "Playlists" ) {
            GetPlaylists = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylists", [], "" );
            return;
        } else if ( _m == "Next" ) {
            GetPlaylistMovie = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylistMovie?direction=1&playlist="+llEscapeURL(Playlist )+"&current="+llEscapeURL(CurrentTexture), [], "" );
            return;
        } else if ( _m == "Back" ) {
            GetPlaylistMovie = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylistMovie?direction=0&playlist="+llEscapeURL(Playlist )+"&current="+llEscapeURL(CurrentTexture), [], "" );
            return;
        } else {
            Start = 0;
            Playlist = _m;
            llWhisper( 0, "The playlist: \"" + Playlist + "\" is now active" );
            GetPlaylist = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylist?playlist="+llEscapeURL(Playlist ), [], "" );
        }
    }

    http_response(key id, integer status, list meta, string body)
    {
        if ( GetPlaylists == id )
        {
            Playlists = llCSV2List( body );
            llWhisper( 0, "Playlists updated. " + (string) llGetListLength( Playlists ) + " playlists found." );
            PlaylistsMenu(llGetOwner());
        } else if ( GetPlaylist == id ) {
            PlaylistMovies = llCSV2List( body );
            llWhisper( 0, "Playlist \"" + Playlist + "\"'s movies updated." + (string) dictCount( PlaylistMovies ) + " movies found." );
            if ( AutoStart )
            {
                GetNext();
                Play();
            }
        } else if ( GetPlaylistMovie == id ) {
            list Temp = llCSV2List( body );
            CurrentTexture = llList2Key( Temp, 1 );
            CurrentTextureName = llList2String( Temp, 0 );
            CurrentTextureWidth = llList2Integer( Temp, 2 );
            CurrentTextureHeight = llList2Integer( Temp, 3 );
            CurrentTextureDuration = llList2Float( Temp, 4 );
            CurrentTextureFramesPerSecond = llList2Float( Temp, 5 );
            // Play texture
            llWhisper( 0, "Now playing: \"" + CurrentTextureName + "\" of playlist: \"" + Playlist + "\"" );
            llSetTexture( CurrentTexture, ALL_SIDES );
            llSetTextureAnim( ( 0 | ANIM_ON | LOOP ), ALL_SIDES,  CurrentTextureWidth, CurrentTextureHeight, 0.0, 0, CurrentTextureFramesPerSecond ); 
            llSetTimerEvent( CurrentTextureDuration );
        }
    }
    
    timer()
    {
        if ( AutoWebUpdate )
            GetPlaylistMovie = llHTTPRequest( "http://2.latest.geezmolaboratory.appspot.com/sl/television/ajax/getTexturesPlaylistMovie?direction=1&playlist="+llEscapeURL(Playlist )+"&current="+llEscapeURL(CurrentTexture), [], "" );
        else {
            list Temp = GetNext();
            CurrentTexture = llList2Key( Temp, 1 );
            CurrentTextureName = llList2String( Temp, 0 );
            CurrentTextureWidth = llList2Integer( Temp, 2 );
            CurrentTextureHeight = llList2Integer( Temp, 3 );
            CurrentTextureDuration = llList2Float( Temp, 4 );
            CurrentTextureFramesPerSecond = llList2Float( Temp, 5 );
            llWhisper( 0, "Now playing: \"" + CurrentTextureName + "\" of playlist: \"" + Playlist + "\"" );
            llSetTexture( CurrentTexture, ALL_SIDES );
            llSetTextureAnim( ( 0 | ANIM_ON | LOOP ), ALL_SIDES,  CurrentTextureWidth, CurrentTextureHeight, 0.0, 0, CurrentTextureFramesPerSecond ); 
            llSetTimerEvent( CurrentTextureDuration );
        }
    }
}