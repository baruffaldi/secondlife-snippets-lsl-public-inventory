integer up;
vector baseHeight;

default
{
    state_entry()
    {
        baseHeight = llGetPos();
        llSleep( 0.1 );
        llSetTimerEvent( 1 );
    }
    
    timer()
    {
        if ( up ) up = FALSE;
        else up = TRUE;
        
        vector bho = baseHeight;
        vector bhi = <0.0, 0.0, 0.1>;
         
        if ( up ) bho.z += 1.0;
        else bho.z -= 1.0;
        
        llStopMoveToTarget();
        llMoveToTarget( bho, 0.3 );
    }
}
 