vector xyz_angles = <.0, .0, 45.0>;

// ---------------// ---------------// ---------------// ---------------

rotation rot_xyzq;
 
default
{
    state_entry()
    {
        vector angles_in_radians = xyz_angles*DEG_TO_RAD; // Change to Radians
        rot_xyzq = llEuler2Rot(angles_in_radians); // Change to a Rotation
        llSetTimerEvent( 0.1 );
    }
    
    timer()
    {
        llSetRot(llGetRot()*rot_xyzq); //Do the Rotation...
    }
}
