float scanningIntervals = 3.0;
float scanningDistance = 96.0;

list targets;
list targetsFollowed;
integer enabled;

// ---------// ---------// ---------// ---------// ---------
 
 string notecardName;
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

// ---------// ---------// ---------// ---------// ---------

integer channel;
integer channelId;

// ---------// ---------// ---------// ---------// ---------

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
        
    if ( _text != "" )
        llOwnerSay( _text );
}

dropObject( key target )
{
    targetsFollowed += target;
    if( llGetInventoryNumber( INVENTORY_OBJECT ) > 0 )
        llRezObject( llGetInventoryName(INVENTORY_OBJECT, 0) , llGetPos() + <0.0,0.0,1.0>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, 0  );
    else llOwnerSay( "You have to put an object to drop" );
    llSleep( 1.0 );
    llShout( 1234647863, (string) target );
}

integer InRange(key uuid, float distance)
{
    list data = llGetObjectDetails(uuid, [OBJECT_POS]);
    if(data == [])
        return FALSE;
    return llVecDist(llList2Vector(data, 0), llGetPos()) <= scanningDistance;
}

default
{
    on_rez( integer _c )
    {
        llResetScript();
    }
    
    state_entry()
    {
        if ( llGetListLength( targets ) == 0 )
            state updateTargets;
        llSetText( "", <0.0,0.0,0.0>, 0.0 );
        enabled = FALSE;
    }
    
    changed( integer _change )
    {
        // Something has changed in inventory, maybe we got  new settings to import!
        if ( _change & CHANGED_INVENTORY )
            state updateTargets;
    }
    
    touch_start( integer _c )
    {
        if ( llGetOwner() == llDetectedKey(0) )
            state SecurityOrb;
    }
}

state SecurityOrb
{
    on_rez( integer _c )
    {
        llResetScript();
    }
    
    state_entry()
    {
        llSetText( "On", <1.0,1.0,1.0>, 1.0 );
        enabled = TRUE;
        llOwnerSay( "Activated!" );
        
        llSetTimerEvent(scanningIntervals);
    }

    state_exit()
    {
        llSetText( "Off", <1.0,1.0,1.0>, 1.0 );
        llSensorRemove();
    }

    timer()
    {
        integer i;
        key target;
        
        for( ; i<llGetListLength(targets); ++i)
        {
            target = llList2String(targets, i);
            if(~llListFindList(targetsFollowed, (list)target))
            {
                if ( InRange( target, 95.0 ) == FALSE )
                {
                    llOwnerSay( llKey2Name( target ) + " [" + (string)target + "] is no more here!" );
                    targetsFollowed = llDeleteSubList( targetsFollowed, llListFindList( targetsFollowed, [target] ), llListFindList( targetsFollowed, [target] ) );
                }
                //else llSay( 0,  llKey2Name( target ) + " [" + (string)target + "] already detected!" );
            }
            else
            {
                //llOwnerSay( "searching for: " + llKey2Name( target ) + " [" + (string)target + "]" );
                if ( InRange( target, 95.0 ) )
                {
                    dropObject( target );
                    //llOwnerSay( llKey2Name( (key)target ) + " [" + (string)target + "] detected, dropping shit!");
                } //else llOwnerSay( llKey2Name( target ) + " [" + (string)target + "] not present" );
            }
        }
    }
    
    touch_start( integer _c )
    {
        if ( llGetOwner() == llDetectedKey(0) )
        {
            llOwnerSay( "Deactivated!" );
            state default;
        }
    }
    
    changed( integer _change )
    {
        // Something has changed in inventory, maybe we got  new settings to import!
        if ( _change & CHANGED_INVENTORY )
            state updateTargets;
    }
}

state updateTargets
{
    on_rez( integer _c )
    {
        llResetScript();
    }
    
    state_entry()
    {
        targets = [];
        
        notecardName = llGetInventoryName( INVENTORY_NOTECARD, 0 );
        
        if ( notecardName == "" ) {
            llOwnerSay( "You have to create a notecard with all targets" );
            state default;
        }
            
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                report(
                    "Notecard! Leeching notecard in progress...",
                    "Notecard reading... 0%",
                    <1.0,1.0,1.0>, FALSE, FALSE
                );

                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
            }
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, 0 );
        }

        else if ( _queryid == notecardLineId )
        {
            if ( _data != EOF )
            {
                list temp;
                string name;
                string value;

                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    integer percent;
                    percent = ( notecardLine * 100 ) / notecardTotalLines;

                    report(
                        "",
                        "Notecard reading... " + (string) percent + "%",
                        <1.0,1.0,1.0>, FALSE, FALSE
                    );
                    
                    targets += [(key) llStringTrim(_data, STRING_TRIM)];
                    llOwnerSay( "New target: " + _data);
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            } else {
                report(
                    "Notecard reading completed!",
                    "Notecard reading completed!",
                    <1.0,1.0,1.0>, TRUE, TRUE
                );
                llSleep(1.0);
                if ( enabled ) state SecurityOrb;
                else state default;
            }
        }
    } 
}