integer Type = 1;
integer RandomColor = TRUE;
float SentenceRandomness = 2;

vector Color;

string Texture;
string Sentence;

list particle_parameters;

SetParticles( integer Active )
{
    particle_parameters = [  // start of particle settings
       // Texture Parameters:
       PSYS_SRC_TEXTURE, Texture, 
       PSYS_PART_START_SCALE, <.1, .1, FALSE>, PSYS_PART_END_SCALE, <1.0, 0.5, FALSE>, 
       PSYS_PART_START_COLOR, Color,    PSYS_PART_END_COLOR, <1,1,1>, 
       PSYS_PART_START_ALPHA,  (float)7.0,            PSYS_PART_END_ALPHA, (float)0.1,   
     
       // Production Parameters:
       PSYS_SRC_BURST_PART_COUNT, (integer)1, 
       PSYS_SRC_BURST_RATE, (float) 0.5,  
       PSYS_PART_MAX_AGE, (float)1.0, 
       PSYS_SRC_MAX_AGE,(float) 0.0,  
    
       // Placement Parameters:
       PSYS_SRC_PATTERN, (integer)1, // 1=DROP, 2=EXPLODE, 4=ANGLE, 8=ANGLE_CONE,
       
       // Placement Parameters (for any non-DROP pattern):
//       PSYS_SRC_BURST_SPEED_MIN, (float).0,   PSYS_SRC_BURST_SPEED_MAX, (float).1, 
    // PSYS_SRC_BURST_RADIUS, 0.0,
    
       // Placement Parameters (only for ANGLE & CONE patterns):
       PSYS_SRC_ANGLE_BEGIN, (float) 0.03*PI,    PSYS_SRC_ANGLE_END, (float)0*PI,  
     //PSYS_SRC_OMEGA, <0,0,1>, 
    
       // After-Effect & Influence Parameters:
       PSYS_SRC_ACCEL, <0,0,0>,  
    // PSYS_SRC_TARGET_KEY,      llGetLinkKey(llGetLinkNum() + 1),       
          
       PSYS_PART_FLAGS, (integer)( 0         // Texture Options:     
                            | PSYS_PART_INTERP_COLOR_MASK   
                            | PSYS_PART_INTERP_SCALE_MASK   
                        //  | PSYS_PART_EMISSIVE_MASK   
                            | PSYS_PART_FOLLOW_VELOCITY_MASK
                                              // After-effect & Influence Options:
                            | PSYS_PART_WIND_MASK            
                            | PSYS_PART_BOUNCE_MASK          
                         // | PSYS_PART_FOLLOW_SRC_MASK     
                         // | PSYS_PART_TARGET_POS_MASK     
                         // | PSYS_PART_TARGET_LINEAR_MASK   
                        ) 
        //end of particle settings                     
    ];
    
    if ( Active ) llParticleSystem( particle_parameters );
    else llParticleSystem( [] );
}

Talk( string String, integer Type, key Target )
{
    string Name = llGetObjectName();
    llSetObjectName("");
    if ( Type == 1 || Type == 0 )
        llWhisper(0, "/me " + String);
    else if ( Type == 2 )
        llSay(0, "/me " + String);
    else if ( Type == 3 )
        llShout(0, "/me " + String);
    llSetObjectName(Name);
}

default
{
    state_entry()
    {
        SetParticles( TRUE );
    }
    
    link_message( integer sibling, integer num, string mesg, key target_key )
    {
        if ( num == 123456 )
            Texture = mesg;
        else if ( num == 1234567 )
            Sentence = mesg;
        else if ( num == 12345678 )
            Type = (integer)mesg;
        else if ( num == 123456789 )
            RandomColor = (integer)mesg;
        else if ( num == 123456789 )
            SentenceRandomness = (integer)mesg;
    }
    
    moving_start( )
    {
        if ( RandomColor || Color == <0,0,0> ) Color = <llFrand(1),llFrand(1),llFrand(1)>;
        else Color = <1,1,1>;
        
        SetParticles( TRUE );
        if ( Sentence != "" && llFrand( SentenceRandomness ) > ( SentenceRandomness / 2 ) )
            Talk( Sentence, Type, NULL_KEY );
    }
    
    moving_end( )
    {
        SetParticles( FALSE );
    }
}