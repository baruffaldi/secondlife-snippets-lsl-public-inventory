// Geezmo Profume Emitter Settings Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

// ===> Texture Settings
// **********************

// (string|uuid) Set the texture's name or uuid
string Texture = "";

// (0|1) Every texture will have a random color
string RandomColor = "0";

// ===> Sentence Settings
// **********************

// (string) Set something to tell whenever the main script is enabled
string Sentence = "";

// (1|2|3) If a Sentence has been set, choose the message type
string Type = "1";  // Types:
                    // - 1 Whisper
                    // - 2 Say
                    // - 3 Shout
                         
// (number) If a Sentence has been set, you can set the randomness factor
// to get a low flooder talking.
string RandomnessSentence = "1000";


// =====================================================================
// ===---------------------------------------------------------------===
// ===-------------------[ DO NOT EDiT BELOW ]-----------------------===
// ===---------------------------------------------------------------===
// =====================================================================

default
{
    state_entry()
    {
        llMessageLinked( LINK_THIS, 123456, Texture, NULL_KEY );
        llMessageLinked( LINK_THIS, 1234567, Sentence, NULL_KEY );
        llMessageLinked( LINK_THIS, 12345678, Type, NULL_KEY );
        llRemoveInventory(llGetScriptName());
    }
}