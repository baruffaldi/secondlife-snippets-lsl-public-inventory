integer Team;

// -----------------------------------------------

integer Channel;
integer RefereeChannel = 3;

// -----------------------------------------------

string Referee;

integer IsRunning;
integer Goals;

integer Listen;
integer RefereeListen;

integer LCD_TEXT = 923846;
integer LCD_FONT = 836423;

Menu()
{
    if ( IsRunning ) 
        llDialog( llGetOwner(), "Choose an option below", ["Stop", "Referee"], Channel );
    else llDialog( llGetOwner(), "Choose an option below", ["Start", "Referee"], Channel );
}

integer ProcessAnswer( integer _c, string _n, key _i, string _m )
{
    if ( _c == 0 )
    {
        string Count;
        ++Goals;
        
        if ( Goals < 10 ) Count = "0" + (string) Goals;
        else Count = (string) Goals;

        llMessageLinked( LINK_THIS, LCD_TEXT, Count + "000000000000000", NULL_KEY );
        llSay(0, "goal porcodio" );
    }
    
    if ( _c == RefereeChannel )
    {
        Referee == _m;
        llSay( 0, "The new referee is: " + _m );
        llListenRemove( RefereeListen );
        return FALSE;
    }
    
    if ( _m == "Referee" )
    {
        llListenRemove( RefereeListen );
        RefereeListen = llListen( RefereeChannel, "", llGetOwner(), "" );
        llOwnerSay( "Type on channel 3 the new referee. ( Example: /3 Bornslippy Ruby )" );
    }
    
    else if ( _m == "Start" )
    {
        return 2;
    }
    
    else if ( _m == "Stop" )
    {
        return 3;
    }
    
    return FALSE;
}

default
{
    state_entry()
    {
        if ( llKey2Name( llGetCreator() ) != "Bornslippy Ruby" )
            state Disabled;
            
        llSetText( "Team " + (string) Team, <1, 1, 1>, 1 );
        Channel = -128637921;
        Listen = llListen( Channel, "", llGetOwner(), "" );
        if ( Team != FALSE )
            llMessageLinked( -1, LCD_TEXT, "T" + (string) Team + "0000000000000000000", NULL_KEY );
        else llMessageLinked( -1, LCD_TEXT, "__0000000000000000000", NULL_KEY );

        if ( Team != FALSE )
            llSetText( "Team " + (string) Team, <1, 1, 1>, 1 );
         else
            llSetText( "No Team", <1, 1, 1>, 1 );
    }
    
    state_exit()
    {
        llListenRemove( Listen );
    }
    
    touch_start( integer _n )
    {
        if ( llGetOwner() == llDetectedKey(0) )
            Menu();
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        integer Result = ProcessAnswer( _c, _n, _i, _m );
        
        if ( Result == 2 ) state Running;
    }

    link_message(integer sender, integer auth, string str, key id)
    {
        if ( auth == 30 ){ 
            Team = (integer) str;
            llSetText( "Team " + (string) Team, <1, 1, 1>, 1 );
            llMessageLinked( -1, LCD_TEXT, "T" + (string) Team + "0000000000000000000", NULL_KEY );}
    }
    
} state Disabled { state_entry() { llSay(0, "succhiami sta minchiazza ripper del cazzo" );}}

state Running
{
    state_entry()
    {
        IsRunning = TRUE;
        llListen( Channel, "", llGetOwner(), "" );
        llListen( 0, "Cipolla"+(string)Team, "", "Goaaalll!" );
        llMessageLinked( LINK_THIS, LCD_TEXT, "00000000000000000", NULL_KEY );
        llShout( 0, "Match started! Good luck to every team.." );
    }
    
    state_exit()
    {
        llShout( 0, "Match ended!" );
        llResetScript();
    }
    
    touch_start( integer _n )
    {
        if ( llGetOwner() == llDetectedKey(0) )
            Menu();
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
        integer Result = ProcessAnswer( _c, _n, _i, _m );
        
        if ( Result == 3 ) state default;
    }

    link_message(integer sender, integer auth, string str, key id)
    {
        if ( auth == 30 ){
            Team = (integer) str;
            llSetText( "Team " + (string) Team, <1, 1, 1>, 1 );
            llMessageLinked( -1, LCD_TEXT, "T" + (string) Team + "0000000000000000000", NULL_KEY );}

        if ( auth == 100|| auth == 101 )
        {
            if ( str == Referee || str == llKey2Name(llGetOwner()) )
            {
                if ( auth == 100 ) --Goals;
                else if ( auth == 101 ) ++Goals;
         
                string Count;
                if ( Goals < 10 ) Count = "0" + (string) Goals;
                else Count = (string) Goals;
        
                llMessageLinked( LINK_THIS, LCD_TEXT, Count + "000000000000000", NULL_KEY );       
            }
        }
        
    }
}