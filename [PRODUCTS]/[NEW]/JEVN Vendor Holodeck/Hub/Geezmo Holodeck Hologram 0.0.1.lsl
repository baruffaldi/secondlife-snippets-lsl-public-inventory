string  CONTROLLER_ID = "GH_HOLO_SWITCH"; // See comments at end regarding CONTROLLERS.
integer AUTO_START = FALSE;   // Optionally FALSE only if using CONTROLLERS.

list particle_parameters=[]; // stores your custom particle effect, defined below.
list target_parameters=[]; // remembers targets found using TARGET TEMPLATE scripts.

default {
    state_entry() {
        particle_parameters = [  // start of particle settings
           // Texture Parameters:
           //PSYS_SRC_TEXTURE, "388b27c2-74cf-72fc-e65c-7ba6bb4805ec", 
           PSYS_PART_START_SCALE, <2.0,2.0, FALSE>,  PSYS_PART_END_SCALE, <2.0,2.0, FALSE>, 
//           PSYS_PART_START_COLOR, <0,0.4,1>,      PSYS_PART_END_COLOR, <0,0.4,1>, 
           PSYS_PART_START_ALPHA, (float).20,       PSYS_PART_END_ALPHA, (float).10,     
         
           // Production Parameters:
           PSYS_SRC_BURST_PART_COUNT, (integer)2, 
           PSYS_SRC_BURST_RATE, (float) 0.05,  
           PSYS_PART_MAX_AGE, (float)0.8, 
           PSYS_SRC_MAX_AGE,(float) 0.0,  
        
           // Placement Parameters:
           PSYS_SRC_PATTERN, (integer)4, // 1=DROP, 2=EXPLODE, 4=ANGLE, 8=ANGLE_CONE,
           
           // Placement Parameters (for any non-DROP pattern):
          PSYS_SRC_BURST_SPEED_MIN, (float)0.0,   PSYS_SRC_BURST_SPEED_MAX, (float)0.0, 
           PSYS_SRC_BURST_RADIUS, 2.0,
        
           // Placement Parameters (only for ANGLE & CONE patterns):
        // PSYS_SRC_ANGLE_BEGIN, (float) .25*PI,   PSYS_SRC_ANGLE_END, (float)0.0*PI,  
        // PSYS_SRC_OMEGA, <0,0,0>, 
        
           // After-Effect & Influence Parameters:
           PSYS_SRC_ACCEL, <0.0,0.0,0.0>,  
        // PSYS_SRC_TARGET_KEY,      llGetLinkKey(llGetLinkNum() + 1),       
              
           PSYS_PART_FLAGS, (integer)( 0         // Texture Options:     
                                | PSYS_PART_INTERP_COLOR_MASK   
                                | PSYS_PART_INTERP_SCALE_MASK   
                                | PSYS_PART_EMISSIVE_MASK   
                             // | PSYS_PART_FOLLOW_VELOCITY_MASK
                                                  // After-effect & Influence Options:
                                | PSYS_PART_WIND_MASK            
                                | PSYS_PART_BOUNCE_MASK          
                             // | PSYS_PART_FOLLOW_SRC_MASK     
                             // | PSYS_PART_TARGET_POS_MASK     
                             // | PSYS_PART_TARGET_LINEAR_MASK   
                            ) 
            //end of particle settings                     
        ];
        
        if ( AUTO_START ) llParticleSystem( particle_parameters );
        
    }
    
    link_message( integer sibling, integer num, string mesg, key target_key ) {
        if ( mesg != CONTROLLER_ID ) { // this message isn't for me.  Bail out.
            return;
        } else if ( num == 0 ) { // Message says to turn particles OFF:
            llParticleSystem( [ ] );
        } else if ( num == 1 ) { // Message says to turn particles ON:
            llParticleSystem( particle_parameters + target_parameters );
        } else if ( num == 2 ) { // Turn on, and remember and use the key sent us as a target:
            target_parameters = [ PSYS_SRC_TARGET_KEY, target_key ];
            llParticleSystem( particle_parameters + target_parameters );
        } else { // bad instruction number
            // do nothing.
        }            
    }
        
}