integer Notice = FALSE;

string Vendor;

string HoloRot = "GH_THOLO_ROTATOR";
string THolo   = "GH_THOLO_SWITCH";
string PHolo   = "GH_HOLO_SWITCH";
string Light   = "GH_LIGHT";
string TLight  = "GH_THOLO_LIGHT";
string TSizer  = "GH_THOLO_SIZER";
       
integer ListenChannel;
integer ListenId;

integer Mode = -1;
integer Size;

list InventoryTextures;
list NotecardTextures = [3];

 string notecardName;
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;


on( float width, string front, string rear )
{
    if ( Mode )
    {
        llMessageLinked( -1, -1, TLight, "1" );
        llMessageLinked( -1, -1, Light, "0.7" );
        llMessageLinked( -1, 1, HoloRot, "<0,0,1>" );

        llMessageLinked( -1, 0, TSizer, "<0.01000, "+(string)width+", 3.00000>" );
        llMessageLinked( -1, 2, THolo, front );
        if ( rear != NULL_KEY )
             llMessageLinked( -1, 4, THolo, rear );
        else llMessageLinked( -1, 4, THolo, front );
    } else {
        llMessageLinked( -1, 0, TLight, "0" );
        llMessageLinked( -1, -2, THolo, NULL_KEY );

        llMessageLinked( -1, 1, Light, "0.7" );
        llMessageLinked( -1, 2, PHolo, front );
    }
}

off( integer notFound )
{
    llMessageLinked( -1, -1, Light, "0" );
    llMessageLinked( -1, -1, TLight, "0" );
    llMessageLinked( -1, -2, THolo, NULL_KEY );
    llMessageLinked( -1, 0, PHolo, NULL_KEY );
    llMessageLinked( -1, 0, HoloRot, "<0,0,0>" );
    
    if ( Notice && notFound ) llOwnerSay( "Hologram not present" );
}

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
        
    if ( _text != "" )
        llOwnerSay( _text );
}

integer like( string value, string mask ) {
    integer tmpy = ( llGetSubString( mask,  0,  0 ) == "%" ) | 
                   ( ( llGetSubString( mask, -1, -1 ) == "%" ) << 1 );
    if( tmpy )
        mask = llDeleteSubString(mask, (tmpy / -2), -(tmpy == 2));
 
    integer tmpx = llSubStringIndex( value, mask );
    if( ~tmpx ) {
        integer diff = llStringLength( value ) - llStringLength( mask );
        return  ( ( !tmpy && !diff)
             || ( ( tmpy == 1 ) && ( tmpx == diff ) )
             || ( ( tmpy == 2 ) && !tmpx )
             ||   ( tmpy == 3 ) );
    }
    return FALSE;
}

string getProductName( string _m )
{
    list s = llParseString2List( _m, [" "], [] );
    string ret;
    
    integer i = 1;
    for(;i<(llGetListLength(s) - 3);++i)
        ret += " " + llList2String( s, i );
    
    return llGetSubString( ret, 1, llStringLength(ret) );
}

integer DictCount(list dict)
{
    return (llGetListLength(dict)-1)/(llList2Integer(dict,0)+1);
}

integer DictFindKey(list dict, string dkey)
{
    if (dkey == "") 
        return -1;
    else {
        integer elements = llList2Integer(dict,0);
        return llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    }
}
 
list DictGetItem(list dict, string dkey)
{
    if (dkey == "") return []; //dkey = llToLower(dkey);
    integer elements = llList2Integer(dict,0);
    integer loc = llListFindList(llList2ListStrided(llList2List(dict,1,-1),0,-1,elements+1),[dkey]);
    if (loc<0)
        return [];
    else
        return llList2List(llList2List(dict,1,-1),loc*(elements+1)+1,loc*(elements+1)+elements);
}

string stackdump(list x) {
    return "["+llDumpList2String(x, "] | [")+"]";
}

default
{
    on_rez(integer _p)
    {
        llResetScript();
    }
    
    state_entry()
    {
        llSetText( "", <0,0,0>, 0 );
        if (llGetInventoryCreator(llGetScriptName()) != llGetCreator()) state Disabled;
        
        off( FALSE );
        
        ListenChannel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        ListenId = llListen( ListenChannel, "", llGetOwner(), "");
    }

    touch_start(integer total_number)
    {
        if ( llDetectedKey( 0 ) == llGetOwner() )
        {
            list buttons;

            if ( llGetListLength( InventoryTextures ) > 0 ) buttons += "0";
            if ( DictCount( NotecardTextures ) > 0 ) buttons += "1";
            if ( Mode != -1 ) buttons += "Start";

            buttons += "Refresh";
            
            llDialog( llGetOwner(), "*** Geezmo Holodeck ***\nVendor Model: " + Vendor + "\nActual Service: " + (string) Mode + "\nChoose the service:\n0 - Inventory Textures(single)\n1 - Notecard Textures(single/double)", buttons, ListenChannel );
        }
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        if ( _m == "Start" ) {
            if ( Mode == -1 ) llOwnerSay( "You have to choose a service's type" );
            else state Running;
        }
        else if ( _m == "Refresh" ) state Refresh;
        
        Mode = (integer) _m;
        
        list buttons;
        
        if ( llGetListLength( InventoryTextures ) > 0 ) buttons += "0";
        if ( DictCount( NotecardTextures ) > 0 ) buttons += "1";
        if ( Mode != -1 ) buttons += "Start";
        
        buttons += "Refresh";
        
        llDialog( llGetOwner(), "*** Geezmo Holodeck ***\nVendor Model: " + Vendor + "\nActual Service: " + (string) Mode + "\nChoose the service:\n0 - Inventory Textures(single)\n1 - Notecard Textures(single/double)", buttons, ListenChannel );
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_INVENTORY )
            if ( llGetInventoryNumber( INVENTORY_TEXTURE ) || llGetInventoryNumber( INVENTORY_NOTECARD ) )
                state Refresh;
    }
}

state Running
{
    state_entry()
    {
        if (llGetInventoryCreator(llGetScriptName()) != llGetCreator()) state Disabled;
        llOwnerSay( "Service started." );
        ListenId = llListen( 0, Vendor, NULL_KEY, "" );
    }
    
    state_exit()
    {
        llListenRemove(ListenId);
        llOwnerSay( "Service stopped." );
    }
    
    listen( integer _c, string _n, key _k, string _m )
    {
        integer found;
        string texture = llGetInventoryName( INVENTORY_TEXTURE, 0 );
        
        if ( like( _m, "% - L$ %" ) )
        {
            string name = getProductName( _m );
            
            if ( Mode ) {
                off(TRUE);
                if ( DictFindKey( NotecardTextures, name ) != -1 )
                {
                    list textures = DictGetItem( NotecardTextures, name );
                    on( llList2Float( textures, 0 ), llList2String( textures, 1 ), llList2String( textures, 2 ) );
                }
            } else {
                off(TRUE);
                if ( ~llListFindList( InventoryTextures, [name] ) )
                    on( 0, name, NULL_KEY );
            }
        }
    }

    touch_start(integer total_number)
    {
        if ( llDetectedKey( 0 ) == llGetOwner() )
            state default;
    }
    
    changed( integer _c )
    {
        if ( _c & CHANGED_INVENTORY )
            if ( llGetInventoryNumber( INVENTORY_TEXTURE ) || llGetInventoryNumber( INVENTORY_NOTECARD ) )
                state Refresh;
    }
} state Disabled{state_entry(){off(FALSE);}}

state Refresh
{
    state_entry()
    {
        Mode = -1;
        InventoryTextures = [];
        NotecardTextures = [3];
        Vendor = "";

        llOwnerSay( "Searching for JEVN Vendors..." );
        llSensor( "", NULL_KEY, (SCRIPTED | PASSIVE), 7, PI );
    }
    
    sensor( integer _d )
    {
        integer i;
        for(;i<_d;++i)
            if ( llDetectedOwner( i ) == llGetOwner() )
                if ( like( llDetectedName( i ), "%JEVN - Vendor%" ) )
                    Vendor = llDetectedName( i );
        
        if ( Vendor != "" )
        {
            llOwnerSay( "Vendor \"" + Vendor + "\" found." );
            llOwnerSay( "Loading products holograms..." );
    
            if ( llGetInventoryNumber( INVENTORY_TEXTURE ) < 1 )
                llOwnerSay( "Inventory Textures not found. All Inventory Textures features are disabled." );
            else {
                llOwnerSay( "Inventory Textures found." );
                integer i;
                for ( ; i < llGetInventoryNumber( INVENTORY_TEXTURE ); ++i )
                    InventoryTextures += llGetInventoryName( INVENTORY_TEXTURE, i );
            }
    
            if ( llGetInventoryNumber( INVENTORY_NOTECARD ) < 1 ) {
                llOwnerSay( "Textures Notecard not found. All Textures Notecard features are disabled." );
                state default;
            } else {
                notecardName = llGetInventoryName( INVENTORY_NOTECARD, 0 );
                
                if ( notecardName == "" ) {
                    llOwnerSay( "You have to create a notecard with all products textures in order to use the notecard features" );
                    state default;
                }
                    
                if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
                    if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
                    {
                        report(
                            "Notecard! Leeching notecard in progress...",
                            "Notecard reading... 0%",
                            <1.0,1.0,1.0>, FALSE, FALSE
                        );
                        
                        notecardLine = 0;
                        notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
                    }
            }
        } else {
            llOwnerSay( "Vendor not found" );
            state default;
        }
    }
    
    no_sensor()
    {
        llOwnerSay( "Vendor not found" );
        state default;
    }
    
    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                list temp;
                string name;
                string front;
                string rear;
                float width;

                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    integer percent;
                    percent = ( notecardLine * 100 ) / notecardTotalLines;

                    report(
                        "",
                        "Notecard reading... " + (string) percent + "%",
                        <1.0,1.0,1.0>, FALSE, FALSE
                    );
                    
                    temp = llParseString2List( _data, ["="], [] );
                    name = llStringTrim( llList2String( temp, 0 ), STRING_TRIM );
                    width = (float) llStringTrim( llList2String( llCSV2List( llList2String( temp, 1 ) ), 0 ), STRING_TRIM );
                    front = llStringTrim( llList2String( llCSV2List( llList2String( temp, 1 ) ), 1 ), STRING_TRIM );
                    
                    if ( llGetListLength( llCSV2List( llList2String( temp, 1 ) ) ) > 2 )
                        rear = llStringTrim( llList2String( llCSV2List( llList2String( temp, 1 ) ), 2 ), STRING_TRIM );
                    else rear = NULL_KEY;
                    
                    NotecardTextures += name;
                    NotecardTextures += width;
                    NotecardTextures += front;
                    NotecardTextures += rear;
                }

                notecardLine++;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else {
            report(
                "Notecard reading completed! " + (string) DictCount( NotecardTextures ) + " Products found.",
                "Notecard reading completed!",
                <1.0,1.0,1.0>, TRUE, TRUE
            );
            llSleep(1.0);
            state default;
        }
    }
}