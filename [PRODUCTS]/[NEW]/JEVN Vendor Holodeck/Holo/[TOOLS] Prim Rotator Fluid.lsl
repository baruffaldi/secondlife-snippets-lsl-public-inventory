// Geezmo PrimRotator FreeBie Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

string CONTROLLER = "GH_THOLO_ROTATOR";
integer AUTOSTART = FALSE;
    
vector Axis = <0, 0, 1>;
float SpinRate = 0.3;
float Gain = PI;

// --------------------------------------------

default
{
    state_entry()
    {
       llTargetOmega( <0, 0, 0>, 0, Gain );
       if ( AUTOSTART ) llTargetOmega( Axis, SpinRate, Gain);
    }
    
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _m == CONTROLLER )
        {
            if ( _n ) {
                string tmp = (string) _k;
                if ( (vector) tmp != <0, 0, 0> )
                {
                    llTargetOmega( (vector) tmp, SpinRate, Gain);
                    return;
                }                    
            }
            
            llTargetOmega( <0, 0, 0>, 0, Gain );
        }
    }
}
