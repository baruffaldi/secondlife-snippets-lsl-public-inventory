// Geezmo TextureController FreeBie Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 

string CONTROLLER = "GH_THOLO_SWITCH";

// ---------------------------------------------------

default
{
    link_message( integer _sn, integer _n, string _m, key _k )
    {
        if ( _m == CONTROLLER )
        {
            if ( _n == -2 )
                llSetTexture( "bd7d7770-39c2-d4c8-e371-0342ecf20921", ALL_SIDES );
            else if ( _k != NULL_KEY )
                llSetTexture( _k, _n );
            
        }
    }
}
