// Truth Game by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ Filippo Baruffaldi <filippo@baruffaldi.info>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
  
key target = NULL_KEY;
key source = NULL_KEY;

string target_name;
string header_truth = "\n---------------------------\nThe Truth Game\n---------------------------\n\n";

float   radius    = 70.0;

integer intervals = 5;
integer channel   = 0;
integer questions = 0;

// 0 - inactive
// 1 - active, waiting requests
// 2 - request, choose target
// 3 - type the question
integer status = FALSE;
integer dialogStatus = FALSE;

list order_buttons( list buttons )
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4)
         + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}
    
stopTheGame()
{
    status = FALSE;
    dialogStatus = FALSE;
    source = NULL_KEY;
    target = NULL_KEY;
    target_name = "";
    llListenRemove(3);
}

default
{
    on_rez(integer x)
    {
        llResetScript();
    }

    state_entry()
    {
        llListen(3, "", "", "");
        
        llSay(
            0,
            header_truth + "The game is ready. Ask the owner to start the game.\n\n"
        );
    }

    touch_start( integer total_number )
    {
        if( ! status && llDetectedKey(0) == llGetOwner() )
        {
            llDialog(
                llGetOwner(),
                header_truth + "Click on \"Start\" button to start playing the Truth Game.",
                order_buttons(["Start"]),
                3
            );
        }
        
        else if( ! status && source != llDetectedKey(0) && source != NULL_KEY )
                llInstantMessage( llDetectedKey(0), "The game is not started. Ask the owner to start it!" );
        
        else if( status )
        {
            source = llDetectedKey(0);
            status = 2;
            dialogStatus = FALSE;
            
            llSay(
                0,
                header_truth + llKey2Name(source) + " wanna ask something..\n\n"
            );
        
            llSensorRepeat( "",
                NULL_KEY,
                AGENT,
                radius,
                PI,
                intervals
            );
        }
        
        else if( status >= 2 && source != NULL_KEY && source != llDetectedKey(0) )
        {
            llInstantMessage(llDetectedKey(0), "Please wait your turn, " + llKey2Name(source) + " is writing his question. Thanks");
        }
    }
    
    sensor( integer total_number )
    {
        if( status == 2 )
        {
            integer x;
            list buttons = ["CancelRequest"];

            if(source == llGetOwner()) buttons += ["CloseGame"];
            else if(source == llGetOwner()) buttons += ["StartGame"];

            for(x=0;x <= total_number;x++)
                if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                    buttons += [(string)llKey2Name(llDetectedKey(x))];
             
            if( !dialogStatus || source == llGetOwner() ) 
            {
                llDialog(
                    source,
                    header_truth + "Please choose a target who will receive the question:",
                    order_buttons(buttons),
                    3
                );
                dialogStatus = TRUE;
            }
            
            status = 3;
        }
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if( _option == "Start" && _id == llGetOwner() )
        {
            status = TRUE;
            
            llSay(
                0,
                header_truth + "The game is started!\n\nType /3 help to view the usage or just touch me to choose who you wanna ask something.\n\n"
            );
        }
        
        else if( _option == "CloseGame" && _id == llGetOwner() )
        {
            stopTheGame();
            
            llSay(
                0,
                header_truth + " is ended!\n\nThanks everyone, come back soon.\n\n"
            );
        }

        else if( _option == "CancelRequest" )
        {
            status = TRUE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
            
            llSay(
                0,
                header_truth + llKey2Name(source) + " looks like shy and he/she doesn't wanna ask anymore..\n\nI'm able again now for another question!\n\n"
            );
        }

        else if( status == 2 )
        {
            target_name = _option;
            llSensorRemove();
            llInstantMessage(source, "Now you can tell me your question for " + target_name + " by typing /3 TheQuestion");
        }

        else if( status == 3 )
        {
            llSay(
                0,
                header_truth + "From: " + llKey2Name(source) + "\nTo: " + target_name + "\n\nQuestion:\n"+_option+"\n\nNow I'm available to manage another question... come on, touch me again!\n\n"
            );
            
            questions++;
            
            status = TRUE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
        }
        
        else if( _option == "help")
                llInstantMessage(_id, header_truth + "To play this game you just need to be true with other players, there's no enemies, just true people.\n\nIf you touch me, I'll keep your request, and right that moment I won't receive another player's requests. After some seconds, I'll give you the list of the players.\n\nAfter this step, remains just to type on localchat:\n\n /3 QUESTION\nWhere QUESTION is the question you want to ask.\n\nIf he/she's a good player, he/she will be true!\n\nGood Game\n\n");
    }
    
    no_sensor()
    {
        stopTheGame();
    }
}