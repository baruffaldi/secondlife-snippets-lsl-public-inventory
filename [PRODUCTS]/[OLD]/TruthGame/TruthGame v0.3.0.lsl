// Truth Game by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ Filippo Baruffaldi <filippo@baruffaldi.info>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
  
key target = NULL_KEY;
key source = NULL_KEY;

string target_name;
string header_truth = "\n---------------------------\nThe Truth Game\n---------------------------\n\n";

integer interval  = 1;
integer cycles    = 1;
integer cycle     = 0;
integer intensity = 1000;
integer radius    = 70;
integer damage    = 10;

integer gameStarted = FALSE;

integer mode      = 0;
integer invisible = 0;

integer channel   = 0;

integer dialogStatus = FALSE;
integer actionStatus = -1;

list order_buttons( list buttons )
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4)
         + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}

lightnings( integer activate )
{
    llSetStatus(STATUS_PHYSICS,FALSE);
    llSetStatus(STATUS_PHANTOM,TRUE);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "fire", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "fire", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "fire", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llMakeExplosion(20, 1.0, 5, 3.0, 1.0, "Smoke", ZERO_VECTOR);
    llSetStatus(STATUS_PHYSICS,TRUE);
    llSetStatus(STATUS_PHANTOM,FALSE);
}

default
{
    on_rez(integer x)
    {
        llResetScript();
    }

    state_entry()
    {
        llSay(
            0,
            header_truth + "The game is ready.\nThe owner is able to start the game.\n\n"
        );
    }

    touch_start(integer total_number)
    {
        if(source != NULL_KEY && source != llDetectedKey(0))
        {
            llInstantMessage(llDetectedKey(0), "Please wait your turn, " + llKey2Name(source) + " is writing his question. Thanks");
            return;
        }
        else if(!actionStatus)
        {
            source = llDetectedKey(0);
            actionStatus = TRUE;
            dialogStatus = FALSE;
            
            llSay(
                0,
                header_truth + llKey2Name(source) + " wanna ask something..\n\n"
            );
        
            llSensorRepeat( "",
                NULL_KEY,
                AGENT,
                70.0,
                PI,
                5
            );
        } 
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "help")  llInstantMessage(_id, header_truth + "To play this game you just need to be true with other players, there's no enemies, just true people.\n\nIf you touch me, I'll keep your request, and right that moment I won't receive another player's requests. After some seconds, I'll give you the list of the players.\n\nAfter this step, remains just to type on localchat:\n\n /3 QUESTION\nWhere QUESTION is the question you want to ask.\n\nIf he/she's a good player, he/she will be true!\n\nGood Game\n\n");
        else if(_option == "CancelRequest" && actionStatus )
        {
            llSay(
                0,
                header_truth + llKey2Name(source) + " looks like shy and he/she doesn't wanna ask anymore..\n\nI'm able again now for other questions!\n\n"
            );
            
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
        }
        else if(_option == "CloseGame" && _id == llGetOwner() )
        {
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target = NULL_KEY;
            target_name = "";
            llListenRemove(3);
            
            llSay(
                0,
                header_truth + " is ended!\n\nThanks everyone, come back soon.\n\n"
            );
        }
        else if(_option == "StartGame" && _id == llGetOwner() )
        {
            gameStarted = TRUE;
            
            llListen(3, "", "", "");
            
            llSay(
                0,
                header_truth + "The game is started!\n\nType /3 help to view the usage or just touch me to choose who you wanna ask something.\n\n"
            );
        }
        else if( actionStatus == 2 )
        {
            llSay(
                0,
                header_truth + "From: " + llKey2Name(source) + "\nTo: " + target_name + "\n\nQuestion:\n"+_option+"\n\nNow I'm available to manage another question... come on, touch me again!\n\n"
            );
            
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
        }
        else if( actionStatus )
        {
            target_name = _option;
            actionStatus = 2;
            llSensorRemove();
            llInstantMessage(source, "The current target is: " + target_name + ", now I'm waiting for the question.");
        }
        
    }
    
    sensor(integer total_number)
    {
        lightnings(TRUE);

        if(actionStatus)
        {
            integer x;
            list buttons = ["CancelRequest"];
            
            if(source == llGetOwner() && gameStarted) buttons += ["CloseGame"];
            else if(source == llGetOwner()) buttons += ["StartGame"];

            for(x=0;x <= total_number;x++)
                if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                    buttons += [(string)llKey2Name(llDetectedKey(x))];
             
            if( !dialogStatus || source == llGetOwner() ) 
            {
                llDialog(
                    source,
                    header_truth + "Please choose a target who will receive the question:",
                    order_buttons(buttons),
                    3
                );
                dialogStatus = TRUE;
            }
        } else llInstantMessage(source, "The game is not started. Ask the owner to start it!" );
        
        lightnings(FALSE);
    }
}