// Truth Game v0.5.2 by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ F.B. <mdb@coolminds.org>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
//        
//TODO: Ogni 9 players mandare un dialog diverso
//TODO: Settings ( solo admin ): Channel, Range, Public/Private
        
key owner  = NULL_KEY;
key source = NULL_KEY;
key target = NULL_KEY;

string target_name;
string header = "\n---------------------------\nThe Truth Game\n---------------------------\n\n";

float radius = 40.0;

integer intervals = 8;
integer queschan  = 3;
integer channel   = 0;
integer questions = 0;
integer totalques = 0;

integer random = FALSE;
integer public = FALSE;

//
//
//

list order_buttons( list buttons )
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4) +
        llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}

integer randInt(integer n)
{
    return (integer)llFrand(n + 1);
}

integer rand(integer min, integer max)
{
    return min + randInt(max - min);
}

resetScript()
{
    source = NULL_KEY;
    target = NULL_KEY;
    target_name = "";
}

//
//
//

default
{
    on_rez(integer x)
    {
        llResetScript();
        if(owner != llGetOwner())
            owner = llGetOwner();
    }
    
    changed(integer change)
    {
        llResetScript();
        
        if (change & CHANGED_OWNER)
            owner = llGetOwner();
    }
    
    state_entry()
    {
        owner = llGetOwner();
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        questions = 0;
        
        llListen(channel, "", owner, "");
        
        //llSay(
        //    0,
        //    header + "The game is ready. Ask " + llKey2Name( owner ) + " to start the game.\n\n"
        //);
    }
    
    state_exit()
    {
        llListenRemove(channel);
    }

    touch_start( integer total_number )
    {    
        if( llDetectedKey(0) == llGetOwner() )
            llDialog(
                llGetOwner(),
                header + "Choose the game mode to start playing the Truth Game.",
                order_buttons(["On Request", "Random", "Channel"]),
                channel
            );
        
        else llInstantMessage(
            llDetectedKey(0),
            "The game is not started. Ask " + llKey2Name(owner) + " to start it!"
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    { 
        if( _option == "On Request" )
        {
            llSay(
                0,
                header + "(onRequest mode)\nThe game is started!\n\nTouch me to reserve your turn.\n\n"
            );
        
            random = FALSE;
    
            state GameStarted;
        }
        
        else if( _option == "Random" )
        {
            llSay(
                0,
                header + "(random mode)\nThe game is started!\n\nNow I'll choose the first who will ask something...\nLet me look around!\n\n"
            );
            
            random = TRUE;
            
            state SearchingSource;
        }
    
        else if( _option == "Channel" )
        {
            llDialog(
                llGetOwner(),
                "Choose the game channel where the players will types the questions into:",
                order_buttons(["1","2","3","4","5","6","7","8","9"]),
                channel
            );
        }
         
        else if( (integer)_option >= 1 )
        {
            queschan = (integer) _option;

            llInstantMessage(
                owner,
                "The new game channel is: " + (string) queschan
            );
        }
    }
}

state GameStarted
{
    state_entry()
    {
    }
    
    touch_start( integer total_number )
    {
        if( source != NULL_KEY && source != llDetectedKey(0) )
            llInstantMessage(
                llDetectedKey(0),
                "Please wait your turn, " + llKey2Name(source) + " is writing his question. Thanks"
            );
    
        else
        {
            source = llDetectedKey(0);
            state SearchingTarget;
        }
    }
}

state SearchingSource
{
    state_entry()
    {
        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            radius,
            PI,
            1
        );
    }

    state_exit()
    {
        llSensorRemove();
    }
    
    sensor( integer total_number )
    {
        integer x;
        
        x = rand( 0, ( total_number - 1 ) );

        source = llDetectedKey(x);

        llSay(
            0,
            "The lucky one is: " + llKey2Name( source )
        );
        
        state SearchingTarget;
    }
 
    no_sensor()
    {
        resetScript();
        state GameStarted;
    }
}
 
state SearchingTarget
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel, "", source, "");

        if(!random)
            llSay(
                0,
                header + llKey2Name(source) + " wanna ask something..\n\n"
            );

        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            radius,
            PI,
            intervals
        );
    }

    state_exit()
    {
        llListenRemove(channel);
        llSensorRemove();
    }
    
    sensor( integer total_number )
    {
        integer x;
        list buttons;

        if(!random)
            buttons = ["Cancel"];
            
        if(source == llGetOwner())
            buttons += ["CloseGame"];

        for(x=0;x <= total_number;x++)
            if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != source)
                buttons += [(string)llKey2Name(llDetectedKey(x))];

        llDialog(
            source,
            header + "Please choose a target who will receive the question:",
            order_buttons(buttons),
            channel
        );
    }

    no_sensor()
    {
        resetScript();
        if(random) state SearchingSource;
        else state GameStarted;
    }
    
    listen(integer _chan, string _name, key _id, string _option)
    {
        if( _option == "CloseGame" && _id == llGetOwner() )
        {
            llSay(
                0,
                header + "The game is ended!\n\nThanks everyone, come back soon.\n\n"
            );
            
            state default;
        }
    
        else if( _option == "Cancel" )
        {
            llSay(
                0,
                header + llKey2Name(source) + " looks like shy and he/she doesn't wanna ask anymore..\n\nI'm able again now for another question!\n\n"
            );
        
            resetScript();
        
            if(random) state SearchingSource;
            else state GameStarted;
        }
    
        else
        {
            target_name = _option;
            
            state SetTargetUUID;
        }
    }
}

state SetTargetUUID
{
    state_entry()
    {
        llSensorRepeat(
            "",
            NULL_KEY,
            AGENT,
            radius,
            PI,
            1
        );
    }
    
    state_exit()
    {
        llSensorRemove();
    }
        
    sensor( integer total_number )
    {
        integer x;
        for(x=0;x <= total_number;x++)
            if(llKey2Name(llDetectedKey(x)) == target_name )
                target = llDetectedKey(x);

        if(target == NULL_KEY)
        {
            llSay(
                0,
                "I'm afraid, I can't find " + target_name + "! Choose another target..."
                );
            
            if(random) state SearchingTarget;
            else state GameStarted;

        } else state WaitingQuestion;
    }
    
    no_sensor()
    {
        resetScript();
        if(random) state SearchingSource;
        else state GameStarted;
    }
}

state WaitingQuestion
{
    state_entry()
    {
        llListen(queschan, "", source, "");
            
        llInstantMessage(
            source,
            "Now you can tell me your question for " + target_name + " by typing /" + (string) queschan + " blahblah"
        );
    }
    
    state_exit()
    {
        llListenRemove(queschan);
    }
     
    listen(integer _chan, string _name, key _id, string _option)
    {
        string next;
         
        if(random) next = "Now " + target_name + " have to choose the new question target!";
        else next = "Now I'm available to manage another question... come on, touch me again!";
        
        questions++;
        totalques++;

        llSay(
            0,
            header + "Question " + (string)questions + " of " + (string) totalques + " total questions.\n\nFrom: " + llKey2Name(source) + "\nTo: " + target_name + "\n\nQuestion:\n" + _option + "\n\n" + next + "\n\n"
        );
        
        if(random) {
            source = target;
            target = NULL_KEY;
            target_name = "";
            state SearchingTarget;
        }
        else {
            resetScript();
            state GameStarted;
        }
    }
}