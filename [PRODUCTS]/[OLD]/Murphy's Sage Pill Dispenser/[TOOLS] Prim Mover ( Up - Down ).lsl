// Geezmo ObjectOrbit FreeBie Script v0.1
// Copyright (C) 2009 Bornslippy Ruby ( Geezmo Lab. )
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// 
    
integer up;
vector baseHeight;
integer running = TRUE;

default
{
    state_entry()
    {
        baseHeight = llGetPos();
        llSleep( 0.1 );
        llSetTimerEvent( 2 );
    }
    
    touch_start(integer _c)
    {
        if ( llDetectedKey(0) != llGetOwner() ) return;
        if ( running ) running = FALSE;
        else running = TRUE;
        
        if ( running ) {        
            baseHeight = llGetPos();
            llSetTimerEvent(2);
        }
        else llSetTimerEvent(0);
    }
    
    timer()
    {
        if ( up ) up = FALSE;
        else up = TRUE;
        
        if ( up ) baseHeight.z += 1.0;
        else baseHeight.z -= 1.0;
        
        llStopMoveToTarget();
        llMoveToTarget( baseHeight, 2 );
        llSetRot(llEuler2Rot(<0,0,0>*DEG_TO_RAD));
    }
}
 