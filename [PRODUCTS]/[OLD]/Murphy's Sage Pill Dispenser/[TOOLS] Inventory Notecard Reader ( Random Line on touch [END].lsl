string target_name = "fail";

// ---------// ---------// ---------// ---------// ---------
 
 string notecardName;
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

// --------------------------------------------------------

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{ 
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
        
    if ( _text != "" )
        llSay( PUBLIC_CHANNEL, _text );
}

default
{
    state_entry()
    {
        if ( llGetInventoryNumber( INVENTORY_NOTECARD ) > 0 )
        {
            notecardName = llGetInventoryName(INVENTORY_NOTECARD, 0);
            
            report(
                "",
                "Touch me to get a lil sage pill",
                <1.0,1.0,1.0>, TRUE, TRUE
            );
        } else report(
                    "",
                    "I'm sorry, I'm out of order.. retry later.",
                    <1.0,1.0,1.0>, TRUE, TRUE
                );
    }
    
    on_rez(integer _c)
    {
        llResetScript();
    }
        
    changed( integer _change )
    {
        if ( _change == CHANGED_INVENTORY )
        {
            if ( llGetInventoryNumber( INVENTORY_NOTECARD ) > 0 )
            {
                notecardName = llGetInventoryName(INVENTORY_NOTECARD, 0);
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName ); 
            }
        }
    }
    
    touch_start(integer _c)
    {
        if ( notecardTotalLines > 0 )
        {
            notecardLine = (integer)llFrand(notecardTotalLines);
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        } else {
            if ( notecardName != "" )
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName ); 
            llInstantMessage( llGetOwner(), "Notecard missing... be fast, I'm feeling lost without ballshits to shoot to the ppl around me" );
            llSay(0, "I'm sorry... I'm out of order at this moment... retry later");
        }
    }

    dataserver( key _queryid, string _data )
    {
        if ( notecardTotalLinesId == _queryid )
        {
            notecardTotalLines = (integer) _data;
            return;
        }
        
        list temp;
        string value;
        integer length;

        if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
        {
            value = llDumpList2String( llParseString2List( _data, ["[TARGET]"], [""] ), target_name );
            length = llStringLength( _data );
            if ( llGetSubString( value, (length -5), length) != "[END]" )
            {
                llSay(0, value);
                notecardLineId = llGetNotecardLine( notecardName, ++notecardLine );;
            } else llSay(0, llGetSubString( value, 0, (length -6)));
        }
    } 
}