string FlipTitle = "( don't touch me )";
string ObjectName = "geezmo jr";

// ---------// ---------// ---------// ---------// ---------
 
 string target_name;
 string notecardName;
integer notecardLine;
    key notecardLineId;
integer notecardTotalLines;
    key notecardTotalLinesId;

// --------------------------------------------------------

default
{
    state_entry()
    {
        if ( ! llGetInventoryNumber( INVENTORY_NOTECARD ) )
            state Disabled;
            
        llInstantMessage( llGetOwner(), "Notecard found. All features are enabled." );
        
        notecardName = llGetInventoryName( INVENTORY_NOTECARD, 0 );
        notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName ); 
        
        llSetText( FlipTitle, <1.0, .0, .0>, 1.0 );
        llSetObjectName( ObjectName );
    }
    
    on_rez(integer _c)
    {
        llResetScript();
    }
        
    changed( integer _change )
    {
        if ( _change & CHANGED_INVENTORY )
            llResetScript();
    }
    
    touch_start(integer _c)
    {
        if ( llGetInventoryNumber( INVENTORY_SOUND ) > 0 )
            llPlaySound( llGetInventoryName( INVENTORY_SOUND,0 ), 1.0 );
        target_name = llDetectedName( 0 );
        notecardLineId = llGetNotecardLine( notecardName, (integer)llFrand(notecardTotalLines) );
    }

    dataserver( key _queryid, string _data )
    {
        if ( notecardTotalLinesId == _queryid )
        {
            notecardTotalLines = (integer) _data;
            return;
        }
        
        if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
        {
            llDumpList2String( llParseString2List( _data, ["[TARGET]"], [""] ), target_name );
            
            llSay(0, _data + " ( for: " + target_name + " )" );
        }
    } 
}

state Disabled
{
    state_entry()
    {
        llInstantMessage( llGetOwner(), "Notecard not found. All features are disabled." );
    }
        
    changed( integer _change )
    {
        if ( _change & CHANGED_INVENTORY )
            if ( llGetInventoryNumber( INVENTORY_SOUND ) > 0 )
                state default;
    }
}