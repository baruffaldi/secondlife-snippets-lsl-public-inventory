string target_name = "fail";

// ---------// ---------// ---------// ---------// ---------
 
 string notecardName;
    key notecardLineId;
integer notecardLine;
    key notecardTotalLinesId;
integer notecardTotalLines;

// --------------------------------------------------------

report( string _text, string _fliptitle, vector _color, integer _last, integer _pause )
{ 
    if ( _fliptitle != "" )
        llSetText( _fliptitle, _color, 1.0 );
        
    if ( _text != "" )
        llInstantMessage( llGetOwner(), _text );
}

default
{
    state_entry()
    {
        llSetObjectName(llKey2Name(llGetOwner()));
    }
    
    on_rez(integer _c)
    {
        llResetScript();
    }
        
    changed( integer _change )
    {
        // Something has changed in inventory, maybe we got  new settings to import!
        if ( _change & CHANGED_INVENTORY )
            state updateTargets;
    }
    
    touch_start(integer _c)
    {
        notecardLineId = llGetNotecardLine( notecardName, (integer)llFrand(notecardTotalLines) );
    }

    dataserver( key _queryid, string _data )
    {
        list temp;
        string name;
        string value;

        if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
        {
            integer percent;
            percent = ( notecardLine * 100 ) / notecardTotalLines;

            report(
                "",
                "Notecard reading... " + (string) percent + "%",
                <1.0,1.0,1.0>, FALSE, FALSE
            );
            
            llDumpList2String( llParseString2List( _data, ["[TARGET]"], [""] ), target_name );
            
            llSay(0, target_name + ": " + _data);
        }
    } 
}

state updateTargets
{
    state_entry()
    {
        notecardName = llGetInventoryName( INVENTORY_NOTECARD, 0 );
        
        if ( notecardName == "" ) {
            llOwnerSay( "You have to create a notecard with all targets" );
            state default;
        }
            
        if ( llGetInventoryType( notecardName ) == INVENTORY_NOTECARD )
            if ( llGetInventoryPermMask( notecardName, MASK_OWNER ) )
            {
                report(
                    "Notecard! Leeching notecard in progress...",
                    "Notecard reading... 0%",
                    <1.0,1.0,1.0>, FALSE, FALSE
                );

                notecardLine = 0;
                notecardTotalLinesId = llGetNumberOfNotecardLines( notecardName );
            }
    }

    dataserver( key _queryid, string _data )
    {
        if ( _queryid == notecardTotalLinesId )
        {
            notecardTotalLines = (integer) _data;
            notecardLineId = llGetNotecardLine( notecardName, notecardLine );
        }

        else if ( _queryid == notecardLineId )
            if ( _data != EOF )
            {
                list temp;
                string name;
                string value;

                if ( llGetSubString( _data, 0, 0 ) != "#" && llStringTrim( _data, STRING_TRIM ) != "" )
                {
                    integer percent;
                    percent = ( notecardLine * 100 ) / notecardTotalLines;

                    report(
                        "",
                        "Notecard reading... " + (string) percent + "%",
                        <1.0,1.0,1.0>, FALSE, FALSE
                    );
                }

                notecardLine++;
                notecardLine = notecardTotalLines;
                notecardLineId = llGetNotecardLine( notecardName, notecardLine );
            }

        else {
            report(
                "Notecard reading completed!",
                "Notecard reading completed!",
                <1.0,1.0,1.0>, TRUE, TRUE
            );
            llSleep(1.0);
            state default;
        }
    } 
}