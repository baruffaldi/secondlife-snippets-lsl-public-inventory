integer channel;
vector originalPosition;

list order_buttons(list buttons)
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4)
         + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}
 
vector getPos(float heightToAdd)
{
    vector pos   = llDetectedPos(0);
    rotation rot = llDetectedRot(0);
    vector offset = <0, 0, heightToAdd>;
    vector avOffset = offset * rot;
    pos += avOffset;
    
    return pos;
}

default
{
    state_entry()
    {
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        llListen(
            0,
            "",
            llGetOwner(),
            ""
        );
    }

    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == llGetOwner() )
            llDialog(
                llGetOwner(),
                "Quanto alto vuoi andare?",
                order_buttons(["50", "100", "200", "300", "1000", "3500", "10000", "15000", "5000"]),
                channel
            );
    }
     
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_chan == 0)
        {
            if( _option == "stop" ) llStopMoveToTarget();
            if( _option == "timer" ) {
                llSay(0,"Ok, 10 seconds for go back on the ground");
                integer x;
                
                for(x=0;x<=10;x++)
                {
                    llSay(0,(string)x);
                    
                    if(x==10) llStopMoveToTarget();
                    else llSleep(1);
                }
            }
        } else {
            vector original;
            original = llGetPos();
            
            llSay(
                0,
                "Ok! Let's go "  + (string)_option + " meters higher!"
            );
            
            if(_option == "50")
            {
                original += <0.0,0.0,(float)_option>;
                
                llMoveToTarget(
                    original,
                    1
                );
            } else {
                integer x;
                integer counts;
                counts = (integer)_option / 50;

                for(x=0;x<=counts;x++)
                {
                    original += <0.0,0.0,50>;
                    
                    llMoveToTarget(
                        original,
                        0.3
                    );
                    llSleep(1.0);
                }
            }
        }
    }
}
