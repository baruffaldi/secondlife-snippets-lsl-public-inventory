// Magic Ball by Bornslippy Ruby
//
// Feel free to edit this script and to let me know
// what you think about it ;)
//
// Copyright @ Filippo Baruffaldi <filippo@baruffaldi.info>
// Licensed under the GNU GPLv2 License
// http://www.gnu.org
//
// TODO: Notecard per mantenere impostazioni

integer AUTO_PET_MODE = FALSE;
integer AUTO_INVISIBLE_MODE = FALSE;

// Configuration Ends here

key target = NULL_KEY;
key source = NULL_KEY;

string target_name;
string header = "\nWelcome to the Magic Ball!\nv2.2.3\n\n";
string header_truth = "\n---------------------------\nThe Truth Game\n---------------------------\n\n";

integer interval = 1;
integer cycles = 1;
integer cycle = 0;
integer intensity = 1000;

integer channel = 0;
integer dialogStatus = FALSE;
integer actionStatus = FALSE;

list order_buttons(list buttons)
{
    return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4)
         + llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}

vector getPos()
{
    vector pos   = llDetectedPos(0);
    rotation rot = llDetectedRot(0);
    vector offset = <-0.5, -1, 1.7>;
    vector worldOffset = offset;
    vector avOffset = offset * rot;
    pos += avOffset;
    
    return pos;
}

vector getOwnerPos()
{
    vector pos   = llDetectedPos(0);
    rotation rot = llDetectedRot(0);
    vector offset = <-0.5, -1, 1.7>;
    vector worldOffset = offset;
    vector avOffset = offset * rot;
    pos += avOffset;
    
    return pos;
}

default
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        if(AUTO_INVISIBLE_MODE) llSetAlpha(0.0,ALL_SIDES);
    }
     
    on_rez(integer i)
    {
        llResetScript();
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:\n",
            order_buttons(["LandTools", "Features", "Hazard", "TestArea"]), 
            channel
        );
    }
    
    listen(integer _chan, string _name, key _id, string _option)
    {
        string intervalString;
        string settingsString;
        
        if(cycles == 0) intervalString = "looped";
        else intervalString = (string)cycles;
        
        if(target != NULL_KEY)
            settingsString = "Current target: " + llKey2Name(target) + "\n";
        else settingsString = "";
        
        settingsString += "Current intensity: " + (string)intensity + "\n";
        settingsString += "Current interval seconds: " + (string)interval + "\n";
        settingsString += "Current cycles: " + intervalString + "\n\n";
        
        if(_name != llKey2Name(llGetOwner()))
        {
           llInstantMessage(_id, "Nice hacking try, but I'm so afraid... it doesn't work :)" );
           return;
        }

        if(_option == "LandTools")
            llDialog(_id,header + "Please choose an option:",
            order_buttons(["AvaCounter"]), channel );
        else if(_option == "Features")
            llDialog(_id,header + "Please choose an option:",
            order_buttons(["TruthGame", "InvisibleMode", "PetMode", "ShieldMode"]), channel );
        else if(_option == "Hazard")
            llDialog(_id,header + "Please choose an option:",
            order_buttons(["SetTarget", "UUID", "WindFury", "DialogFury", "ChatFury", "Settings"]), channel );
        else if(_option == "TestArea")
            llDialog(_id,header + "Please choose an option:",
            order_buttons(["RadioSpot", "AvaOnline", "VisitCount", "AvaCounter", "Murphy", "Radio", "Taxi"]), channel );

        else if(_option == "Settings")
            llDialog(_id,header + settingsString + "Please choose an option:",
            order_buttons(["SetIntensity", "SetInterval", "SetCycles"]), channel );
        
// *** LANDTOOLS
        else if(_option == "AvaCounter")
        {
            integer avatarsOnline = llGetRegionAgentCount();
            
            if(avatarsOnline==1)
                llInstantMessage(llGetOwner(), "Hey boss, seems you're ALONE! Go stole something, nobody can see you ;P");
            else llInstantMessage(llGetOwner(), "In this land there are " + (string)avatarsOnline + " avatars online!");
            
// *** INVISIBLE MODE
        } else if(_option == "InvisibleMode")
            llDialog(_id,header + "Please choose an option:",
            order_buttons(["Invisible On", "Invisible Off"]), channel );
        else if(_option == "Invisible On")
            llSetAlpha(0.0,ALL_SIDES);
        else if(_option == "Invisible Off")
            llSetAlpha(1.0,ALL_SIDES);

// *** PETMODE
        else if(_option == "PetMode")      state petMode;
        
// *** RADIOSPOT
        else if(_option == "RadioSpot")    state radio;
        
// *** HAZARD
        else if(_option == "SetTarget")    state setTarget;
        else if(_option == "SetIntensity") state setIntensity;
        else if(_option == "SetInterval")  state setInterval;
        else if(_option == "SetCycles")    state setCycles;
        
        else if(_option == "WindFury")     state windFury;
        else if(_option == "DialogFury")   state dialogFury;
        else if(_option == "ChatFury")     state chatFury;
        
        else if(_option == "ShieldMode")   state shieldMode;
        
        else if(_option == "UUID")
        {
            // Workaround while waiting the new algorithm to scan the land!
            llInstantMessage(llGetOwner(), "Hey Boss, your UUID key is "  + (string)_id );
            llInstantMessage(llGetOwner(), "... and I'm trying to learn how to detect the target UUID!" );
        }
        
// *** TESTING
        else if(_option == "TruthGame")    state truthGame;
        else {
            llInstantMessage(llGetOwner(), "I'm afraid boss! The feature "  + (string)_option + " is still under development...");
        }
    }
}

state chatFury
{
    state_entry()
    {
        if(target == NULL_KEY)
        {        
            llInstantMessage(
                llGetOwner(),
                "Sorry Boss, you have to choose a target before attack!"
            );
            
            state default;
        } else llInstantMessage(
            llGetOwner(),
            "Ok, I'm sending " + (string) (cycles*30) + " shit sentences :)"
        );
        
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llSetTimerEvent(0.1);
        
    }
    
    state_exit()
    {
        cycle = 0;
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
        
        llInstantMessage(
            llGetOwner(),
            "should be dead. bwah."
        );
            
        state actionDone;
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:",
            order_buttons(["Stop it!"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "Stop it!")
        {
            llSensorRemove();
            state actionDone;
        }
    }
    
    timer()
    {
        if(cycle <= (cycles*30))
        {
            llInstantMessage(
                    target,
                    "The MagicBox is loving you :)"
                );
            cycle++;
        } else state default;
    }
}

state damageFury
{
    state_entry()
    {
        
    }
}

state dialogFury
{
    state_entry()
    {
        if(target == NULL_KEY)
        {        
            llInstantMessage(
                llGetOwner(),
                "Sorry Boss, you have to choose a target before attack!"
            );
            
            state default;
        } else llInstantMessage(
            llGetOwner(),
            "Ok, I'm sending " + (string) (cycles*10) + " dialogs :)"
        );
        
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llSetTimerEvent(0.1);
    }
    
    state_exit()
    {
        cycle = 0;
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
        
        llInstantMessage(
            target,
            "should be dead. bwah."
        );
            
        state actionDone;
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "Stop it!")
        {
            llSensorRemove();
            state actionDone;
        }
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:",
            order_buttons(["Stop it!"]),
            channel
        );
    }
    
    timer()
    {
        if(cycle <= (cycles*10))
        {
            llDialog(
                target,
                "The MagicBox is loving you :)",
                order_buttons(["<3","<3","<3"]),
                channel
            );
            cycle++;
        } else state default;
    }
}

state windFury
{
    state_entry()
    {
        if(target == NULL_KEY)
        {        
            llInstantMessage(
                llGetOwner(),
                "Sorry Boss, you have to choose a target before attack!"
            );
            
            state default;
        } else llInstantMessage(
            llGetOwner(),
            "BWHAHAHHA, I <3 WAAAAR :)"
        );
        
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llSensorRepeat( "",
            target,
            AGENT,
            90.0,
            PI,
            interval
        );
    }
    
    state_exit()
    {
        cycle = 0;
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
        
        llInstantMessage(
            llGetOwner(),
            "should be dead. bwah."
        );
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:",
            order_buttons(["Stop it!"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "Stop it!")
        {
            llSensorRemove();
            state actionDone;
        }
    }
    
    sensor(integer total_number)
    {
        if(!cycles || cycle <= cycles)
        {
            llMoveToTarget(getPos(), 0.3);
            llPushObject(target, <intensity,0,0>, <0,0,0>, FALSE);
            llMoveToTarget(llDetectedPos(0), 0.3);
            cycle++;
        } else {
            state actionDone;
        }
    }
}

state shieldMode
{
    state_entry()
    {   
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            90.0,
            PI,
            interval
        );
        
        llInstantMessage(
            llGetOwner(),
            "Hey Boss, don't worry, everyone who get near to you will be launched to the moon! ;)"
        );
    }
    
    state_exit()
    {   
        llInstantMessage(
            llGetOwner(),
            "Ok, you're safe right now..."
        );
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:",
            order_buttons(["Stop it!"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "Stop it!")
        {
            llSensorRemove();
            state actionDone; 
        }
    }
    
    sensor(integer total_number)
    {
        integer x;
        for(x=0;x <= total_number;x++)
            if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                llPushObject(llDetectedKey(x), <100,0,0>, <0,0,0>, FALSE);
            else if(llDetectedKey(x) == llGetOwner())
                llMoveToTarget(llDetectedPos(x), 1.3);
    }
}

state petMode
{
    state_entry()
    {
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel,"", "","");
        llSensorRepeat( "", 
            llGetOwner(),
            AGENT,
            50.0,
            PI,
            interval
        );
        
        llInstantMessage(
            llGetOwner(),
            "Boss... come near to me and I won't lost you anymore... promised!"
        );
    }
    
    state_exit()
    {
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Please choose an option:\n",
            order_buttons(["Stop it!"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "Stop it!")
        {
            llSensorRemove();
            llInstantMessage(_id, "Ooooooooooook, I leave you alone..." );
            state actionDone;
        }
    }
    
    sensor(integer total_number)
    {
        llMoveToTarget(getPos(), 0.3);
    }
}

state setTarget
{
    state_entry()
    {
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            "");
        
        llSensorRepeat( "",
            NULL_KEY,
            AGENT,
            90.0,
            PI,
            1
        );
        
        llInstantMessage(
            llGetOwner(),
            "Boss... just give me the time to look around me!"
        );
    }
    
    state_exit()
    {
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
    }

    touch_start(integer total_number)
    {
        llInstantMessage(
            llDetectedKey(0),
            "Hey, I'm looking around... wait please!"
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        target = _option;
        dialogStatus = FALSE;
        actionStatus = TRUE;
    }
    
    sensor(integer total_number)
    {
        if(!actionStatus)
        {
            integer x;
            list buttons = [];
    
            for(x=0;x <= total_number;x++)
                if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                    buttons += [(string)llKey2Name(llDetectedKey(x))];
                 
            if(!dialogStatus) 
            { 
                llDialog(
                    llGetOwner(),
                    header + "Please choose a target:",
                    order_buttons(buttons),
                    channel
                );
                dialogStatus = TRUE;
            }
        } else {
            integer x; 
             
            for(x=0;x <= total_number;x++)
                if(llKey2Name(llDetectedKey(x)) == target)
                    target = llDetectedKey(x);
        
            llInstantMessage(
                llDetectedKey(0),
                "Ok, I'm ready to wipe " + llKey2Name(target)
            );
            
            actionStatus = FALSE;
            dialogStatus = FALSE;
            state actionDone;
        }
    }
}

state setIntensity
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel,"", "","");

        llInstantMessage(
            llGetOwner(),
            "Boss... choose the intensity of our weapons... be carefull!"
        );
        
        llDialog(
            llDetectedKey(0),
            header + "Current intensity: "+(string)intensity+"\nPlease choose a target:",
            order_buttons(["Ok", "-1000", "-100", "-10", "+10", "+100", "+1000"]),
            channel
        );
    }
    
    state_exit()
    {
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Current intensity: "+(string)intensity+"\nPlease choose a target:\n",
            order_buttons(["Ok", "-1000", "-100", "-10", "+10", "+100", "+1000"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {   
             if(_option == "-1000") intensity -= 1000;
        else if(_option == "-100")  intensity -= 100;
        else if(_option == "-10")   intensity -= 10;
        else if(_option == "+1000") intensity += 1000;
        else if(_option == "+100")  intensity += 100;
        else if(_option == "+10")   intensity += 10;

        llInstantMessage(
            llGetOwner(),
            "The new intensity is: " + (string)intensity
        );
        
        if(_option == "Ok") state actionDone;
    }
}

state setInterval
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel,"", "","");

        llInstantMessage(
            llGetOwner(),
            "Boss... choose the intensity of our weapons... be carefull!"
        );
        
        llDialog(
            llDetectedKey(0),
            header + "Current interval seconds: "+(string)interval+"\nPlease choose a target:",
            order_buttons(["Ok", "-10", "-3", "-1", "+1", "+3", "+10"]),
            channel
        );
    }
    
    state_exit()
    {
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Current interval seconds: "+(string)interval+"\nPlease choose a target:",
            order_buttons(["Ok", "-10", "-3", "-1", "+1", "+3", "+10"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
             if(_option == "-10") interval -= 10;
        else if(_option == "-3")  interval -= 3;
        else if(_option == "-1")  interval -= 1;
        else if(_option == "+1")  interval += 1;
        else if(_option == "+3")  interval += 3;
        else if(_option == "+10") interval += 10;
        
        if(interval <= 0) interval = 1;

        llInstantMessage(
            llGetOwner(),
            "The new interval seconds are: " + (string)interval
        );
        
        if(_option == "Ok") state actionDone;
    }
}

state setCycles
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel,"", "","");

        llInstantMessage(
            llGetOwner(),
            "Boss... tell me how many times I gotta shoot!"
        );
        
        llDialog(
            llDetectedKey(0),
            header + "Current cycles: "+(string)cycles+"\nPlease choose a target:",
            order_buttons(["Ok", "Looped", "-10", "-3", "-1", "+1", "+3", "+10"]),
            channel
        );
    }
    
    state_exit()
    {
    }

    touch_start(integer total_number)
    {
        if ( llGetOwner() != llDetectedKey(0) ) {
            llInstantMessage(
                llDetectedKey(0),
                "I'm afraid, I can't talk with you, my owner doesn't like it! Ask him or buy me :)"
            );
            llInstantMessage(
                llGetOwner(),
                "Hey Boss, " + llKey2Name( llDetectedKey(0) ) + " is fingering me... Can I kill that shit?"
            );
            
            return;
        }
        
        llDialog(
            llDetectedKey(0),
            header + "Current cycles: "+(string)cycles+"\nPlease choose a target:",
            order_buttons(["Ok", "Looped", "-10", "-3", "-1", "+1", "+3", "+10"]),
            channel
        );
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        string intervalString;
        
        if(cycles == 0) intervalString = "looped";
        else intervalString = (string)cycles;

        llInstantMessage(
            llGetOwner(),
            "The new interval seconds are: " + intervalString
        );
        
             if(_option == "-10") cycles -= 10;
        else if(_option == "-3")  cycles -= 3;
        else if(_option == "-1")  cycles -= 1;
        else if(_option == "+1")  cycles += 1;
        else if(_option == "+3")  cycles += 3;
        else if(_option == "+10") cycles += 10;
        else if(_option == "Looped") cycles = 0;
        
        if(_option == "Ok") state actionDone;
    }
}

state actionDone
{
    state_entry()
    {
        llSetStatus(STATUS_PHYSICS, TRUE);
        llSleep(0.1);
        
        llSensorRepeat( "",
            llGetOwner(),
            AGENT,
            90.0,
            PI,
            1
        );
    }
    
    state_exit()
    {
        llSetStatus(STATUS_PHYSICS, FALSE);
        llSleep(0.1);
    }
 
    touch_start(integer total_number)
    {       
        llInstantMessage(
            llDetectedKey(0),
            "Hey, leave me alone.. I'm looking for my Boss!"
        );
    }
    
    sensor(integer total_number)
    {
        llMoveToTarget(getPos(), 1.3); 
        llSleep(1);
        
        state default;
    }
}

state truthGame
{
    state_entry()
    {
        llListen(3, "", "", "");
            
        llSay(
            0,
            header_truth + "The game is started!\n\nType /3 help to view the usage or just touch me to choose who you wanna ask something.\n\n"
        );
    }
    
    state_exit()
    {
        actionStatus = FALSE;
        dialogStatus = FALSE;
        source = NULL_KEY;
        target = NULL_KEY;
        target_name = "";
        
        llSay(
            0,
            header_truth + " is ended!\n\nThanks everyone, come back soon.\n\n"
        );
    }

    touch_start(integer total_number)
    {
        if(source != NULL_KEY && source != llDetectedKey(0))
        {
            llInstantMessage(llDetectedKey(0), "Please wait your turn, " + llKey2Name(source) + " is writing his question. Thanks");
            return;
        }
        else if(!actionStatus)
        {
            source = llDetectedKey(0);
            actionStatus = TRUE;
            dialogStatus = FALSE;
            
            llSay(
                0,
                header_truth + llKey2Name(source) + " wanna ask something..\n\n"
            );
        
            llSensorRepeat( "",
                NULL_KEY,
                AGENT,
                70.0,
                PI,
                5
            );
        }        
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        if(_option == "help")  llInstantMessage(_id, header_truth + "To play this game you just need to be true with other players, there's no enemies, just true people.\n\nIf you touch me, I'll keep your request, and right that moment I won't receive another player's requests. After some seconds, I'll give you the list of the players.\n\nAfter this step, remains just to type on localchat:\n\n /3 QUESTION\nWhere QUESTION is the question you want to ask.\n\nIf he/she's a good player, he/she will be true!\n\nGood Game\n\n");
        else if(_option == "CancelRequest" && actionStatus )
        {
            llSay(
                0,
                header_truth + llKey2Name(source) + " looks like shy and he/she doesn't wanna ask anymore..\n\nI'm able again now for other questions!\n\n"
            );
            
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
        }
        else if(_option == "CloseGame" && _id == llGetOwner() )
        {
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            state actionDone; 
        }
        else if( actionStatus == 2 )
        {
            llSay(
                0,
                header_truth + "From: " + llKey2Name(source) + "\nTo: " + target_name + "\n\nQuestion:\n"+_option+"\n\nNow I'm available to manage another question... come on, touch me again!\n\n"
            );
            
            actionStatus = FALSE;
            dialogStatus = FALSE;
            source = NULL_KEY;
            target_name = "";
            llSensorRemove();
        }
        else if( actionStatus )
        {
            target_name = _option;
            actionStatus = 2;
            llSensorRemove();
            llInstantMessage(source, "The current target is: " + target_name + ", now I'm waiting for the question.");
        }
        
    }
    
    sensor(integer total_number)
    {
        if(actionStatus)
        {
            integer x;
            list buttons = ["CancelRequest","CippaLippa"];

            for(x=0;x <= total_number;x++)
                if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                    buttons += [(string)llKey2Name(llDetectedKey(x))];
            
            if(source == llGetOwner()) buttons += ["CloseGame"];
             
            if( !dialogStatus || source == llGetOwner() ) 
            {
                llDialog(
                    source,
                    header_truth + "Please choose a target who will receive the question:",
                    order_buttons(buttons),
                    3
                );
                dialogStatus = TRUE;
            }
        }
    }
}

state radio
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(
            channel,
            "",
            llGetOwner(),
            ""
        );
        
        llSay(
            0,
            header + "Let's play good music!\n\n"
        );
        
        llDialog(
            source,
            header + "Please choose the radio:",
            order_buttons(["m2o"]),
            channel
        );
    }
    
    state_exit()
    {
        llSay(
            0,
            header + "Hey, the film is ended! Go home ;P\n\n"
        );
    }

    touch_start(integer total_number)
    {
        
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        
    }
     
    sensor(integer total_number)
    {
        
    }
}