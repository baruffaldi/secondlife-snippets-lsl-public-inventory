
// ON/OFF Textures
key t_sawOn = "7a258002-729f-6457-3e5b-e066381efa29";
key t_sawOff = "147aef50-0bb0-4292-2caf-061ee6173f4e";

default
{
    link_message( integer _sender, integer _num, string _message, key _id) 
    {    
        // Coming from an interface script
        if ( _message == "ZHAO_SITANYWHERE_ON" ) {
            llSetTexture(t_sawOn, ALL_SIDES);
        } else if ( _message == "ZHAO_SITANYWHERE_OFF" ) {
            llSetTexture(t_sawOff, ALL_SIDES);
        }
    }
}
