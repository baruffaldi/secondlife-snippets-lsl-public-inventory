key greenState = "35dc635f-002e-b37b-5f5c-bbca97267eb0";
key redState = "36c86fbc-a816-4e59-7822-66d28ffaf325";
integer channel;
string target;
key target_key;

default
{
    state_entry()
    {
        llSetTexture( greenState, ALL_SIDES );
    }
    
    state_exit()
    {
        llSetTexture( redState, ALL_SIDES );
    }
    
    touch_start(integer total_number)
    {
        state SearchingTarget;
    }
}

state SearchingTarget
{
    state_entry()
    {
        channel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
        llListen(channel, "", llGetOwner(), "");

        llSensor( "",
            NULL_KEY,
            AGENT,
            92.2,
            PI );
    }

    state_exit()
    {
        llListenRemove(channel);
        llSensorRemove();
    }

    sensor( integer total_number )
    {
        integer x;
        list buttons;

        if( total_number <= 9 )
        {    
            for(x=0;x <= total_number;x++)
                if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                    buttons += [(string)llKey2Name(llDetectedKey(x))];
    
            llDialog(
                llGetOwner(),
                "Please choose a target:",
                buttons,
                channel
                    );
        } else {
            integer y;
            integer cycles;
            integer total;
            cycles = total_number / 9;

            for(y=0;y <= cycles;y++)
            {
                total += 9; 
                for(1;x <= total;x++) 
                    if((string)llKey2Name(llDetectedKey(x)) != "" && llDetectedKey(x) != llGetOwner())
                        buttons += [(string)llKey2Name(llDetectedKey(x))];
        
                llDialog(
                    llGetOwner(),
                    "Please choose a target:",
                    buttons,
                    channel
                        );
            }
        }
    }

    no_sensor()
    {
        llOwnerSay( "You're alone" );
        state default;
    }
    
    touch_start( integer _n )
    {
        llDialog( llGetOwner(), "Choose an option below:", ["SearchAgain", "StopSearch"], channel );
    }
    
    listen( integer _chan, string _name, key _id, string _message )
    {
        if ( _message == "SearchAgain" ) 
            llSensor( "",
                NULL_KEY,
                AGENT,
                92.2,
                PI );
                
        else if ( _message == "StopSearch" ) 
        {
            llOwnerSay( "Canceled!" );
            state default;
        } else {
            target = _message;
            state SetTargetUUID;
        }
    }
}

state SetTargetUUID
{
    state_entry()
    {
        llSensor(
            "",
            target,
            AGENT,
            92.2,
            PI );
    }

    sensor( integer total_number )
    {
        integer x;
        for(x=0;x <= total_number;x++)
            if(llKey2Name(llDetectedKey(x)) == target )
                target_key = llDetectedKey(x);

        if(target_key == NULL_KEY)
        {
            llOwnerSay( "You're alone" );
        state default;

        } else state FollowingTarget;
    }
    
    touch_start( integer _n )
    {
        llOwnerSay( "Canceled!" );
        state default;
    }

    no_sensor()
    {
        llOwnerSay( "You're alone" );
        state default;
    }
}


state FollowingTarget
{
    state_entry()
    {
        llRezObject( "007 Ball", llGetPos() + <0.0,0.0,1.0>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, 0  );
        llSleep( 1.0 );
        llShout( 1234647863, (string) target_key );
        state default;
    }
}