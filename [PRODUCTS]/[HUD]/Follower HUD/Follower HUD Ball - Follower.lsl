string dieCommand = "stop";

//---------------------------------------------------

key target_key;

default
{
    on_rez( integer _p )
    {
        llSetSitText( "SuckMyDick" );
        llSetAlpha( 0.0, ALL_SIDES );
    }

    state_entry( )
    {
        llListen( 0, "", llGetOwner(), dieCommand );
        llListen( 1234647863, "", "", "" );
    }
    
    listen( integer _chan, string _name, key _id, string _message )
    {
        if ( _chan == 0 )
        {
            llInstantMessage( llGetOwner(), "Ooook, I'm wiping myself!" );
            llDie();
        } else {
            llInstantMessage( llGetOwner(), "Ok! I detected the target to follow!" );
            target_key = (key) _message;
            state FollowingTarget;
        }
    }
}

state FollowingTarget
{
    state_entry()
    {
        llListen( 0, "", llGetOwner(), dieCommand );

        llSensorRepeat( "",
            target_key,
            AGENT,
            92.2,
            7000*PI, 0.5 );

        llSetStatus( STATUS_PHYSICS, TRUE );
        llSleep( 0.1 );
        
        llSetVehicleType(VEHICLE_TYPE_CAR);
        llSleep( 0.1 );
        
        llMoveToTarget( llGetPos(), 0.1 );
    }

    sensor( integer total_number )
    {
        vector pos = llDetectedPos(0);
        vector offset = <-1, 0, 1>;
        pos+=offset;
        llMoveToTarget( pos, 0.1 ); // Does not enter on parcel
        llLookAt( pos, 2, 1 );
    }
    
    listen( integer _chan, string _name, key _id, string _message )
    {
        llInstantMessage( llGetOwner(), "Ooook, I'm wiping myself!" );
        llDie();
    }

    no_sensor()
    {
        llInstantMessage( llGetOwner(), "The target is no more here!" );
        llDie();
    }
}