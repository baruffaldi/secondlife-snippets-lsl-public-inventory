default
{
    state_entry()
    {
        llListen(0,"","","");
    }

    listen(integer _chan, string _name, key _id, string _option)
    {
        llInstantMessage( llGetOwner(), "[" + llGetRegionName() + "]" + _name + ": " + _option );
    }
}