key greenState = "35dc635f-002e-b37b-5f5c-bbca97267eb0";
key redState = "36c86fbc-a816-4e59-7822-66d28ffaf325";

default
{
    state_entry()
    {
        llSetTexture( redState, ALL_SIDES );
    }
    
    state_exit()
    {
        llSetTexture( greenState, ALL_SIDES );
    }
    
    touch_start(integer total_number)
    {
        state Sniffing;
    }
}

state Sniffing
{
    state_entry()
    {
        integer i = 1;
        for ( ; i <= 65; ++i )
            llListen( i, "", "", "" );
    }

    state_exit()
    {
        
    }
    
    touch_start( integer _n )
    {
        state default;
    }
    
    listen( integer _chan, string _name, key _id, string _message )
    {
        llOwnerSay( "[" + (string) _chan + "]" + "[" + _name + "]" + "[" + (string) _id + "]" + " " + _message );
    }
}